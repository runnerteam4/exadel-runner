export const data = [
	{
		time: '10 минут',
		name: 'Java многопоточность',
		type: 'Тренировочный',
		id: 111,
		topics: ['Java'],
	},
	{
		name: 'C++ ООП',
		type: 'Контрольный',
		time: '20 минут',
		id: 222,
		topics: ['C++'],
	},
	{
		name: 'Java Core',
		type: 'Контрольный',
		time: '30 минут',
		id: 333,
		topics: ['Java'],
	}
];
