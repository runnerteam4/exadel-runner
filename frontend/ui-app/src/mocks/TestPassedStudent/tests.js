export const tests = [
	{'name': 'Python PYQT5', 'type': 'Контрольный', 'date': '12.05.2018', 'result': '7/10', 'comment': '-'},
	{'name': 'JavaScript ООП', 'type': 'Контрольный', 'date': '12.05.2017', 'result': '4/10', 'comment': 'Почитай про наследованние'},
	{'name': 'Java Core', 'type': 'Тренировочный', 'date': '12.05.2018', 'result': '10/10', 'comment': '-'},
	{'name': 'C# ООП', 'type': 'Тренировочный', 'date': '12.05.2016', 'result': '2/10', 'comment': '-'}
];

export const answers = [
	{'name': 'Что такое многопоточность?', 'result': 'Принято'},
	{'name': 'К чему ближе промис, к событию или коллбэку?', 'result': 'Не принято'},
	{'name': 'Какие существуют примитивные типы?', 'result': 'Принято'},
	{'name': 'Напишите свою реализацию функции bind.', 'result': 'Не принято'},
];
