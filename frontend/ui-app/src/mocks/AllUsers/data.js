export const users = [
	{ name: 'Анна', surname: 'Зарецкая', role: 'преподаватель', status: true, id: '1'},
	{ name: 'Антон', surname: 'Бакатович', role: 'преподаватель', status: true, id: '2'},
	{ name: 'Андрей', surname: 'Бутырчик', role: 'преподаватель', status: false, id: '3'},
	{ name: 'Антонина', surname: 'Вертинская', role: 'администратор', status: true, id: '4'},
	{ name: 'Антон', surname: 'Голосной', role: 'администратор', status: true, id: '5'},
	{ name: 'Елизаветта', surname: 'Дмитренко', role: 'студент', status: true, id: '6'},
	{ name: 'Антон', surname: 'Нарбутович', role: 'преподаватель', status: false, id: '7'},
	{ name: 'Никита', surname: 'Прищепов', role: 'студент', status: true, id: '8'},
];

export const dataRejectRequest = [
	{ name: 'Анна', surname: 'Зарецкая', role: 'преподаватель', status: true, id: '1'},
	{ name: 'Андрей', surname: 'Бутырчик', role: 'студент', status: true, id: '2'},
	{ name: 'Антон', surname: 'Бакатович', role: 'преподаватель', status: true, id: '3'},
	{ name: 'Антонина', surname: 'Вертинская', role: 'администратор', status: true, id: '4'},
	{ name: 'Антон', surname: 'Голосной', role: 'администратор', status: true, id: '5'},
	{ name: 'Елизаветта', surname: 'Дмитренко', role: 'студент', status: true, id: '6'},
	{ name: 'Антон', surname: 'Нарбутович', role: 'преподаватель', status: false, id: '7'},
	{ name: 'Никита', surname: 'Прищепов', role: 'студент', status: true, id: '8'},
];

export const dataApproveRequest = [
	{ name: 'Анна', surname: 'Зарецкая', role: 'преподаватель', status: true, id: '1'},
	{ name: 'Андрей', surname: 'Бутырчик', role: 'преподаватель', status: true, id: '2'},
	{ name: 'Антон', surname: 'Бакатович', role: 'преподаватель', status: true, id: '3'},
	{ name: 'Антонина', surname: 'Вертинская', role: 'администратор', status: true, id: '4'},
	{ name: 'Антон', surname: 'Голосной', role: 'администратор', status: true, id: '5'},
	{ name: 'Елизаветта', surname: 'Дмитренко', role: 'студент', status: true, id: '6'},
	{ name: 'Антон', surname: 'Нарбутович', role: 'преподаватель', status: false, id: '7'},
	{ name: 'Никита', surname: 'Прищепов', role: 'студент', status: true, id: '8'},
];

export const teachersRequest = [
	{ name: 'Андрей', surname: 'Бутырчик', role: 'преподаватель', status: false, id: '2'},
	{ name: 'Антон', surname: 'Нарбутович', role: 'преподаватель', status: false, id: '7'},
];

export const dataRejectTeacterRequest = [
	{ name: 'Aндрей', surname: 'Бутырчик', role: 'преподаватель', status: false, id: '2'},
];