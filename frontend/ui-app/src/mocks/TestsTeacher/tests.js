const [groups, student, name, type, date] = ['Группа', 'Имя ученика', 'Название', 'Тип теста', 'Дата прохождения'];
export const tests = [{[groups]: '13', [student]: 'Андрей Бутырчик', [name]: 'Python', [type]: 'Контрольный', [date]: '12.08.2018', id: '132'},
	{[groups]: '12', [student]: 'Павел Подопригора', [name]: 'Java', [type]: 'Контрольный', [date]: '24.07.2018', id: '32'},
	{[groups]: '8', [student]: 'Никита Гришин', [name]: 'JavaScript', [type]: 'Контрольный', [date]: '24.01.2018', id: '321'},
	{[groups]: '1', [student]: 'Максим Прищеп', [name]: 'Swift', [type]: 'Контрольный', [date]: '14.02.2018', id: '332'},
	{[groups]: '13', [student]: 'Дмитрий Гаврушко', [name]: 'C++', [type]: 'Контрольный', [date]: '13.06.2018', id: '21'},
	{[groups]: '13', [student]: 'Никита Никифоров', [name]: 'C#', [type]: 'Контрольный', [date]: '31.04.2018', id: '992'}];
