export const groups = [{
	label: '12',
	value: '12',
},
{
	label: '8',
	value: '8',
},
{
	label: '6',
	value: '6',
},
{
	label: '13',
	value: '13',
},
{
	label: '2',
	value: '2',
}];

export const tasks = [{
	name: 'Пятнашки',
	group: '13',
	assign: 'Павел Подопригора',
	deadline: '12.08.2018',
	mark: '7',
},
{
	name: 'Пирамида',
	group: '12',
	assign: 'Никита Гришин',
	deadline: '22.09.2018',
	mark: '8',
},
{
	name: 'Призрак',
	group: '8',
	assign: 'Зоя Носова',
	deadline: '14.12.2018',
	mark: '4',
},
{
	name: 'Хеопс',
	group: '1',
	assign: 'Елена Рябич',
	deadline: '01.08.2018',
	mark: '9',
},
{
	name: 'Матрешка',
	group: '14',
	assign: 'Клавдия Пакет',
	deadline: '15.01.2019',
	mark: '8',
},
{
	name: 'Йода',
	group: '20',
	assign: 'Глеб Шишкин',
	deadline: '04.11.2018',
	mark: '6',
},
{
	name: 'Саморез',
	group: '4',
	assign: 'Юлия Морозова',
	deadline: '17.10.2018',
	mark: '10',
}];

export const allTasks = [
	{text: 'лабораторная работа №1 Java', id: '1' },
	{text: 'Саморез', id: '2' },
	{text: 'Матрёшка', id: '7' },
	{text: 'лабораторная работа №5 Java', id: '8' },
	{text: 'лабораторная работа №3 Java', id: '3' },
	{text: 'Йода', id: '4' },
	{text: 'лабораторная работа №5 Java', id: '5' },
	{text: 'лабораторная работа №6 Java', id: '6' },
	{text: 'лабораторная работа №7 Java', id: '7' },
];