export const tasks = [
	{
		id: 1,
		name: 'Построить матрицу смежности',
		proveTests: '10/15',
		deadline: '31.08.2018'
	},
	{
		id: 2,
		name: 'Алгоритм дейкстры',
		proveTests: '0/21',
		deadline: '25.08.2018'
	},
	{
		id: 3,
		name: 'Поиск в ширину',
		proveTests: '7/34',
		deadline: '16.08.2018'
	},
	{
		id: 4,
		name: 'Поиск в глубину',
		proveTests: '12/14',
		deadline: '01.09.2018'
	},
	{
		id: 5,
		name: 'Построить дерево',
		proveTests: '1/3',
		deadline: '26.08.2018'
	},
];