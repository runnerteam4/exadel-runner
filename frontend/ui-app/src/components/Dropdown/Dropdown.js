import React from 'react';
import Button from '../Button/Button';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAlignJustify} from '@fortawesome/free-solid-svg-icons/faAlignJustify';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

class Dropdown extends React.Component {
	constructor() {
		super();
		this.state = {
			isOpen: false
		};
	}

	clickDropDown = () => {
		this.setState(prevState => ({
			isOpen: !prevState.isOpen
		}));
	};

	lostFocus = () => {
		this.setState({
			isOpen: false
		})
	};

	render() {
		const dropDownClassNames = classNames('w-100 mb-0 pl-0', this.state.isOpen ? 'd-block' : 'd-none');
		return (
			<React.Fragment>
				<Button className="d-flex d-sm-none"
						onClick={this.clickDropDown}>
					<FontAwesomeIcon icon={faAlignJustify}/>
				</Button>
				<ul className={ dropDownClassNames }>
					{
						this.props.options.map((tab, index) =>
							<li className="nav-link"
								key={index}
								onClick={this.lostFocus}>
								<Link to={tab.link} className="nav-link text-light">{tab.text}</Link>
							</li>
						)
					}
				</ul>
			</React.Fragment>
		)
	}
}

Dropdown.defaultProps = {
	options: null
};

Dropdown.propTypes = {
	options: PropTypes.array
};


export default Dropdown;
