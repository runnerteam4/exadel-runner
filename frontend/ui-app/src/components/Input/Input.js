import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';

const Input = (props) => {
	const {className, label,  ...remain} = props;
	return (
		<React.Fragment>
			{label && <label>{label}</label>}
			<input {...remain} className={classNames('form-control', className)}/>
		</React.Fragment>
	);
};

Input.propTypes = {
	type: PropTypes.string,
	placeholder: PropTypes.string,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]),
	onChange: PropTypes.func,
	label: PropTypes.string
};

Input.defaultProps = {
	type: 'text',
	placeholder: '',
	value: '',
	label:''
};

export default Input;
