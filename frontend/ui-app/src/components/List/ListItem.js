import React, {Component} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';

class ListItem extends Component{
	render(){
		const classes = classNames(
			'list-group-item',
			this.props.className,
			{disabled: this.props.disabled}
		);
		const handleClick = () => {
			this.props.onClick(this.props.id, this.props.multiple);
		};
		const inputType = (this.props.multiple)? 'checkbox' : 'radio';
		return <li className = {classes}  onClick = {handleClick}>
			{this.props.children}
			{this.props.selectable &&
			<input key = {this.props.id} type={inputType}
				   checked={this.props.checkedItems.includes(this.props.id)} name={'group'} />}
		</li>;
	}
}

ListItem.propTypes = {
	checkedItems: PropTypes.array,
	onClick: PropTypes.func,
	onDelete: PropTypes.func,
	title: PropTypes.string,
	key: PropTypes.number,
	disabled: PropTypes.bool,
	className: PropTypes.string,
	selectable:PropTypes.bool,
	multiple: PropTypes.bool,
	id: PropTypes.number
};

ListItem.defaultProps = {
	onClick: ()=>{},
	disabled: false,
	selectable: false,
	multiple: false,
	checkedItems: []
};

export default ListItem;