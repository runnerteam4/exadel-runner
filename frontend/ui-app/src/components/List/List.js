import React, {Component} from 'react';
import ListItem from './ListItem.js';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';


export default class List extends Component{
	addCheckedItemForCheckbox = (index) => {
		const isItemChecked = !!(this.props.checkedItems.find(item => {
				return item === index;})
		);
		let data = this.props.checkedItems;
		if(isItemChecked) {
			data = data.filter(item => item !== index)
		}
		else{
			data = [...data,index];
		}
		this.props.handleChecked(data);
	};
	addCheckedItemForRadio = (index) => {
		const isItemChecked = !!(this.props.checkedItems.find(item => {
				return item === index;})
		);
		this.props.handleChecked((isItemChecked) ? [] : [index]);
	};

	addCheckedItem = (index, multiple) => {
		if(multiple == true) {
			this.addCheckedItemForCheckbox(index);
		}else{
			this.addCheckedItemForRadio(index);
		}
	};

	getListItem = (item, index) => {
		return this.props.renderListItem(item, index, this.props.checkedItems, this.addCheckedItem);
	};
	render(){

		const {data, className} = this.props;
		return <ul className = {classNames('list-group list-group-flush',className)}>
			{data.map(this.getListItem)}
		</ul>;
	}
}

List.propTypes = {
	data: PropTypes.array,
	renderListItem: PropTypes.func,
	className: PropTypes.string
};

const renderListItem = (item, index, checkedItems, click) => (
	<ListItem  selectable={true} className= "d-flex justify-content-between"
			   multiple = {true} id = {item.text} checkedItems = {checkedItems} onClick = {click}>
		{item.text}
	</ListItem>
);

List.defaultProps = {
	data: [],
	renderListItem: renderListItem
};
