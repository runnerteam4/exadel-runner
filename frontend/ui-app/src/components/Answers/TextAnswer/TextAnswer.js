import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextArea from '../../TextArea/TextArea';
import 'bootstrap/dist/css/bootstrap.css';


class TextAnswer extends Component {
	constructor() {
		super();

		this.state = {
			value: '',
		};

		this.handleChange = this.handleChange.bind(this);
	}

	componentDidMount() {
		const {answers, currentPage} = this.props;
		if (answers[currentPage - 1]) {
			this.setState({
				value: answers[currentPage - 1].answer,
			});
		}
	}

	handleChange(event) {
		this.setState({
			value: event.target.value,
		});
		this.props.changeAnswer(event.target.value);
	}

	render() {
		const {name, maxLength, placeholder, rows} = this.props;
		return (
			<TextArea
				name={name}
				value={this.state.value}
				maxLength={maxLength}
				rows={rows}
				placeholder={placeholder}
				onChange={this.handleChange}
			/>
		);
	}
}

TextAnswer.propTypes = {
	name: PropTypes.string,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]),
	maxLength: PropTypes.number,
	placeholder: PropTypes.string,
	rows: PropTypes.number,
};

TextAnswer.defaultProps = {
	name: 'answer',
	value: '',
	maxLength: 40,
	placeholder: 'Напишите ответ',
	rows: 1,
};

export default TextAnswer;
