import React, {Component} from 'react';
import List from '../../List/List';
import ListItem from '../../List/ListItem';
import 'bootstrap/dist/css/bootstrap.css';


class ListAnswer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			checkedItems: [],
		};

		this.handleChecked = this.handleChecked.bind(this);
		this.renderListItem = this.renderListItem.bind(this);
	}

	componentDidMount() {
		const {answers, currentPage} = this.props;
		if (answers[currentPage - 1]) {
			const {data, answers, currentPage} = this.props;
			const ids = answers[currentPage -1].answer.split(' ');
			const checked = data.map((item, index) => {
				if (ids.indexOf(String(item.id)) > -1) {
					return index + 1;
				}
			});
			this.setState({
				checkedItems: checked,
			});
		}
	}

	handleChecked(items) {
		this.setState({
			checkedItems: items,
		});
		this.props.changeAnswer(items);
	}

	renderListItem(item, index, checkedItems, click) {
		return (
			<ListItem
				id = {index + 1}
				selectable = {true}
				multiple = {this.props.multiple}
				checkedItems = {checkedItems}
				className = "d-flex justify-content-between"
				onClick = {click}
			>
				{item.text}
			</ListItem>
		);
	}

	render() {
		return (
			<List
				data = {this.props.data}
				renderListItem = {this.renderListItem}
				handleChecked = {this.handleChecked}
				checkedItems = {this.state.checkedItems}
			/>
		);
	}
}

export default ListAnswer;
