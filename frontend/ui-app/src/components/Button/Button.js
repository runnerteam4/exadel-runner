import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';


const Button = (props) => {
	const {className, ...remain} = props;
	return (
		<button {...remain} className={classNames('btn btn-default', className)}>
			{props.loading ?
				'Loading...'
				: props.children
			}
		</button>
	);
};

Button.defaultProps = {
	type: 'submit',
	disabled: null,
	loading: null,
};

Button.propTypes = {
	type: PropTypes.oneOf(['submit', 'reset', 'button']),
	disabled: PropTypes.bool,
	loading: PropTypes.bool,
	onClick: PropTypes.func
};

export default Button;
