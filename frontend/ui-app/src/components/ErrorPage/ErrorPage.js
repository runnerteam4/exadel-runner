import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {NavLink} from 'react-router-dom';
import Button from "../Button/Button";


const Error = (props) => {
	const {className, textError} = props;

	return (
		<div className={classNames('d-flex align-items-center justify-content-center mt-5', className)}>
			<div>
				<h1 className="text-center font-weight-bold">Oops...</h1>
				<p className="text-center">{`${textError}`}</p>
				<NavLink to="/app">
					<Button>На главную страницу</Button>
				</NavLink>
			</div>
		</div>
	);
};

Error.defaultProps = {
	textError: 'Что-то пошло не так'
};

Error.propTypes = {
	textError: PropTypes.string
};

export default Error;
