import React, {Component} from 'react';
import {connect} from "react-redux";
import Navbar from "./Navbar";


class CommonNavBar extends Component {
	render() {
		const role = sessionStorage.getItem('role');
		const teacherData = [ {navTitle :'Тестирование', navLink: '/app/tests'},
			{navTitle :'Задачи', navLink:'/app/tasks'},
			{navTitle :'Материалы', navLink:'/app/materials'},
			{navTitle :'Группы', navLink:'/app/groups'},
			{navTitle: 'Студенты', navLink: '/app/students/all'}];

		const adminData = [ {navTitle :'Тестирование', navLink: '/app/tests'},
			{navTitle :'Задачи', navLink:'/app/tasks'},
			{navTitle :'Материалы', navLink:'/app/materials'},
			{navTitle :'Группы', navLink:'/app/groups'},
			{navTitle :'Пользователи', navLink:'/app/users/all'},
			{navTitle: 'Студенты', navLink: '/app/students/all'}];

		const studentData = [ {navTitle :'Тестирование', navLink: '/app/tests'},
			{navTitle :'Задачи', navLink:'/app/tasks'},
			{navTitle :'Материалы', navLink:'/app/materials'}];
		if (role === 'ROLE_TEACHER' && (sessionStorage.getItem('isConfirm')!=='false')){
			return <Navbar contents={teacherData}/>
		}
		if(role === 'ROLE_ADMIN'){
			return <Navbar contents={adminData}/>
		}
		if(role ==='ROLE_STUDENT' || (role === 'ROLE_TEACHER' && (sessionStorage.getItem('isConfirm')==='false'))){
			return <Navbar contents={studentData}/>
		}
		else {
			return null;
		}
	}
}
const mapStateToProps = ({login}) => ({
	role: login.role
});

export default connect(mapStateToProps)(CommonNavBar);
