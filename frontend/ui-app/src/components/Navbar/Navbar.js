import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Nav from './Nav.js';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';

export default class Navbar extends Component {
    constructor() {
        super();
        this.state = {
            indexActive: -1
        }
    }
    setActiveNav = (index) => {
        this.setState({indexActive: index});
    };
    getNavElement = (item, i) => {
        return <Nav onClick={this.setActiveNav} link = {item.navLink} key = {i}
                    title = {item.navTitle} keyIndex = {i}
                    active={this.state.indexActive === i}/>
    };
    render() {
        const {contents, className} = this.props;
        return <div className = {classNames('navbar navbar-dark bg-dark d-flex justify-content-around',className)}>
            {contents.map(this.getNavElement)}
            </div>;
    }
}

const NavItemModel = {
    navTitle: PropTypes.string,
    href: PropTypes.string,
};

Navbar.propTypes = {
    contents: PropTypes.arrayOf(PropTypes.shape(NavItemModel)),
    className: PropTypes.string
};

Navbar.defaultProps = {
    contents: [ {navTitle :'Тестирование', navLink: '/app/tests'},
		{navTitle :'Задачи', navLink:'/app/tasks'},
		{navTitle :'Материалы', navLink:'/app/materials'},
		{navTitle :'Группы', navLink:'/app/groups'}]
};