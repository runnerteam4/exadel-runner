import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';

export default class Nav extends Component{
    changeNav = () => {
        this.props.onClick(this.props.keyIndex);
    };
    render(){
        const classes = classNames('nav-link text-white',this.props.className, {active: this.props.active});
        const textSize = (this.props.active ? 'h3' : '');
        return <NavLink to={this.props.link} key = {this.props.keyIndex} onClick = {this.changeNav} className = {classes}>
            <h5 className={textSize}>{this.props.title}</h5>
        </NavLink>;
    }
}

Nav.propTypes = {
    onClick: PropTypes.func,
    href: PropTypes.string,
    title: PropTypes.string,
    keyIndex: PropTypes.number,
    active: PropTypes.bool,
    className: PropTypes.string
};

Nav.defaultProps = {
    href: '#',
    title: 'title',
    keyIndex: 0,
    active: false
};