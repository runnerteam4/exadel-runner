import React from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons/faTimes';
import './uploader.css';
import Button from "../Button/Button";


class FileUploader extends React.Component {
	pushFiles = () => {
		this.props.pushFiles();
	};

	chooseFiles = (event) => {
		const files = Object.values(event.target.files);
		const regexp = new RegExp(`\w*\.${this.props.fileTypes}`, 'i');
		const filtFiles = files.filter((file) => {
			return file.size < 1e+6 && regexp.test(file.name);
		});
		const limitFiles = filtFiles.slice(0, this.props.filesNum);

		this.props.handleFiles(limitFiles, this.props.index);
	};

	removeFile = (event) => {
		const newListFiles = this.props.upFiles.slice();
		const fileName = event.target.parentElement.dataset.name;
		const idx = newListFiles.findIndex(file => file.name === fileName);
		newListFiles.splice(idx, 1);

		this.props.handleFiles(newListFiles, this.props.index);
	};

	render() {
		const files = this.props.upFiles;
		const listFiles = files.length
			? files.map((file, index) =>
				<li data-name={file.name}
					key={index} className="list-group-item no-dots">
					<FontAwesomeIcon icon={faTimes}
									 className="remove-cursor remove-icon-position mr-2"
									 onClick={this.removeFile}/>
					{file.name}
				</li>
			)
			:
			<li className="list-group-item">
				Файлы не загружены
			</li>;

		return (
			<React.Fragment>
				<div className="btn btn-outline-dark uploader">
					Выбрать файлы
					<input type="file"
						   title=" "
						   multiple
						   onInput={this.chooseFiles}
					       required
					/>
				</div>
				<Button type="submit"
						className={'ml-4' + (this.props.displayButton ? '' : ' d-none')}
						onClick={this.pushFiles}
						disabled={!Object.keys(this.props.upFiles).length}
				>Отправить</Button>
				<ul className="list-group list-group-flush">
					<li className="font-weight-light small no-dots">
						Максимальное число прикрепленных файлов: {this.props.filesNum}
					</li>
					{listFiles}
				</ul>
			</React.Fragment>
		);
	}
}

FileUploader.defaultProps = {
	filesNum: 1,
	index: 0,
};

FileUploader.propTypes = {
	filesNum: PropTypes.number,
	index: PropTypes.number,
};

export default FileUploader;
