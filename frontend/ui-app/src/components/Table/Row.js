import React, {Component} from 'react';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';

export default class Row extends Component{
	renderItemElement = (header,index) => {
		return <td key={index}>
			{this.props.item[header.key]}
		</td>
	};
	render(){
		return <tr >
			{this.props.headers.map(this.renderItemElement)}
		</tr>;
	}
}

Row.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};