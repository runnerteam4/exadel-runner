import React, {Component} from 'react';
import Row from './Row';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';

export default class Table extends Component {
	renderItem = (item, index) => {
		return this.props.renderRow(item, index, this.props.headers);
	};
	render() {
		const {headers, data, hideHeaders, className} = this.props;
		const name = (hideHeaders)? 'd-none' : '';
		return <table className={classNames('table table-hover',className)}>
			<thead>
			<tr className={name} >
				{
					headers && headers.map((header, index) =>
						(<th key={index}>{header.label}</th>))
				}
				</tr>
			</thead>
			<tbody>
			{data.map(this.renderItem)}
			</tbody>
		</table>;
	}
}

Table.propTypes = {
	headers: PropTypes.array,
	data: PropTypes.array,
	renderRow: PropTypes.func,
	hideHeaders: PropTypes.bool
};

const renderRow = (item, index, headers) => {
	return <Row item={item} key={index} headers={headers}/>;
};

Table.defaultProps = {
	headers: [],
	data: [],
	renderRow: renderRow,
	hideHeaders: false
};