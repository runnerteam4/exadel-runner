import React from 'react';
import Dropdown from '../Dropdown/Dropdown';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import './css/index.css';
import { Link } from 'react-router-dom';
import {connect} from "react-redux";
import {request} from "../../actions/fetchWrapper";


class Header extends React.Component {
	render() {
		const firstName = this.props.firstName || sessionStorage.getItem('firstName');
		const lastName = this.props.lastName || sessionStorage.getItem('lastName');
		const user = `${firstName} ${lastName}`;


		const logOut = async () => {
			await request('logout', {method: 'POST', headers: {'Content-type': 'application/json'}});
			sessionStorage.clear();
			window.location.href = '/';
		};

		const dropDownOptions = (sessionStorage.getItem('role') || this.props.role) ?
			[
				{
					text: user,
					link: '/app/profile/' + (sessionStorage.getItem('id') || this.props.id),
				},
				{
					text: 'Выйти',
					link: this.props.linkLogOut,
					onClick: logOut,
				}
			] :
			[
				{
					text: 'Войти',
					link: this.props.linkSignIn
				},
				{
					text: 'Зарегистрироваться',
					link: this.props.linkSignUp
				}
			];

		const tabs = (
			<div className="nav d-none d-sm-flex">
				{
					dropDownOptions.map((option, index) =>
						<li key={index} className="nav-link pb-0 pt-0">
							<Link to={option.link} onClick={option.onClick} className="nav-link text-light">{option.text}</Link>
						</li>
					)
				}
			</div>
		);

		return (
			<header className="navbar header-color">
				<Link className="text-light" to="/" style={{fontFamily: 'Mistral' , fontSize: '2rem', textDecoration: 'none' }}>JDline</Link>
				<React.Fragment>
					{tabs}
					<Dropdown options={dropDownOptions}/>
				</React.Fragment>
			</header>
		);
	}
}

Header.defaultProps = {
	userName: null,
	linkProfile: null,
	linkLogOut: null,
	linkSignIn: null,
	linkSignUp: null
};

Header.propTypes = {
	userName: PropTypes.string,
	linkProfile: PropTypes.string,
	linkLogOut: PropTypes.string,
	linkSignIn: PropTypes.string,
	linkSignUp: PropTypes.string
};


const mapStateToProps = ({login}) => ({
	firstName: login.firstName,
	lastName: login.lastName,
	role: login.role,
	id: login.id
});


export default connect(mapStateToProps)(Header);

