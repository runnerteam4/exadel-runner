import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {PrivateRoute} from './PrivateRoute';
import CommonNavBar from '../Navbar/CommonNavBar';
import './style.css';
import Groups from '../../views/GroupsView/Groups';
import Group from '../../views/GroupsView/Group';
import TestTeacher from '../../views/TestsTeacher/TestsTeacher';
import TestCheck from '../../components/TestCheck/TestCheck';
import NewGroup from "../../views/GroupsView/NewGroup";
import AddStudent from "../../views/GroupsView/AddStudent";
import TaskList from "../../views/TaskList/TaskList";
import TaskView from "../../views/TaskView/TaskView";
import TestsListView from "../../views/TestsListView/TestsListView";
import TestPassedStudent from "../../views/TestPassedStudent/TestPassedStudent";
import TestView from "../../views/TestView/TestView";
import Profile from "../../views/Profile/Profile";
import commonMaterialsView from "../../views/MaterialsView/commonMaterialsView";
import AdminView from "../../views/AdminView/AdminView";
import TeacherRequest from "../../views/AdminView/TeacherRequest";
import TaskTeacher from "../../views/TaskTeacher/TaskTeacher";
import TaskTeacherShowAll from "../../views/TaskTeacher/TaskTeacherShowAll";
import StartPage from "../../views/StartPage/StartPage";
import Students from "../../views/Students/Students";
import ShowTask from "../../views/TaskTeacher/ShowTask";

const MainRoute = () => {
	const auth = sessionStorage.getItem('authorized');
	const role = sessionStorage.getItem('role');
	return (
		<React.Fragment>
			<CommonNavBar/>
			<div className='d-flex flex-column pl-4 mt-5 pr-4 pb-4 scrolling'>
				<Switch>
					<PrivateRoute authed={auth} exact path='/app/groups/group/addStudent/:id' component={AddStudent}/>
					<PrivateRoute authed={auth} exact path='/app/tests/check/:id' component={TestCheck}/>
					<PrivateRoute authed={auth} exact path='/app/tests' component={(role === 'ROLE_TEACHER' && (sessionStorage.getItem('isConfirm')!=='false')) || role === 'ROLE_ADMIN' ? TestTeacher : TestsListView}/>
					<PrivateRoute authed={auth} exact path='/app/tests/passed' component={TestPassedStudent}/>
					<PrivateRoute authed={auth} exact path='/app/tests/:id' component={TestView}/>
					<PrivateRoute authed={auth} path='/app/materials' component={commonMaterialsView}/>
					<PrivateRoute authed={auth} exact path='/app/tasks/task/:id' component={ShowTask}/>
					<PrivateRoute authed={auth} exact path='/app/tasks/all' component={TaskTeacherShowAll}/>
					<PrivateRoute authed={auth} exact path='/app/tasks/:id' component={TaskView}/>
					<PrivateRoute authed={auth} exact path='/app/tasks' component={(role === 'ROLE_TEACHER' && (sessionStorage.getItem('isConfirm')!=='false'))|| role === 'ROLE_ADMIN' ? TaskTeacher : TaskList}/>
					<PrivateRoute authed={auth} exact path='/app/groups/group/:id' component={Group}/>
					<PrivateRoute authed={auth} path='/app/groups/new' component={NewGroup}/>
					<PrivateRoute authed={auth} path='/app/groups' component={Groups}/>
					<PrivateRoute authed={auth} exact path='/app/profile/:id' component={Profile}/>
					<PrivateRoute authed={auth} path='/app/users/all' component={AdminView}/>
					<PrivateRoute authed={auth} path='/app/users/request' component={TeacherRequest}/>
					<PrivateRoute authed={auth} path='/app/students/all' component={Students}/>
					<Route path='/' component={StartPage}/>
				</Switch>
			</div>
		</React.Fragment>
	);
};

export default MainRoute;
