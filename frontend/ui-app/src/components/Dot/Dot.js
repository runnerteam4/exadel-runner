import React from 'react';
import classNames from 'classnames';
import './css/style.css';

const Dot = ({value, className, ...remain}) => {
	return (
		<div {...remain}  className={classNames(className, 'dot')}>
			{value}
		</div>
	);
};

export default Dot;
