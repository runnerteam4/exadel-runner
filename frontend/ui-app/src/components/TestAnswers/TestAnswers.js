import React from 'react';
import Table from '../Table/Table';
import TestAnswersRow from './TestAnswersRow';
import Loading from '../Loading/Loading';
import ErrorPage from '../ErrorPage/ErrorPage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {getTestAnswers} from '../../actions/TestAnswers/action';
import Button from "../Button/Button";

class TestAnswers extends React.Component {
	constructor() {
		super();

		this.state = {
			isPending: true,
			error: '',
		};
	}

	async componentDidMount() {
		const error = await this.props.getTestAnswers();

		this.setState({
			isPending: false,
			error,
		});
	}

	render() {
		const renderRow = (item, index, headers) => {
			return <TestAnswersRow item={item} key={index} headers={headers} numberRow={index + 1}/>;
		};

		const {currentData} = this.props;
		const headers = [
			{key:'number', label: 'Номер'},
			{key:'name', label: 'Вопрос'},
			{key:'result', label: 'Оценка'}
		];

		return (
			<React.Fragment>
				<div className='modal-body scrollingModal'>
					<Table
						className="mt-4"
						headers={headers}
						data={currentData}
						renderRow={renderRow}/>
					{
						this.state.isPending &&
						<div className="d-flex h-50">
							<Loading/>
						</div>
					}
					{
						this.state.error && (<ErrorPage textError={this.state.error}/>)
					}
				</div>
				<div className='modal-footer'>
					<Button onClick={this.props.toggle} className="mt-2 justify-content-end">Закрыть</Button>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({testAnswers}) => ({
	currentData: testAnswers.testAnswers.currentData,
});

const mapDispatchToProps = (dispatch) => ({
	getTestAnswers: bindActionCreators(getTestAnswers, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TestAnswers);


