import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './css/index.css';

export default class TestAnswersRow extends Component {
	renderItemElement = (header, index) => {
		switch (header.label) {
		case 'Номер':
			return (
				<td key={index}
					className="text-center">
					{this.props.numberRow}
				</td>
			);
		case 'Вопрос':
			return (
				<td key={index}>
					<details className="details-style">
						<summary className="summary-style">Просмотреть вопрос...</summary>
						{this.props.item[header.key]}
					</details>
				</td>
			);
		case 'Оценка':
			const classColor = this.props.item[header.key] === 'Принято' ? 'bg-success' : 'bg-danger';
			return (
				<td key={index}>
					<span className={classNames('p-1 rounded', classColor)}>
						{this.props.item[header.key]}
					</span>
				</td>
			);
		default:
			return (
				<td key={index}>
					{this.props.item[header.key]}
				</td>
			);
		}
	};

	render() {
		return (
			<tr>
				{this.props.headers.map(this.renderItemElement)}
			</tr>
		);
	}
}

TestAnswersRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};
