import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import {changeInputValue} from '../../actions/TestCheck/action';
import Input from '../../components/Input/Input';
import TextArea from '../TextArea/TextArea';
import './css/index.css';


class TestsTeacherRow extends Component {
	changeInputValue = (event) => {
		const regex = /^(?:[1-9]|0[1-9]|10)$/;
		if (regex.test(event.target.value)) {
			this.props.changeInputValue(this.props.answerId, event.target.value);
		}
		else {
			this.props.changeInputValue(this.props.answerId, false);
		}
	};

	render() {
		const {answers, answerId} = this.props;

		return (
			<div className="was-validated border-top border-dark bd-color">
				<div className="d-flex mt-2">
					<div className="w-100 mr-1">
						<h5>Вопрос</h5>
						<TextArea className="mb-1 question-answer-size"
								  value={this.props.data.question}
								  readOnly
						/>
					</div>
					<div className="w-100 ml-1">
						<h5>Ответ</h5>
						<TextArea className="mb-4 question-answer-size"
								  value={this.props.data.answer}
								  readOnly
						/>
					</div>
				</div>
				<div className="d-flex">
					<h5>Оценка: </h5>
					<Input
						className="text-center mb-5 ml-4 input-size"
						id={`${this.props.answerId}`}
						value={answers[answerId].mark}
						onChange={this.changeInputValue}
						pattern="^(?:[1-9]|0[1-9]|10)$"
						required
					/>
				</div>
			</div>
		);
	}
}

TestsTeacherRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};

const mapStateToProps = ({testCheck}) => ({
	answers: testCheck.answers,
});
const mapDispatchToProps = (dispatch) => ({
	changeInputValue: bindActionCreators(changeInputValue, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(TestsTeacherRow);
