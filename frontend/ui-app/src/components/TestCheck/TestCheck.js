import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getAnswers, approveCheck} from '../../actions/TestCheck/action';
import {changeInputValue, changeCommentValue} from '../../actions/TestCheck/action';
import TestsCheckRow from './TestCheckRow';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';
import Button from '../../components/Button/Button';
import TextArea from '../TextArea/TextArea';
import './css/index.css';


class TestCheck extends React.Component {
	constructor() {
		super();
		this.state = {
			isPending: true,
			error: '',
		};
	}

	async componentDidMount() {
		const res = await this.props.getAnswers(this.props.match.params.id);
		const error = res.error;

		this.setState({
			isPending: false,
			error,
		});
	}

	changeCommentValue = (event) => {
		this.props.changeCommentValue(this.props.answerId, event.target.value);
	};

	approveCheck = () => {
		this.props.approveCheck(this.props.match.params.id);
		this.props.history.push('/app');
	};

	noReload = (event) => {
		event.preventDefault();
	};

	render() {
		const {answers} = this.props;

		return (
			<React.Fragment>
				{!this.state.error &&
				<form onSubmit={this.noReload} autoComplete="off"> {/*need for error message in input fields*/}
					{answers.map((answer, index) => {
						return (
							<TestsCheckRow key={index} data={answer} answerId={index}/>
						);
					})}
					<div className="d-flex justify-content-between align-items-center mt-2">
						<div className="w-50">
							<h5>Комментарий</h5>
							<TextArea
								value={this.props.comment}
								maxLength={200}
								onChange={this.changeCommentValue}
								className="mb-4 comment-size"/>
						</div>
						<Button
							className="button-size"
							onClick={() => {
								return false;
							}}
							onMouseUp={this.approveCheck}>
							Закончить проверку
						</Button>
					</div>
				</form>
				}
				{
					this.state.isPending &&
					<div className="d-flex h-50">
						<Loading/>
					</div>
				}
				{
					this.state.error && (<ErrorPage textError={this.state.error}/>)
				}
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({testCheck}) => ({
	answers: testCheck.answers,
	comment: testCheck.comment,
});
const mapDispatchToProps = (dispatch) => ({
	getAnswers: bindActionCreators(getAnswers, dispatch),
	approveCheck: bindActionCreators(approveCheck, dispatch),
	changeInputValue: bindActionCreators(changeInputValue, dispatch),
	changeCommentValue: bindActionCreators(changeCommentValue, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(TestCheck);
