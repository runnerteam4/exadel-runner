import {Component} from 'react';
import Pagination from './Pagination';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { onChangeChunk, onPrevious, onNext} from './actionPagination';
import List from '../List/List';
import React from 'react';



class GroupPagination extends Component {
	render() {
		const {data, currentPage, chunkSize, onNext, onPrevious,onChangeChunk, currentData, renderListItem} = this.props;
		return <div>
			<Pagination totalPageNumber = {data.length}
						currentPage = {currentPage}
						chunkSize = {chunkSize}
						chunkSizes = {[2,5,10]}
						onNext = {onNext}
						onPrevious = {onPrevious}
						onChangeChunk = {onChangeChunk}/>
			<List data = {currentData} renderListItem = {renderListItem}/>
		</div>;
	}
}

const mapStateToProps = (state) => ({
	data: state.data,
	currentData: state.currentData,
	chunkSize: state.chunkSize,
	currentPage: state.currentPage
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(GroupPagination);