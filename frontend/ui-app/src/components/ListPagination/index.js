import React, {Component} from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import * as ReactDOM from 'react-dom';
import GroupPagination from './GroupPagination';

import store from './PaginationReducer';

const appStore = createStore(store);

function App() {
	return (
		<Provider store={appStore}>
			<div className="App">
				<GroupPagination/>
			</div>
		</Provider>
	);
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App/>, rootElement);