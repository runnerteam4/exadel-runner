const data = [
	{text: 'Тестирование'},
	{text: 'Задача'},
	{text: 'Материалы'},
	{text: 'Группы'},
	{text: 'Тестирование1'},
	{text: 'Задача1'},
	{text: 'Материалы1'},
	{text: 'Группы1'}
];

const initialState = {
	data: data,
	currentPage: 1,
	currentData: [data[0],data[1]],
	chunkSize: 2
};

const makeCurrentData = (currentPage,chunkSize, data) => {
	const indexOfLastTodo = currentPage * chunkSize;
	const indexOfFirstTodo = indexOfLastTodo - chunkSize;
	return data.slice(indexOfFirstTodo, indexOfLastTodo);
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case 'CHANGE_CHUNK':
			return { ...state, currentPage: 1, chunkSize: action.payload,
				currentData: makeCurrentData(1,action.payload, state.data)};
		case 'ON_NEXT':
			return { ...state, currentPage: state.currentPage + 1,
				currentData: makeCurrentData(state.currentPage + 1,state.chunkSize, state.data)};
		case 'ON_PREVIOUS':
			return { ...state, currentPage: state.currentPage - 1,
				currentData: makeCurrentData(state.currentPage - 1,state.chunkSize, state.data)};
		default:
			return state;
	}
}