import React, {Component} from 'react';
import Button from "../Button/Button";
import PropTypes from 'prop-types';


export default class Pagination extends Component {
	onChangeChunk = (event) => {
		this.props.onChangeChunk(Number(event.target.id));
	};

	renderCountItem = () => {
		const countOfElementOnThePage = this.props.chunkSizes || [2, 5, 10];
		return countOfElementOnThePage.map(item => {
			return (
				<Button className="mr-1 ml-1"
					key={item}
					id={item}
					onClick={this.onChangeChunk}>
					{item}
				</Button>
			);
		});
	};
	renderPageNumbers = () => {
		const {currentPage, onPrevious, onNext, chunkSize, totalPageNumber} = this.props;
		const countOfPages = Math.ceil(totalPageNumber / chunkSize);
		const previousPage = currentPage - 1;
		const nextPage = currentPage + 1;
		return (
			<div className={'d-flex justify-content-between'}>
				<Button disabled={previousPage <= 0}
						key={previousPage}
						id={previousPage}
						onClick={onPrevious}
						className={'btn btn-outline-dark'}>
					previous
				</Button>
				<ul className={'align-self-center mr-5'}>{currentPage} из {countOfPages}</ul>
				<Button disabled={nextPage > countOfPages}
						key={nextPage}
						id={nextPage}
						onClick={onNext}
						className={'btn btn-outline-dark'}>
					next
				</Button>
			</div>
		);
	};

	render() {
		return (
			<div className={'d-flex justify-content-between mt-4'}>
				<ul id="page-numbers">
					{this.renderPageNumbers()}
				</ul>
				<div className={'col-3'}>
					{this.renderCountItem()}
				</div>
			</div>
		);
	}
}

Pagination.propTypes = {
	currentPage: PropTypes.number,
	onPrevious: PropTypes.func,
	onNext: PropTypes.func,
	onChangeChunk: PropTypes.func,
	chunkSize: PropTypes.number,
	totalPageNumber: PropTypes.number,
	chunkSizes: PropTypes.array
};

Pagination.defaultProps = {
	currentPage: 1,
	onPrevious: () => {},
	onNext: () => {},
	onChangeChunk: () => {},
	chunkSize: 2,
	totalPageNumber: 0,
	chunkSizes: [2, 5, 10]
};
