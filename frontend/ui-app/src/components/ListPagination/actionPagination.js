export  function onChangeChunk (count){
	return {
		type: 'CHANGE_CHUNK',
		payload: count
	};
}
export  function onPrevious(){
	return {
		type: 'ON_PREVIOUS'
	};
}

export function onNext(){
	return {
		type: 'ON_NEXT'
	};
}
