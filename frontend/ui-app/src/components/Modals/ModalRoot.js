import React, {Component} from 'react';
import {connect} from 'react-redux';
import AddMaterial from './modalComponents/AddMaterial';
import AddTask from './modalComponents/AddTask';
import AddTest from './modalComponents/AddTest';
import AssignTest from './modalComponents/AssignTest';
import AssignTask from './modalComponents/AssignTask';
import AddCategory from './modalComponents/AddCategory';
import {
	hideModal, sendAssigntask, sendAssigntest,
	sendNewQuestion, sendNewTask, sendAdditionalTaskTest,
} from '../../actions/Modals/ModalAction';
import {update} from '../../actions/Registration/action';
import {bindActionCreators} from 'redux';
import Modal from './modalComponents/Modal';
import EditGroupName from "./modalComponents/EditGroupName";
import {sendNewGroupName, deleteGroup} from "../../actions/Groups/GroupsActions";
import SimpleModal from './modalComponents/SimpleModal';
import {excludeStudent} from "../../actions/Groups/GroupActions";
import RegistrationSuccess from "./modalComponents/RegistrationSuccess";
import RegistrationFailed from "./modalComponents/RegistrationFailed";
import TestAnswers from "../TestAnswers/TestAnswers";
import ModalWaitingCheck from "./modalComponents/ModalWaitingCheck";
import ChangePassword from "./modalComponents/ChangePassword";
import {addMaterial, addCategory} from "../../actions/Materials/MaterialsAction";
import {changePassword} from "../../actions/Profile/action";
import addTestFile from "./modalComponents/addTestFile";
import {deleteTask} from "../../actions/TaskTeacher/action";

class ModalRoot extends Component {

	render() {
		const MODAL_COMPONENTS = {
			'ADD_TEST': [AddTest, 'Добавление вопроса', this.props.sendNewQuestion],
			'ADD_TASK': [AddTask, 'Добавление задачи', this.props.sendNewTask],
			'ADD_MATERIAL': [AddMaterial, 'Добавление материала', this.props.sendNewMaterial],
			'ADD_CATEGORY': [AddCategory, 'Добавление раздела', this.props.sendNewCategory],
			'ASSIGN_TASK': [AssignTask, 'Назначение задачи', this.props.sendAssigntask],
			'ASSIGN_TEST': [AssignTest, 'Назначение теста', this.props.sendAssigntest],
			'REGISTRATION_SUCCESS': [RegistrationSuccess, 'Регистрация завершена', this.props.hideModal],
			'REGISTRATION_FAILED': [RegistrationFailed, 'Ошибка при регистрации', this.props.update],
			'ANSWERS': [TestAnswers, 'Просмотр ответов', this.props.toggle],
			'EDIT_GROUP_NAME': [EditGroupName, 'Изменение названия группы', this.props.sendNewGroupName],
			'CONFIRM_DELETING': [SimpleModal, '', this.props.deleteGroup],
			'CONFIRM_EXCLUDE_STUDENT': [SimpleModal, '', this.props.excludeStudent],
			'CONFIRM_DELETE_TASK' : [SimpleModal, '', this.props.deleteTask],
			'WAITING_CHECK': [ModalWaitingCheck, 'Тест пройден', this.props.toggle],
			'CHANGE_PASSWORD': [ChangePassword, 'Изменение пароля',this.props.changePassword],
			'ADD_TASKfiLE': [addTestFile, 'Добавление файла',this.props.sendAdditionalTaskTest],
		};
		if (!this.props.modalType) {
			return null;
		}
		const [SpecificModal, title, onClick] = MODAL_COMPONENTS[this.props.modalType];
		const renderBody = () => {
			return <SpecificModal {...this.props.modalProps} toggle={this.props.toggle} onClick={onClick}/>;
		};
		return <Modal isActive={true} toggle={this.props.toggle}
					  title={title || this.props.modalProps.text} renderBody={renderBody}/>;
	}
}

const mapStateToProps = ({ModalReducer}) => ({
	modalType: ModalReducer.modalType,
	modalProps: ModalReducer.modalProps
});

const mapDispatchToProps = (dispatch) => ({
	sendAssigntest: bindActionCreators(sendAssigntest, dispatch),
	sendAssigntask: bindActionCreators(sendAssigntask, dispatch),
	sendNewCategory: bindActionCreators(addCategory, dispatch),
	sendNewMaterial: bindActionCreators(addMaterial, dispatch),
	sendNewQuestion: bindActionCreators(sendNewQuestion, dispatch),
	sendNewTask: bindActionCreators(sendNewTask, dispatch),
	toggle: bindActionCreators(hideModal, dispatch),
	hideModal: bindActionCreators(hideModal, dispatch),
	update: bindActionCreators(update, dispatch),
	sendNewGroupName: bindActionCreators(sendNewGroupName, dispatch),
	deleteGroup: bindActionCreators(deleteGroup, dispatch),
	excludeStudent: bindActionCreators(excludeStudent, dispatch),
	changePassword:bindActionCreators(changePassword, dispatch),
	deleteTask: bindActionCreators(deleteTask, dispatch),
	sendAdditionalTaskTest: bindActionCreators(sendAdditionalTaskTest, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalRoot);