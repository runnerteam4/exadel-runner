import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';

export default class AddCategory extends React.Component {
	constructor() {
		super();
		this.state = {
			category: ''
		}
	}
	onChange = (e) => {
		this.setState(
			{category: e.target.value}
		)
	};
	onHandleClick = async () => {
		await this.props.onClick(this.state.category);
	};
	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='button' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='button' className='btn btn-dark' onClick={this.onHandleClick}>сохранить</Button>
		</div>;
	};

	render() {
		return <div>
			<div className='modal-body'>
				<div className='mt-3'>
					<Input value={this.state.category} label={'Название раздела:'} onChange={this.onChange}/>
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

AddCategory.propTypes = {
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

AddCategory.defaultProps = {
	toggle: () => {},
	onClick: () => {}
};

