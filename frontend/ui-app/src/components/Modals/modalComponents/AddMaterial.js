import React, {Component} from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';

export default class AddMaterial extends React.Component {
	constructor() {
		super();
		this.state = {
			name: '',
			link: '',
			selectedCategory: []
		}
	}

	onHandleClick = () => {
		const form = document.getElementsByName('modalForm')[0];
		if (form.checkValidity()) {
			this.props.onClick(this.state.link, sessionStorage.getItem('id'),
				this.state.name, this.state.selectedCategory.value);
		}
	};

	onChangeSelect = (e) => {
		this.setState(
			{selectedCategory : e}
		);
	};

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};
	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='submit' className='btn btn-dark' onClick={this.onHandleClick}>сохранить</Button>
		</div>;
	};

	render() {
		const {selectedCategory, name, link} = this.state;
		const {category} = this.props;
		return (
			<div>
				<div className='modal-body scrollingModal'>
					<Select
						className={'w-100'}
						placeholder={'Выберите категорию...'}
						value={selectedCategory}
						onChange={this.onChangeSelect}
						isMulti={false}
						options={category}
					/>
					<div className='mt-3'>
						<Input
							value={name}
							name="name"
							label="Название материала:"
							onChange={this.onChange}
							maxLength="100"
							required
						/>
					</div>
					<div className='mt-2'>
						<Input
							value={link}
							name="link"
							label="Ссылка:"
							onChange={this.onChange}
							maxLength="200"
							required
						/>
					</div>
				</div>
				<div className='modal-footer'>
					{this.renderFooter()}
				</div>
			</div>);
	}
}

AddMaterial.propTypes = {
	category: PropTypes.array,
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

AddMaterial.defaultProps = {
	category: [
		{value: 'one', label: 'Раздел1'},
		{value: 'two', label: 'Раздел2'},
	],
	toggle: () => {},
	onClick: () => {}
};