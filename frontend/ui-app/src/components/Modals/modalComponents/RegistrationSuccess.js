import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';
import {Link} from 'react-router-dom';

export default class RegistrationSuccess extends Component {
	renderFooter = () => {
		return (
			<div className='d-inline-flex justify-content-around'>
				<Link to='/'>
					<Button type='button' className='btn btn-dark' onClick={this.props.onClick}>Ок</Button>
				</Link>
			</div>
		);
	};

	render() {
		return (
			<div>
				<div className='modal-body'>
					<div className='mt-3'>
						<p>{this.props.text}</p>
					</div>
				</div>
				<div className='modal-footer'>
					{this.renderFooter()}
				</div>
			</div>
		);
	}
}

RegistrationSuccess.propTypes = {
	onClick: PropTypes.func,
};

RegistrationSuccess.defaultProps = {
	onClick: () => {},
};

