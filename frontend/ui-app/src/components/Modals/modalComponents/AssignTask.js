import React, {Component} from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';

export default class AssignTask extends React.Component {

	constructor() {
		super();
		this.state = {
			selectedTask: [],
			deadline: ''
		}
	}

	onChangeSelect = (e, name) => {
		this.setState(
			{[name]: e}
		);
	};

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};

	checkValidate = () => {
		const form = document.getElementsByName('modalForm')[0];
		let body = {};
		if (form.checkValidity()) {
			if (this.props.role === 'student') {
				body = {
					teacherId: Number(sessionStorage.getItem('id')),
					taskId: this.state.selectedTask.value,
					deadline: this.state.deadline,
					studentId: this.props.id
				}

			}else{
				body = {
					teacherId: Number(sessionStorage.getItem('id')),
					taskId: this.state.selectedTask.value,
					deadline: this.state.deadline,
					groupId: this.props.id
				}
			}
			this.props.onClick(body, this.props.role);
		}
	};

	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='submit' className='btn btn-dark' onClick={this.checkValidate}>сохранить</Button>
		</div>;
	};


	changeTaskField = (e) => {
		this.onChangeSelect(e, 'selectedTask');
	};

	render() {
		const {selectedTask, deadline} = this.state;
		const {tasks} = this.props;
		return <div>
			<div className='modal-body'>
				<Select
					className={'w-100'}
					placeholder={'Выберите задачу...'}
					value={selectedTask}
					onChange={this.changeTaskField}
					isMulti={false}
					options={tasks}
				/>
				<div className='mt-3'>
					<Input
						value={deadline}
						name="deadline"
						label="Дедлайн:"
						onChange={this.onChange}
						placeholder="чч.мм.гггг"
						pattern="(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)\d\d"
						title="Введите дедлайн в формате чч.мм.гггг"
						required
					/>
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

AssignTask.propTypes = {
	tasks: PropTypes.array,
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

AssignTask.defaultProps = {
	tasks: [
		{value: 'one', label: 'Раздел1'},
		{value: 'two', label: 'Раздел2'},
	],
	toggle: () => {
	},
	onClick: () => {
	}
};
