import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';

export default class EditGroupName extends React.Component {
	constructor() {
		super();
		this.state = {
			name: ''
		}
	}

	onChange = (e) => {
		this.setState(
			{name: e.target.value}
		)
	};

	noReload = (event) => {
		event.preventDefault();
	};

	checkValidation = () => {
		const form = document.getElementsByName('modalForm')[0];
		if (form.checkValidity()) {
			this.props.onClick(this.state.name, this.props.id);
		}
	};

	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='submit' className='btn btn-dark' onClick={this.checkValidation}>сохранить</Button>
		</div>;
	};

	render() {
		return <div>
			<div className='modal-body'>
				<Input
					type="text"
					placeholder="Введите новое название"
					value={this.state.name}
					onChange={this.onChange}
					maxLength="50"
					required
				/>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

EditGroupName.propTypes = {
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

EditGroupName.defaultProps = {
	toggle: () => {
	},
	onClick: () => {
	}
};

