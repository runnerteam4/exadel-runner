import React, {Component} from 'react';
import {connect} from 'react-redux';
import Select from 'react-select';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';
import {getCategories} from "../../../actions/TestList/actions";
import {bindActionCreators} from "redux";
import './modal.css';

class AddTest extends React.Component {

	constructor() {
		super();
		this.state = {
			countAnswers: 1,
			questions: [
				{value: '1', label: '1'},
			],
			selectedTopic: '',
			selectedCategory: '',
			selectedType: '',
			condition: '',
			tags: '',
			answers: [''],
			rightAnswers: [''],
			complexity: '',
			shortAnswer: '',
		}
	}

	componentDidMount = async () => {
		await this.props.getCategories();
	};

	onChangeSelect = (e, name) => {
		this.setState(
			{[name]: e}
		);
	};
	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};
	handleChangeAnswer = (e, index) => {

		this.state.answers[index] = e.target.value;
		this.setState(
			{
				answers: this.state.answers
			}
		);
	};
	onChangeShortAnswer = (e) => {
		this.setState({
			shortAnswer: e.target.value
		});
	};
	onClickDeleteButton = () => {
		const count = this.state.questions.length - 1;
		this.setState(
			{
				rightAnswers: [],
				answers: this.state.answers.slice(0, count),
				countAnswers: count,
				questions: this.state.questions.slice(0, count)
			}
		);
	};
	onClick = () => {
		const count = this.state.countAnswers + 1;
		this.setState(
			{
				countAnswers: count,
				questions: [...this.state.questions, {value: count.toString(10), label: count.toString(10)}]
			}
		);
		this.state.answers[count - 1] = '';
	};

	renderWithOneAnswer = (item, index) => {
		const changeAnswer = (e) => {
			this.handleChangeAnswer(e,index);
		};
		return <div className={'d-flex justify-content-between mt-3'}>
			<label>{index + 1})</label>
			<Input
				className={'ml-2'}
				value={item}
				key={index}
				onChange={changeAnswer}
				maxLength="300"
				required
			/>
		</div>
	};

	changeTopicField = (e) => {
		this.onChangeSelect(e,'selectedTopic');
	};
	changeCategoryField = (e) => {
		this.onChangeSelect(e,'selectedCategory');
	};
	changeRightAnswersField = (e) => {
		this.onChangeSelect(e,'rightAnswers')
	};
	changeTypeField = (e) => {
		this.onChangeSelect(e,'selectedType')
	};

	renderAnswersForRadio = () => {
		return <div>
			{this.state.answers.map(this.renderWithOneAnswer)}
			<div className={'d-flex justify-content-between mt-2'}>
				<button type='button' className='close' onClick={this.onClickDeleteButton}>
					<span aria-hidden='true'>&times;</span>
				</button>
				<Button onClick={this.onClick}>Добавить еще</Button>
				<label className={'align-self-center mt-1'}>Правильный вариант ответа:</label>
				<Select
					className={'w-10'}
					placeholder={''}
					value={this.state.rightAnswers}
					onChange={this.changeRightAnswersField}
					isMulti={false}
					options={this.state.questions}
				/>
			</div>
		</div>;
	};

	renderAnswersForCheckbox = () => {
		return <div>
			{this.state.answers.map(this.renderWithOneAnswer)}
			<div className={'d-flex justify-content-end mt-3'}>
				<button type='button' className='close' onClick={this.onClickDeleteButton}>
					<span aria-hidden='true'>&times;</span>
				</button>
				<Button onClick={this.onClick}>Добавить еще</Button>
			</div>
			<div>
				<label>Правильные варианты ответов:</label>
				<Select
					className={'w-10'}
					placeholder={''}
					value={this.state.rightAnswers}
					onChange={this.changeRightAnswersField}
					isMulti={true}
					options={this.state.questions}
				/>
			</div>
		</div>;
	};

	renderShortAnswer = () => {
		return (
			<div className={'mt-3'}>
				<label>Краткий ответ</label>
				<Input
					className={'ml-2'}
					value={this.state.shortAnswer}
					onChange={this.onChangeShortAnswer}
					placeholder="Введите правильный краткий ответ"
					maxLength="40"
					required
				/>
			</div>
		);
	};

	checkValidate = () => {
		const form = document.getElementsByName('modalForm')[0];
		if (form.checkValidity()) {
			let typeTest;
			if (this.state.selectedCategory.label === 'Контрольный') {
				typeTest = 0;
			}
			else if (this.state.selectedCategory.label === 'Тренировочный') {
				typeTest = 1;
			}
			else {
				typeTest = null;
			}
			const state = this.state;
			let answers = null;
			if (state.selectedType.value === 'LIST_MULTIPLE_ANSWER') {
				answers = state.answers.map((text) => {
					return {text, correct: false}
				});
				state.rightAnswers.forEach((item) => {
					answers[item.value - 1].correct = true;
				});
			}
			else if (state.selectedType.value === 'LIST_ONE_ANSWER') {
				answers = state.answers.map((text) => {
					return {text, correct: false}
				});
				answers[state.rightAnswers.value - 1].correct = true;
			}
			if (this.state.shortAnswer) {
				answers = [{text: this.state.shortAnswer, correct: 1}];
			}
			const body = {
				tags: [state.tags],
				topicId: state.selectedTopic.value,
				training: typeTest,
				type: state.selectedType.value,
				complexity: +state.complexity,
				authorId: sessionStorage.getItem('id'),
				answers,
				text: state.condition,
			};
			this.props.onClick(body);
		}
	};

	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='submit' className='btn btn-dark' onClick={this.checkValidate}>сохранить</Button>
		</div>;
	};

	render() {
		const {category, type, topic} = this.props;
		const {selectedCategory, selectedType, selectedTopic, tags, condition, complexity} = this.state;
		return <React.Fragment>
			<div className='modal-body scrollingModal'>
				<Select
					className={'w-100'}
					placeholder={'Выберите раздел...'}
					value={selectedTopic}
					onChange={this.changeTopicField}
					isMulti={false}
					options={topic}
					required
				/>
				<div className={'mt-3'}>
					<Select
						className={'w-100'}
						placeholder={'Выберите категорию вопроса...'}
						value={selectedCategory}
						onChange={this.changeCategoryField}
						isMulti={false}
						options={category}
					/>
				</div>
				<div className={'mt-2'}>
					<Input
						value={tags}
						name="tags"
						label="Введите теги:"
						onChange={this.onChange}
						maxLength="100"
						required
					/>
				</div>
				<div className={'mt-1'}>
					<Input
						value={condition}
						name="condition"
						label="Вопрос:"
						onChange={this.onChange}
						maxLength="300"
						required
					/>
				</div>
				<div className="mt-1">
					<Input
						type="number"
						value={complexity}
						name="complexity"
						label="Сложность:"
						placeholder="Введите коэффициент сложности вопроса от 1 до 4"
						onChange={this.onChange}
						min="1"
						max="4"
						required
					/>
				</div>
				<div className={'mt-3'}>
					<Select
						className={'w-100'}
						placeholder={'Выберите тип вопроса...'}
						value={selectedType}
						onChange={this.changeTypeField}
						isMulti={false}
						options={type}
					/>
				</div>
				{(selectedType.label === 'С одним вариантом ответа') && (this.renderAnswersForRadio())}
				{(selectedType.label === 'С несколькими вариантами ответа') && (this.renderAnswersForCheckbox())}
				{(selectedType.label === 'Краткий ответ') && (this.renderShortAnswer())}
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</React.Fragment>;
	}
}

const mapStateToProps = ({testList}) => ({
	topic: testList.categories.categories,
});

const mapDispatchToProps = (dispatch) => ({
	getCategories: bindActionCreators(getCategories, dispatch),
});

AddTest.propTypes = {
	category: PropTypes.array,
	topic: PropTypes.array,
	type: PropTypes.array,
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

AddTest.defaultProps = {
	topic: [
		{value: 'one', label: 'Раздел1'},
		{value: 'two', label: 'Раздел2'},
	],
	type: [
		{value: 'LIST_ONE_ANSWER', label: 'С одним вариантом ответа'},
		{value: 'LIST_MULTIPLE_ANSWER', label: 'С несколькими вариантами ответа'},
		{value: 'SHORT_ANSWER', label: 'Краткий ответ'},
		{value: 'LONG_ANSWER', label: 'Развернутый ответ'}
	],
	category: [
		{value: 'one', label: 'Контрольный'},
		{value: 'two', label: 'Тренировочный'},
	],
	toggle: () => {},
	onClick: () => {}
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTest);
