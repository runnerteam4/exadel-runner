import PropTypes from "prop-types";
import React from "react";
import Button from "../../Button/Button";
import Input from "../../Input/Input";
import {connect} from "react-redux";
import FileUploader from "../../FileUploader/FileUploader";


class addTestFile extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			upFiles: [
				{
					input: '',
					output: '',
				},
			],
		};

		this.addFileUploader = this.addFileUploader.bind(this);
		this.handleInputFiles = this.handleInputFiles.bind(this);
		this.handleOutputFiles = this.handleOutputFiles.bind(this);
	}

	checkValidate = async () => {
		const form = document.getElementsByName('modalForm')[0];
		if (form.checkValidity()) {
			const files = [];

			await this.state.upFiles.forEach((file, index) => {
				const reader = new FileReader();
				reader.onload = function() {
					files.push({
						input: reader.result,
						answer: '',
					});
				};
				reader.readAsText(this.state.upFiles[index].input[0], 'utf-8');
			});

			await this.state.upFiles.forEach((file, index) => {
				const reader = new FileReader();
				reader.onload = function() {
					files[index].answer = reader.result;
				};
				reader.readAsText(this.state.upFiles[index].output[0], 'utf-8');
			});

			this.props.onClick(this.props.taskId, files);
		}
	};

	addFileUploader() {
		this.setState((prevState, props) => {
			let files = prevState.upFiles.slice();
			files.push({
				input: '',
				output: '',
			});
			return {
				upFiles: files,
			}
		});
	}

	handleInputFiles(items, index) {
		this.setState((prevState, props) => {
			let files = prevState.upFiles.slice();
			files[index].input = items.length === 0? '': items;
			return {
				upFiles: files,
			}
		});

	}

	handleOutputFiles(items, index) {
		this.setState((prevState, props) => {
			let files = prevState.upFiles.slice();
			files[index].output = items.length === 0? '': items;
			return {
				upFiles: files,
			}
		});
	}

	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3'
					onClick={this.props.toggle}>Отмена</Button>
			<Button type='submit'
					onClick={this.checkValidate}>Сохранить</Button>
		</div>;
	};

	render() {
		return <div>
			<div className='modal-body'>
				<div className="d-inline-flex ml-2">
					<label>Выберите 2 файла: input и output</label>
				</div>
				<div className="ml-3 mt-2">
					{
						this.state.upFiles.map((item, index) => {
							return (
								<div className="row" key={index}>
									<div className="col">
										<FileUploader
											fileTypes='.txt'
											upFiles={this.state.upFiles[index].input}
											handleFiles={this.handleInputFiles}
											displayButton={false}
											index={index}
										/>
									</div>
									<div className="col text-right">
										<FileUploader
											fileTypes='.txt'
											upFiles={this.state.upFiles[index].output}
											handleFiles={this.handleOutputFiles}
											displayButton={false}
											index={index}
										/>
									</div>
								</div>
							);
						})
					}
					<div className="row">
						<div className="col text-center">
							<Button
								className="btn btn-dark"
								type="button"
								onClick={this.addFileUploader}
							>
								Добавить еще
							</Button>
						</div>
					</div>
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

const mapStateToProps = ({login}) => ({
	userID: login.userID,
});

addTestFile.propTypes = {
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

addTestFile.defaultProps = {
	toggle: () => {
	},
	onClick: () => {
	}
};

export default connect(mapStateToProps)(addTestFile);