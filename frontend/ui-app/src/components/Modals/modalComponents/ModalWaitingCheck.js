import PropTypes from "prop-types";
import React from "react";
import {NavLink} from 'react-router-dom';
import Button from "../../Button/Button";

export default class ModalWaitingCheck extends React.Component {


	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<NavLink to='/app/tests' className='text-dark'>
				<Button type='button' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>ок</Button>
			</NavLink>
		</div>;
	};

	render() {
		return <div>
			<div className='modal-body'>
				<div className='mt-3'>
					Ожидайте проверки учителем
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

ModalWaitingCheck.propTypes = {
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

ModalWaitingCheck.defaultProps = {
	toggle: () => {},
	onClick: () => {}
};
