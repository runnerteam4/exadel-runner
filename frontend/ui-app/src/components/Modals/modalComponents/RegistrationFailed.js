import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';

export default class RegistrationFailed extends Component {
	renderFooter = () => {
		return (
			<div className='d-inline-flex justify-content-around'>
				<Button type='button' className='btn btn-dark' onClick={this.props.onClick}>Ок</Button>
			</div>
		);
	};

	render() {
		return (
			<div>
				<div className='modal-body'>
					<div className='mt-3'>
						<p>{this.props.text}</p>
					</div>
				</div>
				<div className='modal-footer'>
					{this.renderFooter()}
				</div>
			</div>
		);
	}
}

RegistrationFailed.propTypes = {
	onClick: PropTypes.func,
};

RegistrationFailed.defaultProps = {
	onClick: () => {},
};

