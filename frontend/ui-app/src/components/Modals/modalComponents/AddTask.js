import React, {Component} from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';
import FileUploader from "../../FileUploader/FileUploader";

export default class AddTask extends React.Component {
	constructor() {
		super();

		this.state = {
			name: '',
			condition: '',
			tags: '',
			selectedCategory: [],
			upFiles: [
				{
					input: '',
					output: '',
				},
			],
		};

		this.addFileUploader = this.addFileUploader.bind(this);
		this.handleInputFiles = this.handleInputFiles.bind(this);
		this.handleOutputFiles = this.handleOutputFiles.bind(this);
	}

	addFileUploader() {
		this.setState((prevState, props) => {
			let files = prevState.upFiles.slice();
			files.push({
				input: '',
				output: '',
			});
			return {
				upFiles: files,
			}
		});
	}

	handleInputFiles(items, index) {
		this.setState((prevState, props) => {
			let files = prevState.upFiles.slice();
			files[index].input = items.length === 0? '': items;
			return {
				upFiles: files,
			}
		});

	}

	handleOutputFiles(items, index) {
		this.setState((prevState, props) => {
			let files = prevState.upFiles.slice();
			files[index].output = items.length === 0? '': items;
			return {
				upFiles: files,
			}
		});
	}

	onChangeSelect = (e, name) => {
		this.setState(
			{[name]: e}
		);
	};

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};

	checkValidate = async () => {
		const form = document.getElementsByName('modalForm')[0];
		if (form.checkValidity()) {
			let body = {
				name: this.state.name,
				condition: this.state.condition,
				categoryId: this.state.selectedCategory.value
			};

			const files = [];

			await this.state.upFiles.forEach((file, index) => {
				const reader = new FileReader();
				reader.onload = function() {
					files.push({
						input: reader.result,
						answer: '',
					});
				};
				reader.readAsText(this.state.upFiles[index].input[0], 'utf-8');
			});

			await this.state.upFiles.forEach((file, index) => {
				const reader = new FileReader();
				reader.onload = function() {
					files[index].answer = reader.result;
				};
				reader.readAsText(this.state.upFiles[index].output[0], 'utf-8');
			});

			this.props.onClick(body, files);
		}
	};

	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='submit' className='btn btn-dark' onClick={this.checkValidate}>сохранить</Button>
		</div>;
	};

	changeCategoryField = (e) => {
		this.onChangeSelect(e, 'selectedCategory')
	};

	render() {
		const {selectedCategory, name, condition, tags} = this.state;
		const {category} = this.props;
		return <div>
			<div className='modal-body scrollingModal'>
				<Select
					className={'w-100'}
					placeholder={'Выберите раздел...'}
					value={selectedCategory}
					onChange={this.changeCategoryField}
					isMulti={false}
					options={category}
				/>
				<div className='mt-2'>
					<div className={'mt-1'}>
						<Input
							value={name}
							name="name"
							label="Название задачи:"
							onChange={this.onChange}
							placeholder="Введите название задачи"
							maxLength={30}
							required
						/>
					</div>
					<div className={'mt-1'}>
						<Input
							value={condition}
							name="condition"
							label="Условие:"
							placeholder="Введите условие"
							onChange={this.onChange}
							maxLength={1500}
							required
						/>
					</div>
					<div className={'mt-1'}>
						<Input
							value={tags}
							name="tags"
							label="Теги:"
							placeholder="Введите теги"
							onChange={this.onChange}
							maxLength={200}
							required
						/>
					</div>
				</div>
				<div className='container mt-2'>
					{
						this.state.upFiles.map((item, index) => {
							return (
								<div className="row" key={index}>
									<div className="col">
										<FileUploader
											fileTypes='.txt'
											upFiles={this.state.upFiles[index].input}
											handleFiles={this.handleInputFiles}
											displayButton={false}
											index={index}
										/>
									</div>
									<div className="col text-right">
										<FileUploader
											fileTypes='.txt'
											upFiles={this.state.upFiles[index].output}
											handleFiles={this.handleOutputFiles}
											displayButton={false}
											index={index}
										/>
									</div>
								</div>
							);
						})
					}
					<div className="row">
						<div className="col text-center">
							<Button
								className="btn btn-dark"
								type="button"
								onClick={this.addFileUploader}
							>
								Добавить еще
							</Button>
						</div>
					</div>
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

AddTask.propTypes = {
	category: PropTypes.array,
	toggle: PropTypes.func,
	onClick: PropTypes.func,
};

AddTask.defaultProps = {
	category: [
		{value: 'one', label: 'Раздел1'},
		{value: 'two', label: 'Раздел2'},
	],
	toggle: () => {
	},
	onClick: () => {
	},
};
