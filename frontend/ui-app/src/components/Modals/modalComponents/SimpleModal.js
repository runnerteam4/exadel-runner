import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';
import {bindActionCreators} from "redux";
import {getTask} from "../../../actions/TaskTeacher/action";
import {connect} from "react-redux";

class SimpleModal extends React.Component {
	onclick = () => {
		if (this.props.id) {
			this.props.onClick(this.props.id);
		}
		if (this.props.studentId) {
			this.props.onClick(this.props.studentId, this.props.groupId);
		}
		if(this.props.taskId){
			this.props.onClick(this.props.taskId);
		//	this.props.deleteTask(this.props.taskId);
		}
	};
	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='button' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='button' className='btn btn-dark' onClick={this.onclick}>подтвердить</Button>
		</div>;
	};

	render() {
		return <div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

SimpleModal.propTypes = {
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

SimpleModal.defaultProps = {
	toggle: () => {
	},
	onClick: () => {
	}
};

const mapStateToProps = ({}) => ({
});

const mapDispatchToProps = (dispatch) => ({
	deleteTask: bindActionCreators(getTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SimpleModal);

