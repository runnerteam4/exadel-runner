import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as ReactDOM from 'react-dom';


export default class Modal extends React.Component {
	noReload = (event) => {
		event.preventDefault();
	};

	renderModal = () => {
		return ((this.props.isActive) && (
				<form onSubmit={this.noReload} name="modalForm">
				<div className="modal-backdrop fade show">
				</div>
				<div className='modal fade show d-block modal-open'>
					<div className='modal-dialog'>
						<div className='modal-content'>
							<div className='modal-header'>
								<h5 className='modal-title'>{this.props.title}</h5>
								<button type='button' className='close' onClick={this.props.toggle}>
									<span aria-hidden='true'>&times;</span>
								</button>
							</div>
						{/*<div className="scrolling">*/}
							{this.props.renderBody()}
						{/*</div>*/}
						</div>
					</div>
				</div>
			</form>));
	};
	render() {
		return ReactDOM.createPortal(this.renderModal(), document.getElementById('modal-portal'));
	}
}

const renderBody = () => {
	return <form>
		<div className='form-group'>
		</div>
	</form>;
};


Modal.propTypes = {
	title: PropTypes.string,
	isActive: PropTypes.bool,
	toggle: PropTypes.func,
	renderFooter: PropTypes.func,
	renderBody: PropTypes.func
};

Modal.defaultProps = {
	title: '',
	isActive: false,
	toggle: () => {
	},
	renderBody: renderBody,
};
