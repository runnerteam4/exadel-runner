import PropTypes from "prop-types";
import React from "react";
import Button from "../../Button/Button";
import Input from "../../Input/Input";
import {connect} from "react-redux";


class ChangePassword extends React.Component {

	constructor() {
		super();
		this.state = {
			password: '',
			newPassword: '',
			repeatPassword: '',
			validField: true,
			rightPassword: true
		}
	}

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};


	sendPassword = async () => {
		this.setState({
			...this.state,
		});
		const res = await this.props.onClick(this.state.password, this.state.newPassword, this.props.userID);
		if (res.isValid) {
			this.props.toggle();
		}
		else {
			this.setState({rightPassword: false})
		}
	};


	onClick = (e) => {
		const form = document.getElementsByName('modalForm')[0];
		if (form.checkValidity()) {
			const {validField, newPassword, repeatPassword} = this.state;
			if (newPassword !== repeatPassword || newPassword === '') {
				this.setState({validField: !validField});
			}
			else {
				this.sendPassword();
			}
		}
	};
	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3'
					onClick={this.props.toggle}>отмена</Button>
			<Button type='submit'
					onClick={this.onClick}>сохранить</Button>
		</div>;
	};

	render() {
		return <div>
			<div className='modal-body'>
				<div>
					<Input
						value={this.state.password}
						name="password"
						className={this.state.rightPassword ? '' : ' alert-danger'}
						label="Введите текущий пароль:"
						type="password"
						onChange={this.onChange}
						maxLength={30}
						minLength={6}
						required
					/>
				</div>
				<div>
					<Input
						value={this.state.newPassword}
						name="newPassword"
						className={this.state.validField ? '' : ' alert-danger'}
						label="Введите новый пароль:"
						type="password"
						onChange={this.onChange}
						maxLength={30}
						minLength={6}
						required
					/>
				</div>
				<div>
					<Input
						value={this.state.repeatPassword}
						name="repeatPassword"
						className={this.state.validField ? '' : ' alert-danger'}
						label="Повторите пароль:"
						type="password"
						onChange={this.onChange}
						required
					/>
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

const mapStateToProps = ({login}) => ({
	userID: login.id,
});

ChangePassword.propTypes = {
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

ChangePassword.defaultProps = {
	toggle: () => {
	},
	onClick: () => {
	}
};

export default connect(mapStateToProps)(ChangePassword);
