import React, {Component} from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import Input from '../../Input/Input';
import Button from '../../Button/Button';

export default class AssignTest extends React.Component {
	constructor() {
		super();
		this.state = {
			name: '',
			selectedCategory: [],
			tags: '',
			selectedType: [],
			time: '',
			countQuestion: '',
			deadline: ''
		}
	}

	onChangeSelect = (e, name) => {
		this.setState(
			{[name]: e}
		);
	};

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};

	checkValidate = () => {
		const form = document.getElementsByName('modalForm')[0];
		let changeFormatTime = Number(this.state.time) * 60;
		let formTags = this.state.tags.split(' ');
		if (form.checkValidity() && ((this.state.countQuestion % 4) === 0)) {
			let body = {};
			if (this.props.role === 'student') {
				body = {
					studentId: this.props.id,
					time: changeFormatTime,
					deadline: `${this.state.deadline}`,
					name: this.state.name,
					topicId: this.state.selectedCategory.value,
					tags: formTags,
					questionAmount: Number(this.state.countQuestion),
					training: Number(this.state.selectedType.value)
				}
			} else {
				body = {
					groupId: this.props.id,
					time: changeFormatTime,
					deadline: `${this.state.deadline}`,
					name: this.state.name,
					topicId: this.state.selectedCategory.value,
					tags: formTags,
					questionAmount: Number(this.state.countQuestion),
					training: Number(this.state.selectedType.value)
				}
			}
			this.props.onClick(body, this.props.role);
		}
	};

	renderFooter = () => {
		return <div className='d-inline-flex justify-content-around'>
			<Button type='submit' className='btn btn-outline-dark mr-3' onClick={this.props.toggle}>отмена</Button>
			<Button type='submit' className='btn btn-dark' onClick={this.checkValidate}>сохранить</Button>
		</div>;
	};

	changeCategoryField = (e) => {
		this.onChangeSelect(e, 'selectedCategory');
	};
	changeTypeField = (e) => {
		this.onChangeSelect(e, 'selectedType')
	};

	render() {
		const {
			selectedCategory, selectedType,
			deadline, tags, countQuestion, time
		} = this.state;
		const {type, category} = this.props;
		return <div>
			<div className='modal-body scrollingModal'>
				<Select
					className={'w-100'}
					placeholder={'Выберите раздел...'}
					value={selectedCategory}
					onChange={this.changeCategoryField}
					isMulti={false}
					options={category}
				/>
				<div className={'mt-2'}>
					<Select
						className={'w-100'}
						placeholder={'Выберите тип теста...'}
						value={selectedType}
						onChange={this.changeTypeField}
						isMulti={false}
						options={type}
					/>
				</div>
				<div className='mt-2'>
					<Input
						value={this.state.name}
						name="name"
						label="Название теста:"
						placeholder="Java 2 курс экзамен"
						onChange={this.onChange}
						maxLength="200"
						required
					/>
				</div>
				<div className='mt-2'>
					<Input
						value={tags}
						name="tags"
						label="Теги:"
						placeholder="Введите теги"
						onChange={this.onChange}
						maxLength="200"
						required
					/>
				</div>
				<div>
					<div className={'mt-1'}>
						<Input
							value={time}
							name="time"
							label="Время выполнения:"
							placeholder="Введите время выполнения"
							onChange={this.onChange}
							maxLength="200"
							required
						/>
					</div>
					<div className={'d-flex justify-content-between mt-1'}>
						<div>
							<Input
								type="number"
								value={countQuestion}
								name="countQuestion"
								label="Количество вопросов:"
								onChange={this.onChange}
								required
								min={1}
								max={50}
							/>
						</div>
						<div>
							<Input
								type="text"
								value={deadline}
								name="deadline"
								label="Дедлайн:"
								onChange={this.onChange}
								placeholder="чч.мм.гггг"
								pattern="(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[012])\.(19|20)\d\d"
								title="Введите дедлайн в формате чч.мм.гггг"
								required
							/>
						</div>
					</div>
				</div>
			</div>
			<div className='modal-footer'>
				{this.renderFooter()}
			</div>
		</div>;
	}
}

AssignTest.propTypes = {
	category: PropTypes.array,
	type: PropTypes.array,
	toggle: PropTypes.func,
	onClick: PropTypes.func
};

AssignTest.defaultProps = {
	category: [
		{value: 'one', label: 'Раздел1'},
		{value: 'two', label: 'Раздел2'},
	],
	type: [
		{value: '0', label: 'Контрольный'},
		{value: '1', label: 'Тренировочный'},
	],
	toggle: () => {
	},
	onClick: () => {
	}
};
