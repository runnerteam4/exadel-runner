import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';

const Checkbox = (props) => {
    const {className, name, value, data, ...remain} = props;
    return (
        <div className="input-group mb-3">
            <div className="input-group-prepend">
                <div className="input-group-text">
                    <input
                        {...remain}
                        className={classNames(className)}
                        name={name}
                        value={value}
                    />
                </div>
            </div>
            <label className="form-control">{data}</label>
        </div>
    );
};

Checkbox.propTypes = {
    name: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    type: PropTypes.string,
    onChange: PropTypes.func,
};

Checkbox.defaultProps = {
    name: '',
    value: '',
    type: 'checkbox',
};

export default Checkbox;