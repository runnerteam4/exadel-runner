import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';


class Countdown extends Component {
	constructor(props) {
		super(props);

		this.state = {
			time: { },
			seconds: this.props.seconds,
		};

		this.timer = null;
		this.countdown = this.countdown.bind(this);
		this.secondsConversion = this.secondsConversion.bind(this);
	}

	componentDidMount() {
		const remainingTime = this.secondsConversion(this.state.seconds);
		this.setState({
			time: remainingTime,
		});
		this.timer = setInterval(this.countdown, 1000);
	}

	componentWillUnmount() {
		clearInterval(this.timer);
	}

	countdown() {
		const seconds = this.state.seconds - 1;
		const remainingTime = this.secondsConversion(seconds);

		this.setState({
			time: remainingTime,
			seconds: seconds,
		});

		if (seconds === 0) {
			clearInterval(this.timer);
			this.props.timeOver();
		}
	}

	secondsConversion(s) {
		const hours = Math.floor(s / (60 * 60));

		const divisorForMinutes = s % (60 * 60);
		const minutes = Math.floor(divisorForMinutes / 60);

		const divisorForSeconds = divisorForMinutes % 60;
		const seconds = Math.ceil(divisorForSeconds);

		return {
			hours,
			minutes,
			seconds,
		}
	}

	render() {
		return (
			<div className={classNames('col', this.props.className)}>
				<h4>
					Осталось {this.state.time.hours} : { this.state.time.minutes } : { this.state.time.seconds }
				</h4>
			</div>
		);
	}
}

Countdown.propTypes = {
	seconds: PropTypes.number.isRequired,
};

export default Countdown;
