import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './css/index.css';

const Footer = () => {
	return (
		<footer className={'footer page-footer font-small'}>
			<div className={'footer-copyright text-center py-3 text-dark'}>
				© 2018 Exadel practice
			</div>
		</footer>
	);
};

export default Footer;
