import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';

const TextArea = ({className, name, value, maxLength, rows, placeholder, ...remain}) => {
    return (
        <textarea
            {...remain}
            className={classNames('form-control', className)}
            name={name}
            value={value}
            maxLength={maxLength}
            rows={rows}
            placeholder={placeholder}
        />
    );
};

TextArea.propTypes = {
    className: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    maxLength: PropTypes.number,
    rows: PropTypes.number,
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
};

TextArea.defaultProps = {
    className: '',
    name: '',
    value: '',
    maxLength: 120,
    rows: 5,
    placeholder: '',
};

export default TextArea;