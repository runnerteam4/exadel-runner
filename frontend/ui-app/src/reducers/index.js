import { combineReducers } from 'redux';
import testList from './TestList/index';
import materials from './Materials/MaterialsReducer';
import GroupReducer from './Groups/index';
import ModalReducer from './Modals/ModalReducer';
import testPassedStudent from './TestPassedStudent/testList';
import testAnswers from './TestAnswers/index';
import testsTeacher from './TestsTeacher/testList';
import testCheck from './TestCheck/testCheck';
import taskList from './TaskList/taskList';
import testView from './TestView/reducer';
import profile from './Profile/profile';
import login from './LogIn/login';
import Students from './Students/Students';
import registration from './Registration/reducer';
import allUsers from './Users/Users';
import teacherRequest from './Users/TeacherRequest';
import taskTeacher from './TaskTeacher/taskTeacher';
import startpage from './StartPage/startpage';
import taskView from './TaskView/reducer';


export default combineReducers({
	testList,
	testView,
	materials,
	GroupReducer,
	ModalReducer,
	testsTeacher,
	testCheck,
	testPassedStudent,
	testAnswers,
	taskList,
	profile,
	Students,
	registration,
	allUsers,
	teacherRequest,
	login,
	taskTeacher,
	startpage,
	taskView,
});
