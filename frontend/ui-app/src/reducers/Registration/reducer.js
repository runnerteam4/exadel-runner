const initialState = {
	isFetching: false,
	error: null,
	response: null,
};

export default function(state = initialState, action) {
	switch (action.type) {
		case 'REGISTRATION/SEND_USER':
			return { ...state, isFetching: true };
		case 'REGISTRATION/SEND_USER_SUCCESS':
			return { ...state, isFetching: false, response: action.payload };
		case 'REGISTRATION/SEND_USER_FAILED':
			return { ...state, isFetching: false, error: action.payload };
		case 'REGISTRATION/UPDATE':
			return initialState;
		default:
			return state;
	}
}
