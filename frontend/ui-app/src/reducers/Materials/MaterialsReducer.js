const initialState = {
	category: [],
	data: [],
	currentPage: 1,
	currentData: [],
	chunkSize: 5,
	totalPageNumber: 3
};

export default function materials(state = initialState, action) {
	const {chunkSize, currentData, currentPage, totalPageNumber, data} = action;
	switch (action.type) {
		case 'MATERIALS/CHANGE_CHUNK':
			return {
				...state,
				currentPage: 1,
				chunkSize,
				currentData,
				totalPageNumber
			};
		case 'MATERIALS/GET_DATA':
			return {
				...state,
				data,
				currentPage: 1,
				currentData,
				totalPageNumber
			};
		case 'MATERIALS/ON_NEXT':
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				currentData,
				totalPageNumber
			};
		case 'MATERIALS/ON_PREVIOUS':
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				currentData,
				totalPageNumber
			};
		case 'MATERIALS/GET_CATEGORIES':
			return {
				...state, category: action.category
			};
		case 'MATERIALS/ADD_MATERIAL':
			return {
				...state,
				currentPage: 1,
				currentData,
				totalPageNumber
			};
		case 'MATERIALS/ADD_CATEGORY':
			return {
				...state,
				category: action.category,

			};
		default:
			return state;
	}
}