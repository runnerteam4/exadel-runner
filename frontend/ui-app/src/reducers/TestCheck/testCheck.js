export const TEST_CHECK = 'TEST_CHECK_';

const initialState = {
	answers: [],
	comment: '',
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
	case `${TEST_CHECK}GET_ANSWERS`:
		return {
			...state,
			answers: action.answers
		};
	case `${TEST_CHECK}APPROVE_CHECK`:
		return {
			...state
		};
	case `${TEST_CHECK}CHANGE_INPUT_VALUE`:
		return {
			...state,
			answers: state.answers.map((currentValue, index) => {
				return index === action.id
					? {
						...currentValue,
						mark: action.mark,
					}
					: currentValue;
			})
		};
	case `${TEST_CHECK}CHANGE_COMMENT_VALUE`:
		return {
			...state,
			comment: action.comment,
		};
	default:
		return state;
	}
}
