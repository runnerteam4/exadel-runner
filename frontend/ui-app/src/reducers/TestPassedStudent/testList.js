export const TEST_PASSED_STUDENT = 'TEST_PASSED_STUDENT_';

const initialState = {
	currentData: []
};

export default function reducer(state = initialState, action) {
	const {currentData} = action;
	switch (action.type) {
	case `${TEST_PASSED_STUDENT}GET_TEST_LIST`:
		return {
			currentData
		};
	default:
		return state;
	}
}
