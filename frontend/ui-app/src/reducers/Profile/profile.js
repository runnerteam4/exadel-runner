const initialState = {
	isFetching: true,
	error: null,
	data: null,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case 'PROFILE/GET_USER':
			return { ...state, isFetching: true };
		case 'PROFILE/GET_USER_SUCCESS':
			return { ...state, isFetching: false, data: action.payload };
		case 'PROFILE/GET_USER_FAILED':
			return { ...state, isFetching: false, error: action.payload };
		default:
			return state;
	}
}
