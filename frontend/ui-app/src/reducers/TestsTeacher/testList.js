export const TEST_TEACHER = 'TEST_TEACHER_';

const initialState = {
	tests: []
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
	case `${TEST_TEACHER}GET_TESTS`:
		return {
			tests: action.tests
		};
	default:
		return state;
	}
}
