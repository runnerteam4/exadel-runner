
const initialState = {
	data: [],
	isFetching: false
};

export default function groups(state = initialState, action) {
	switch (action.type) {
		case 'GROUPS/GETDATA':
			return {
				...state,
				data: action.data
			};
		case 'GROUPS/CHANGE_GROUP_NAME':
			return {
				...state,
				data: action.data};
		case 'GROUPS/DELETE':
			return {
				...state,
				data: action.data};
		case 'GROUPS/ADD':
			return {
				isFetching: action.isFetching,
				data: action.data};
		case'GROUPS/UPDATE':
			return {...state,
				isFetching: action.isFetching};
		default:
			return state;
	}
}