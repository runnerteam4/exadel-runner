const currentGroup = [
	{name: 'студент 1', id: '1'},
	{name: 'студент 2', id: '2'},
	{name: 'студент 3', id: '3'},
	{name: 'студент 4', id: '4'},
	{name: 'студент 5', id: '5'},
	{name: 'студент 6', id: '6'}
];

const initialState = {
	data: currentGroup,
	name: ''
};

export default function group(state = initialState, action) {
	switch (action.type) {
		case 'GROUP/GET_DATA':
			return {data: action.data, name: action.name};
		case 'GROUP/EXCLUDE_STUDENT':
			return {data: action.data, name: action.name};
		default:
			return state;
	}
}