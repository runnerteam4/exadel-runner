import { combineReducers } from 'redux';
import group from './group';
import groups from './groups';

export default combineReducers({
	groups,
	group,
});