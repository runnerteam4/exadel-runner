const initialState = {
	id: '',
	firstName: '',
	lastName: '',
	role: '',
	isConfirm: false,
};

export default function reducer(state = initialState, action) {
	const {id, firstName, lastName, role, isConfirm} = action;
	switch (action.type) {
	case 'LOGIN':
		return state;
	case 'GET_USER_DATA':
		return {
			id,
			firstName,
			lastName,
			role,
			isConfirm,
		};
	default:
		return state;
	}
}
