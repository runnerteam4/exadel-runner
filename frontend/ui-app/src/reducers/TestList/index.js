import { combineReducers } from 'redux';
import pagination from './pagination';
import categories from './categories';

export const TEST_LIST = 'TEST_LIST_';

export default combineReducers({
	pagination,
	categories,
});
