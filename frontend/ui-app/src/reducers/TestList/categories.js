import {TEST_LIST} from './index';

const initialState = {
	categories: []
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
	case `${TEST_LIST}GET_CATEGORIES`:
		return {
			categories: action.payload
		};
	default:
		return state;
	}
}
