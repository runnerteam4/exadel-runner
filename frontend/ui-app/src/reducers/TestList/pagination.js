import {TEST_LIST} from './index';

const initialState = {
	totalPageNumber: 0,
	currentPage: 1,
	currentData: [],
	chunkSize: 5,
	isPending: false,
	status: '',
	error: '',
};

export default function reducer(state = initialState, action) {
	const {chunkSize, currentData, isPending, status, error, currentPage, totalPageNumber} = action;
	switch (action.type) {
	case `${TEST_LIST}CHANGE_CHUNK`:
		return {
			...state,
			currentPage: 1,
			chunkSize,
			currentData,
			isPending,
			status,
			error,
		};
	case `${TEST_LIST}ON_NEXT`:
		return {
			...state,
			currentPage: currentPage ? currentPage : state.currentPage,
			currentData,
			isPending,
			status,
			error,
		};
	case `${TEST_LIST}ON_PREVIOUS`:
		return {
			...state,
			currentPage: currentPage ? currentPage : state.currentPage,
			currentData,
			isPending,
			status,
			error,
		};
	case `${TEST_LIST}GET_TEST_LIST`:
		return {
			...state,
			currentPage: 1,
			currentData,
			totalPageNumber,
			isPending,
			status,
			error,
		};
	default:
		return state;
	}
}
