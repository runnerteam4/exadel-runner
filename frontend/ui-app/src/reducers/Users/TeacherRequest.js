const initialState = {
	totalPageNumber: 0,
	currentPage: 1,
	currentData: [],
	chunkSize: 5
};

export default function teacherRequest(state = initialState, action) {
	const {chunkSize, currentData, currentPage, totalPageNumber} = action;
	switch (action.type) {

		case 'TEACHERS_REQUEST/GET_DATA':
			return {
				...state,
				currentPage: 1,
				currentData,
				totalPageNumber
			};
		case 'TEACHERS_REQUEST/REJECT_REQUEST':
			return {
				...state,
				currentData: action.data
			};
		case 'TEACHERS_REQUEST/APPROVE_REQUEST':
			return {
				...state,
				currentData: action.data
			};
		default:
			return state;
	}
}
