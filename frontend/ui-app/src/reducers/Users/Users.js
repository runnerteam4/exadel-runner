const initialState = {
	totalPageNumber: 0,
	currentPage: 1,
	currentData: [],
	chunkSize: 5
};

export default function allUsers(state = initialState, action) {
	const {chunkSize, currentData, currentPage, totalPageNumber} = action;
	switch (action.type) {
		case 'USERS/CHANGE_CHUNK':
			return {
				...state,
				currentPage: 1,
				chunkSize,
				currentData,
				totalPageNumber
			};
		case 'USERS/ON_NEXT':
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				currentData,
				totalPageNumber
			};
		case 'USERS/ON_PREVIOUS':
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				currentData,
				totalPageNumber
			};
		case 'USERS/GET_DATA':
			return {
				...state,
				currentPage: 1,
				currentData,
				totalPageNumber
			};
		case 'USERS/REJECT_REQUEST':
			return {
				...state,
				currentData: action.data
			};
		case 'USERS/APPROVE_REQUEST':
			return {
				...state,
				currentData: action.data
			};
		default:
			return state;
	}
}
