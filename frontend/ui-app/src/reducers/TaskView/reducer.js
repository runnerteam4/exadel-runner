const initialState = {
	data: {},
	isFetching: true,
	error: null,
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case 'TASK/GET_TASK_SUCCESS':
			return { ...state, isFetching: false, data: action.payload };
		case 'TASK/GET_TASK_FAILED':
			return { ...state, isFetching: false, error: action.payload };
		default:
			return state;
	}
}