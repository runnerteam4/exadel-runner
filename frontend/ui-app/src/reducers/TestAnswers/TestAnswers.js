import {TEST_ANSWERS} from './index';

const initialState = {
	currentData: []
};

export default function reducer(state = initialState, action) {
	const {currentData} = action;
	switch (action.type) {
	case `${TEST_ANSWERS}GET_TEST_ANSWERS`:
		return {
			currentData
		};
	default:
		return state;
	}
}
