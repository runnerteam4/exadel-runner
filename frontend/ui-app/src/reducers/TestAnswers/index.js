import { combineReducers } from 'redux';
import testAnswers from './TestAnswers';

export const TEST_ANSWERS = 'TEST_ANSWERS_';

export default combineReducers({
	testAnswers,
});
