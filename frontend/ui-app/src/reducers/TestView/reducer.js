if (!localStorage.getItem('currentPage')) {
	localStorage.setItem('currentPage', JSON.stringify(1));
}
if (!localStorage.getItem('answers')) {
	localStorage.setItem('answers', JSON.stringify([]));
}

const initialState = {
	data: {},
	currentPage: JSON.parse(localStorage.getItem('currentPage')),
	answers: JSON.parse(localStorage.getItem('answers')),
	seconds: 0,
	isFetching: true,
	error: null,
};

const makeAnswer = (currentPage, answer, data, questionId) => {
	data[currentPage] =  { answer, questionId };
	return data;
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case 'TEST/ON_NEXT':
			return { ...state, currentPage: state.currentPage + 1 };
		case 'TEST/ON_PREVIOUS':
			return { ...state, currentPage: state.currentPage - 1 };
		case 'TEST/CHANGE_ANSWER':
			return { ...state, answers: makeAnswer(action.payload.page - 1, action.payload.answer, state.answers, action.payload.questionId) };
		case 'TEST/GET_TEST_SUCCESS':
			return { ...state, isFetching: false, data: action.payload };
		case 'TEST/GET_TEST_FAILED':
			return { ...state, isFetching: false, error: action.payload };
		case 'TEST/UPDATE_TIME':
			return { ...state, seconds: action.payload };
		case 'TEST/UPDATE':
			return { ...initialState, currentPage: 1, answers: [] };
		default:
			return state;
	}
}
