const initialState = {
	totalPageNumber: 0,
	currentPage: 1,
	currentData: [],
	chunkSize: 5
};

export default function newGroup(state = initialState, action) {
	const {chunkSize, currentData, currentPage, totalPageNumber} = action;
	switch (action.type) {
		case 'STUDENTS/CHANGE_CHUNK':
			return {
				...state,
				currentPage: 1,
				chunkSize,
				currentData,
				totalPageNumber
			};
		case 'STUDENTS/ON_NEXT':
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				currentData,
				totalPageNumber
			};
		case 'STUDENTS/ON_PREVIOUS':
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				currentData,
				totalPageNumber
			};
		case 'STUDENTS/GET_DATA':
			return {
				...state,
				currentPage: 1,
				currentData,
				totalPageNumber
			};
		default:
			return state;
	}
}
