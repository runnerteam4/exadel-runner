export const TASK_LIST = 'TASK_LIST_';

const initialState = {
	tasks: [],
	currentData: [],
	categories: [],
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case `${TASK_LIST}GET_CATEGORIES`:
			return {
				...state,
				categories: action.categories,
			};
		case `${TASK_LIST}GET_TASKS`:
			return {
				...state,
				tasks: action.tasks,
				currentData: action.currentData
			};
		case `${TASK_LIST}GET_TASKS_FILTER`:
			return {
				...state,
				tasks: action.tasks,
			};
		default:
			return state;
	}
}
