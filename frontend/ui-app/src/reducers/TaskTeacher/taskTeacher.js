export const TASK_TEACHER = 'TASK_TEACHER_';

const initialState = {
	groups: [],
	tasks: [],
	allTasks: [],
	task: {},
	totalPageNumber: 0,
	currentPage: 1,
	chunkSize: 5,
	isFetching: true
};

export default function reducer(state = initialState, action) {
	const {chunkSize, allTasks, currentPage, totalPageNumber, task} = action;
	switch (action.type) {
		case `${TASK_TEACHER}GET_CATEGORIES`:
			return {
				...state,
				groups: action.categories
			};
		case `${TASK_TEACHER}GET_TASKS`:
			return {
				...state,
				tasks: action.tasks,
			};
		case `${TASK_TEACHER}GET_ALL_TASKS`:
			return {
				...state,
				currentPage: 1,
				totalPageNumber,
				allTasks: action.allTasks
			};
		case `${TASK_TEACHER}CHANGE_CHUNK`:
			return {
				...state,
				currentPage: 1,
				chunkSize,
				allTasks,
				totalPageNumber
			};
		case `${TASK_TEACHER}ON_NEXT`:
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				allTasks,
				totalPageNumber
			};
		case `${TASK_TEACHER}GET_TASK`:
			return {
				...state,
				task: task ? task : state.task,
				isFetching: action.isFetching
			};
		case `${TASK_TEACHER}ON_PREVIOUS`:
			return {
				...state,
				currentPage: currentPage ? currentPage : state.currentPage,
				allTasks,
				totalPageNumber
			};
		default:
			return state;
	}
}
