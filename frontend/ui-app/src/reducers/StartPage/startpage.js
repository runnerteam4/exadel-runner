const initialState = {
	data: {},
};

export default function startPage(state = initialState, action) {
	switch (action.type) {
		case 'START_PAGE/GET_DATA':
			return {
				data: action.data,
			};
		default:
			return state;
	}
}