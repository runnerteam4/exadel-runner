import {request} from "../fetchWrapper";

const makeCurrentData = (currentPage, chunkSize, data) => {
	const indexOfLastTodo = currentPage * chunkSize;
	const indexOfFirstTodo = indexOfLastTodo - chunkSize;
	return data.slice(indexOfFirstTodo, indexOfLastTodo);
};

const sendRequest = (type, filter, chunkSize) => {
	return async (dispatch, getState) => {
		dispatch({
			type: type,
		});
		const state = getState();
		let pageNumber = state.materials.currentPage;
		let currentData = [];
		let changeChunk = chunkSize || state.materials.chunkSize;
		let countOfPages;
		let data = [];
		try {
			const res = await request('/api/wiki', {method: 'GET'});
			data = await res.response.json();
			if (filter) {
				currentData = data.filter((item) => {
					return (item.name.indexOf(filter) >= 0);
				});
				countOfPages = currentData.length;
				if (type === 'MATERIALS/GET_DATA') {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === 'MATERIALS/CHANGE_CHUNK') {
					pageNumber = 1;
					changeChunk = chunkSize;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === 'MATERIALS/ON_NEXT') {
					pageNumber = pageNumber + 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
				else if (type === 'MATERIALS/ON_PREVIOUS') {
					pageNumber = pageNumber - 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
			} else {
				countOfPages = data.length;
				if (type === 'MATERIALS/GET_DATA') {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, data);
				}
				if (type === 'MATERIALS/CHANGE_CHUNK') {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, data);
				}
				if (type === 'MATERIALS/ON_NEXT') {
					pageNumber = pageNumber + 1;
					currentData = makeCurrentData(pageNumber, changeChunk, data);
				}
				else if (type === 'MATERIALS/ON_PREVIOUS') {
					pageNumber = pageNumber - 1;
					currentData = makeCurrentData(pageNumber, changeChunk, data);
				}
			}
		}
		catch
			(err) {
			currentData = [];
		}
		dispatch({
			type: type,
			data: data,
			chunkSize: changeChunk,
			currentData: currentData,
			totalPageNumber: countOfPages, // с сервера
			currentPage: pageNumber,
		});
	};
};


const MaterialsFilter = (type, filterType, filterValue, chunkSize) => {
	return async (dispatch, getState) => {
		const state = getState();
		let data = state.materials.data;
		let pageNumber = state.materials.currentPage;
		let changeChunk = chunkSize || state.materials.chunkSize;
		let countOfPages;
		let currentData = [];
		try {
			if (filterType === 'filter') {
				currentData = data.filter((item) => {
					return (item.name.indexOf(filterValue) >= 0);
				});
			}
			if (filterType === 'select') {
				currentData = data.filter((item) => {
					let check = false;
					filterValue.forEach((filterItem) => {
							 if(filterItem === item.categoryId){
							 	check = true;
							 }
						}
					);
					return check;
				});
			}
			countOfPages = currentData.length;
			if (type === 'MATERIALS/GET_DATA') {
				pageNumber = 1;
				currentData = makeCurrentData(1, changeChunk, currentData);
			}
		}
		catch
			(err) {
			currentData = [];
		}
		dispatch({
			type: type,
			chunkSize: changeChunk,
			currentData: currentData,
			data: data,
			totalPageNumber: countOfPages, // с сервера
			currentPage: pageNumber,
		});
	};
};



export const getCategories = () => {
	return async (dispatch, getState) => {
		const formData = (item) => {
			const {id, name} = item;
			return {value: id, label: name};
		};
		const state = getState();
		let categories = state.materials.category;
		let error = '';
		try {
			const res = await request('/api/wiki/categories', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);
		}

		catch (err) {
			error = err;
			categories = [{}];
		}

		dispatch({
			type: 'MATERIALS/GET_CATEGORIES',
			category: categories
		});
		return error;
	};
};


export const addCategoryModal = () => {
	return async (dispatch, getState) => {
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'ADD_CATEGORY',
			modalProps: {}
		});
	};
};


export const addMaterialModal = () => {
	const formData = (item, index) => {
		const {id, createDate, name} = item;
		return {value: id, label: name};
	};
	return async (dispatch, getState) => {
		let error = '';
		const state = getState();
		let categories = [];
		try {
			const res = await request('/api/wiki/categories', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);

		}
		catch (err) {
			error = err;
			categories = [];
		}
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'ADD_MATERIAL',
			modalProps: {
				category: categories
			}
		});
		return error;
	};
};


export const addMaterial = (link, author_id, name, category) => {
	return async (dispatch, getState) => {
		let totalPageNumber;
		let data = {
			link: link,
			authorId: author_id,
			name: name,
			categoryId: category
		};
		const state = getState();
		data = JSON.stringify(data);
		let error = '';
		try {
			let res = await request('/api/wiki/material/new', {
				method: 'POST', body: data,
				headers: {'Content-type': 'application/json'}
			});
			res = await res.response.json();
		}
		catch (err) {
			error = err;
		}
		dispatch({
			type: 'MATERIALS/ADD_MATERIAL',
		});
		dispatch({
			type: 'HIDE_MODAL'
		});
		return error;
	};
};

export const addCategory = (category) => {
	return async (dispatch, getState) => {
		const state = getState();
		let currentData = {"name": category};
		currentData = JSON.stringify(currentData);
		let categories = state.materials.category;
		let error = '';
		try {
			const res = await request('/api/wiki/category/new', {
				method: 'POST', body: currentData,
				headers: {'Content-type': 'application/json', 'Accept': 'application/json',}
			});

		}
		catch (err) {
			error = err;
			categories = [];
		}
		dispatch({
			type: 'MATERIALS/ADD_CATEGORY',
			category: categories
		});
		dispatch({
			type: 'HIDE_MODAL'
		});
		return error;
	};
};

export const getMaterials = (filter) => {
	return sendRequest('MATERIALS/GET_DATA', filter);
};

export const onChangeChunk = (filter, chunkSize) => {
	return sendRequest('MATERIALS/CHANGE_CHUNK', filter, chunkSize);
};

export const onNext = (filter) => {
	return sendRequest('MATERIALS/ON_NEXT', filter);
};

export const onPrevious = (filter) => {
	return sendRequest('MATERIALS/ON_PREVIOUS', filter);
};


export const getMaterialsFilter = (filterType,filterValue) => {
	return MaterialsFilter('MATERIALS/GET_DATA',filterType, filterValue);
};
