import {request} from "../fetchWrapper";

const makeCurrentData = (currentPage, chunkSize, data) => {
	const indexOfLastTodo = currentPage * chunkSize;
	const indexOfFirstTodo = indexOfLastTodo - chunkSize;
	return data.slice(indexOfFirstTodo, indexOfLastTodo);
};

const sendRequest = (type, filter, chunkSize) => {
	return async (dispatch, getState) => {
		dispatch({
			type: type,
		});
		const state = getState();
		let pageNumber = state.Students.currentPage;
		let currentData = [];
		let changeChunk = chunkSize || state.Students.chunkSize;
		let countOfPages;
		let data = [];
		try {
			const res = await request('/api/users/all', {method: 'GET'});
			data = await res.response.json();
			currentData = data.filter((item) => {
				return (item.role.indexOf('ROLE_STUDENT') >= 0);
			});
			if (filter) {
				currentData = currentData.filter((item) => {
					return ((item.firstName.indexOf(filter) >= 0) || (item.lastName.indexOf(filter) >= 0));
				});
				countOfPages = currentData.length;
				if (type === 'STUDENTS/GET_DATA') {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === 'STUDENTS/CHANGE_CHUNK') {
					pageNumber = 1;
					changeChunk = chunkSize;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === 'STUDENTS/ON_NEXT') {
					pageNumber = pageNumber + 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
				else if (type === 'STUDENTS/ON_PREVIOUS') {
					pageNumber = pageNumber - 1;
					currentData = makeCurrentData(pageNumber,changeChunk, currentData);
				}
			} else {
				countOfPages = currentData.length;
				if(type === 'STUDENTS/GET_DATA'){
					pageNumber = 1;
					currentData = makeCurrentData(1,changeChunk,currentData);
				}
				if (type === 'STUDENTS/CHANGE_CHUNK') {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === 'STUDENTS/ON_NEXT') {
					pageNumber = pageNumber + 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
				else if (type === 'STUDENTS/ON_PREVIOUS') {
					pageNumber = pageNumber - 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
			}
		}
		catch
			(err) {
			currentData = [];
		}
		dispatch({
			type: type,
			chunkSize: changeChunk,
			currentData: currentData,
			totalPageNumber: countOfPages, // с сервера
			currentPage: pageNumber,
		});
	};
};

export const getData = (filter) => {
	return sendRequest('STUDENTS/GET_DATA', filter);
};

export const onChangeChunk = (filter, chunkSize) => {
	return sendRequest('STUDENTS/CHANGE_CHUNK', filter, chunkSize);
};

export const onNext = (filter) => {
	return sendRequest('STUDENTS/ON_NEXT', filter);
};

export const onPrevious = (filter) => {
	return sendRequest('STUDENTS/ON_PREVIOUS', filter);
};


