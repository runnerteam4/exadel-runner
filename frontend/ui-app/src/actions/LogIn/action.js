import {request} from '../fetchWrapper';

export const LogIn = (username, password) => {
	return async (dispatch) => {
		const data = new FormData();
		data.append('username', username);
		data.append('password', password);

		const answer = await request('login', {method: 'POST', body: data});

		dispatch({
			type: 'LOGIN',
		});

		return {
			isLogin: answer.response.status === 200,
			error: answer.error
		};
	};
};

export const getUserData = () => {
	return async (dispatch) => {
		const answer = await request('api/users/current', {method: 'GET'});
		const data = await answer.response.json();
		const {id, firstName, lastName, role, status} = data;
		dispatch({
			type: 'GET_USER_DATA',
			id,
			firstName,
			lastName,
			role,
			isConfirm: status
		});
		return data;
	};
};
