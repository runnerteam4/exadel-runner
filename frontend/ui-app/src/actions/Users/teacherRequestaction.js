import {dataRejectTeacterRequest} from '../../mocks/AllUsers/data';
import {request} from "../fetchWrapper";


const sendRequest = (type, filter) => {
	return async (dispatch, getState) => {
		dispatch({
			type: type,
		});
		const state = getState();
		let currentData = [];
		let data = [];
		try {
			const res = await request('/api/users/request', {method: 'GET'});
			currentData = await res.response.json();
			data = currentData;
			if(filter){
				data = currentData.filter((item) => {
					return ((item.firstName.indexOf(filter) >= 0)||(item.lastName.indexOf(filter)>=0));
				});
			}
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: type,
			currentData: data,
		});
	};
};

export const rejectRequest = (user_id) => {

	return async (dispatch, getState) => {
		dispatch({
			type: 'TEACHERS_REQUEST/REJECT_REQUEST'
		});

		let data = [];
		let body = JSON.stringify({status: false});
		try {
			const res = await request(`api/users/teacher/enable/${user_id}`,
				{
					method: 'PUT',
					body:body,
					headers: {'Content-type': 'application/json'}
				});
			data = await res.response.json();
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'TEACHERS_REQUEST/REJECT_REQUEST',
			data: data
		});
	};

};


export const approveRequest = (user_id) => {

	return async (dispatch, getState) => {
		dispatch({
			type: 'TEACHERS_REQUEST/APPROVE_REQUEST'
		});

		let data = [];
		let body = JSON.stringify({status: true});
		try {
			const res = await request(`api/users/teacher/enable/${user_id}`,
				{
					method: 'PUT',
					body:body,
					headers: {'Content-type': 'application/json'}
				});
			data = await res.response.json();
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'TEACHERS_REQUEST/APPROVE_REQUEST',
			data: data
		});
	};

};

export const getData = (filter) => {
	return sendRequest('TEACHERS_REQUEST/GET_DATA', filter);
};


