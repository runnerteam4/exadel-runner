import {users as mockUsers, dataRejectRequest, dataApproveRequest} from '../../mocks/AllUsers/data';
import teachersRequest from '../../mocks/AllUsers/data';
import {request} from "../fetchWrapper";


const makeCurrentData = (currentPage, chunkSize, data) => {
	const indexOfLastTodo = currentPage * chunkSize;
	const indexOfFirstTodo = indexOfLastTodo - chunkSize;
	return data.slice(indexOfFirstTodo, indexOfLastTodo);
};


const sendRequest = (type, filter, chunkSize) => {
	return async (dispatch, getState) => {
		dispatch({
			type: type,
		});
		const state = getState();
		let pageNumber = state.allUsers.currentPage;
		let currentData = [];
		let data = [];
		let changeChunk;
		let countOfPages;
		try {
			const res = await request('/api/users/all', {method: 'GET'});
			data = await res.response.json();
			if (filter) {
				if(filter === 'студент'){
					filter = 'ROLE_STUDENT';
				}
				if(filter === 'преподаватель'){
					filter = 'ROLE_TEACHER';
				}
				currentData = data.filter((item) => {
					return (item.role.indexOf(filter) >= 0);
				});
				if(!currentData.length){
					currentData = data.filter((item) => {
						return ((item.firstName.indexOf(filter) >= 0)||(item.lastName.indexOf(filter)>=0));
					});
				}
				countOfPages = currentData.length;
				if(type === 'USERS/GET_DATA') {
					pageNumber = 1;
					currentData = makeCurrentData(1, state.allUsers.chunkSize, currentData);
				}
				if(type === 'USERS/CHANGE_CHUNK'){
					pageNumber =  1;
					changeChunk = chunkSize;
					currentData =  makeCurrentData(1,chunkSize, currentData);
				}
				if (type === 'USERS/ON_NEXT') {
					pageNumber = state.allUsers.currentPage + 1 ;
					currentData =  makeCurrentData(state.allUsers.currentPage + 1,state.allUsers.chunkSize, currentData);
				}
				else if (type === 'USERS/ON_PREVIOUS') {
					pageNumber = pageNumber - 1;
					currentData =  makeCurrentData(pageNumber,state.allUsers.chunkSize, currentData);
				}
			}else{
				countOfPages = data.length;
				if(type === 'USERS/GET_DATA'){
					pageNumber = 1;
					currentData = makeCurrentData(1,state.allUsers.chunkSize,data);
				}
				if(type === 'USERS/CHANGE_CHUNK'){
					pageNumber =  1;
					changeChunk = chunkSize;
					currentData =  makeCurrentData(1,chunkSize, data);
				}
				if (type === 'USERS/ON_NEXT') {
					pageNumber = state.allUsers.currentPage + 1 ;
					currentData =  makeCurrentData(state.allUsers.currentPage + 1,state.allUsers.chunkSize, data);
				}
				else if (type === 'USERS/ON_PREVIOUS') {
					pageNumber = pageNumber - 1;
					currentData =  makeCurrentData(pageNumber,state.allUsers.chunkSize, data);
				}
			}
		}
		catch (err) {
			currentData = [];
		}
		dispatch({
			type: type,
			chunkSize: changeChunk,
			currentData: currentData,
			totalPageNumber: countOfPages,
			currentPage: pageNumber,
		});
	};
};

export const rejectRequest = (user_id) => {
	return async (dispatch, getState) => {
		dispatch({
			type:'USERS/REJECT_REQUEST',
		});

		let data = [];
		let body = JSON.stringify({status: false});
		try {
			const res = await request(`api/users/teacher/enable/${user_id}`,
				{
					method: 'PUT',
					body:body,
					headers: {'Content-type': 'application/json'}
				});
			data = await res.response.json();
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'USERS/REJECT_REQUEST',
			currentData: data
		});
	};
};

export const approveRequest = (user_id) => {

	return async (dispatch, getState) => {
		dispatch({
			type:'USERS/APPROVE_REQUEST',
		});

		let data = [];
		let body = JSON.stringify({status: true});
		try {
			const res = await request(`api/users/teacher/enable/${user_id}`,
				{
					method: 'PUT',
					body:body,
					headers: {'Content-type': 'application/json'}
				});
			data = await res.response.json();
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type:'USERS/APPROVE_REQUEST',
			currentData: data
		});
	};

};


export const getData = (filter) => {
	return sendRequest('USERS/GET_DATA', filter);
};

export const onChangeChunk = (filter, chunkSize) => {
	return sendRequest('USERS/CHANGE_CHUNK', filter, chunkSize);
};

export const onNext = (filter) => {
	return sendRequest('USERS/ON_NEXT', filter);
};

export const onPrevious = (filter) => {
	return sendRequest('USERS/ON_PREVIOUS', filter);
};


