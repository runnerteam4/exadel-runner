export function getStartPageData(){
	return async (dispatch) => {
		dispatch({
			type: 'START_PAGE/GET_DATA',
		});
		let data = [];
		try {
			data = {
				topTest: ['Anton Narbutovich', 'Anton Brown','Andrey Butyrchik'],
				topTasks: ['Andrey Butyrchik', 'Anton Narbutovich',
					'Anton Brown'],
				topRating: ['Anton Brown', 'Anton Narbutovich' ,'Andrey Butyrchik'],
				topActivity: ['Anton Narbutovich', 'Andrey Butyrchik','Anton Brown']

			};
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'START_PAGE/GET_DATA',
			data: data
		});
	};
}
