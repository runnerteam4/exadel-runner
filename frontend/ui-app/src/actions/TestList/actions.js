import {TEST_LIST} from '../../reducers/TestList/index';
import {request} from "../fetchWrapper";

const sendRequest = (type, filter, chunkSize) => {
	return async (dispatch, getState) => {
		dispatch({
			type: type,
			isPending: true
		});

		const state = getState();
		let pageNumber = state.testList.pagination.currentPage;
		if (type === `${TEST_LIST}ON_NEXT`) {
			pageNumber++;
		}
		else if (type === `${TEST_LIST}ON_PREVIOUS`) {
			pageNumber--;
		}

		let typeTest;
		if (filter.testType === 'Контрольный') {
			typeTest = 0;
		}
		else if (filter.testType === 'Тренировочный') {
			typeTest = 1;
		}
		else {
			typeTest = null;
		}
		const body = {
			userId: Number(sessionStorage.getItem('id')),
			topics: filter.categories.length ? filter.categories : null,
			type: typeTest,
			name: filter.input ? filter.input : null,
			page: pageNumber,
			chunkSize,
		};
		const res = await request('api/tests',
			{
				method: 'POST',
				body: JSON.stringify(body),
				headers: {'Content-type': 'application/json'}
			}
		);
		const data = await res.response.json();
		console.log(data);
		dispatch({
			type: type,
			chunkSize: chunkSize,
			currentData: data,
			totalPageNumber: data.length,
			currentPage: pageNumber,
			isPending: false,
			status: '',
			error: '',
		});
	};
};

export const getTestList = (filter, chunkSize) => {
	return sendRequest(`${TEST_LIST}GET_TEST_LIST`, filter, chunkSize);
};

export const onChangeChunk = (filter, chunkSize) => {
	return sendRequest(`${TEST_LIST}CHANGE_CHUNK`, filter, chunkSize);
};

export const onNext = (filter, chunkSize) => {
	return sendRequest(`${TEST_LIST}ON_NEXT`, filter, chunkSize);
};

export const onPrevious = (filter, chunkSize) => {
	return sendRequest(`${TEST_LIST}ON_PREVIOUS`, filter, chunkSize);
};

export const getCategories = () => {
	return async (dispatch) => {
		const res = await request('api/wiki/categories', {method: 'GET'});
		const categories = await res.response.json();
		const parseCategories = categories.map((item) => {
			return {
				value: item.id,
				label: item.name,
			};
		});
		dispatch({
			type: `${TEST_LIST}GET_CATEGORIES`,
			payload: parseCategories,
			status: '',
			error: '',
		});
	};
};
