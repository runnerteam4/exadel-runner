export const request = async (url, {method, body, params = '', headers}) => {
	let error = '';
	const newUrl = new URL(`${url}${params}`, 'http://i-runner.herokuapp.com');
	const config = {
		method,
		headers,
		body: body,
		credentials: 'include',
	};
	let response = '';
	try {
		response = await fetch(newUrl, config);
		if (response.status === 401) {
			sessionStorage.clear();
		}
	}
	catch (err) {
		error = err;
		response = '';
	}
	return {
		response,
		error,
	};
};