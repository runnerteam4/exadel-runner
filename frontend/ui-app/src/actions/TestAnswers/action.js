import {TEST_ANSWERS} from '../../reducers/TestAnswers/index';
// import {answers} from "../../mocks/TestPassedStudent/tests";
import {request} from "../fetchWrapper";

export const getTestAnswers = () => {
	return async (dispatch, getState) => {
		dispatch({
			type: `${TEST_ANSWERS}GET_TEST_ANSWERS`
		});
		const state = getState();
		const data = {
			userId: Number(sessionStorage.getItem('id')),
			testId: Number(state.ModalReducer.modalProps.testId),
		};
		const res = await request('api/tests/student/passed',
			{
				method: 'POST',
				body: JSON.stringify(data),
				headers: {'Content-type': 'application/json'}
			}
		);
		const answers = await res.response.json();
		const parseAnswers = answers.map((item) => {
			return {
				name: item.question,
				result: item.correct ? 'Принято' : 'Не принято',
			};
		});

		dispatch({
			type: `${TEST_ANSWERS}GET_TEST_ANSWERS`,
			currentData: parseAnswers,
		});

		return res.error;
	};
};
