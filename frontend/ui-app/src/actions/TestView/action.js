import {request} from '../fetchWrapper';
import {data} from '../../mocks/TestView/data';
import {getAnswers} from "../TestCheck/action";

export function onPrevious() {
	return {
		type: 'TEST/ON_PREVIOUS'
	};
}

export function onNext() {
	return {
		type: 'TEST/ON_NEXT'
	};
}

export function changeAnswer(data) {
	return {
		type: 'TEST/CHANGE_ANSWER',
		payload: data,
	};
}

export function getTestSuccess(response) {
	return {
		type: 'TEST/GET_TEST_SUCCESS',
		payload: response,
	};
}

export function getTestFailed(error) {
	return {
		type: 'TEST/GET_TEST_FAILED',
		payload: error,
	};
}

export function update() {
	return {
		type: 'TEST/UPDATE',
	};
}

export function updateTime(id) {
	return async  (dispatch) => {
		const res = await request('api/tests/currentTime/',
			{
				method: 'GET',
				params: id,
			}
		);
		const seconds = await res.response.json();
		dispatch({
			type: 'TEST/UPDATE_TIME',
			payload: seconds,
		});
	};
}

export function fetchTest(id) {
	return async (dispatch) => {
		const res = await request('api/tests/', {method: 'GET', params: `${sessionStorage.getItem('id')}/${id}`});
		const test = await res.response.json();
		await dispatch(updateTime(test.id));
		dispatch(getTestSuccess(test));
	};
}

export const fetchAnswers = (testId, answers) => {
	return async (dispatch) => {
		const id = sessionStorage.getItem('id');
		const data = {
			userId: parseInt(id),
			testId: parseInt(testId),
			answers,
		};
		const res = await request('api/tests/saveAnswer', {
			method: 'POST',
			body: JSON.stringify(data),
			headers: {'Content-type': 'application/json'}
		});
	};
};
