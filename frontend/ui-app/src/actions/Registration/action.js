import { request } from '../fetchWrapper';

export function sendUser() {
	return {
		type: 'REGISTRATION/SEND_USER',
	};
}

export function sendUserSuccess(response) {
	return {
		type: 'REGISTRATION/SEND_USER_SUCCESS',
		payload: response,
	};
}

export function sendUserFailed(error) {
	return {
		type: 'REGISTRATION/SEND_USER_FAILED',
		payload: error,
	};
}

export function update() {
	return {
		type: 'REGISTRATION/UPDATE',
	};
}

export function fetchUser(user, userStatus) {
	return async (dispatch) => {
		dispatch(sendUser());
		const data = JSON.stringify(user);
		const answer = await request(`api/users/register/${userStatus}`, {method: 'POST', body: data, headers: {'Content-type': 'application/json'} });
		if (answer.error || answer.response.status !== 200) {
			dispatch(sendUserFailed(answer.error.message || answer.response.status));
		} else {
			dispatch(sendUserSuccess(answer.response));
		}
	};
}
