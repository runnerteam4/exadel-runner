import {TASK_TEACHER} from '../../reducers/TaskTeacher/taskTeacher';
import {groups, tasks, allTasks} from '../../mocks/TaskTeacher/tasks';
import {request} from "../fetchWrapper";


export const getTasks = (filter) => {
	return async (dispatch, getState) => {
		let tasks;
		try {
			if(!filter) {
				let res = await request(`api/tasks/teacher/${sessionStorage.getItem('id')}`, {method: 'GET'});
				tasks = await res.response.json();
				if (res.response.status !== 200) {
					tasks = [];
				}
			}else{
				const state = getState();
				tasks = state.taskTeacher.tasks;
				tasks = tasks.filter((item) => {
					return (item['taskName'].indexOf(filter) >= 0);
				});
			}
		} catch (err) {
			tasks = []
		}
		dispatch({
			type: `${TASK_TEACHER}GET_TASKS`,
			tasks,
		});
	};

};


export const getGroups = () => {
	return async (dispatch) => {
		const formData = (item, index) => {
			const {id, createDate, name} = item;
			return {value: id, label: name};
		};
		let error = '';
		let categories;
		try {
			const res = await request('/api/wiki/categories', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);
		}

		catch (err) {
			error = err;
			categories = [{}];
		}
		dispatch({
			type: `${TASK_TEACHER}GET_CATEGORIES`,
			categories: categories,
		});
	};
};

const makeCurrentData = (currentPage, chunkSize, data) => {
	const indexOfLastTodo = currentPage * chunkSize;
	const indexOfFirstTodo = indexOfLastTodo - chunkSize;
	return data.slice(indexOfFirstTodo, indexOfLastTodo);
};

const sendRequest = (type, filter, chunkSize) => {
	// return async (dispatch, getState) => {
	// 	dispatch({
	// 		type: type,
	// 	});
	//
	// 	const state = getState();
	// 	let pageNumber = state.taskTeacher.currentPage;
	//
	// 	if (type === `${TASK_TEACHER}ON_NEXT`) {
	// 		pageNumber++;
	// 	}
	// 	else if (type === `${TASK_TEACHER}ON_PREVIOUS`) {
	// 		pageNumber--;
	// 	}
	//
	// 	let data = [];
	// 	try {
	// 		data = allTasks;
	// 		//data = await fetch('https://i-runner.herokuapp.com/');
	// 		//status = 'success';
	// 	}
	// 	catch (err) {
	// 		data = [];
	// 	}
	// 	dispatch({
	// 		type: type,
	// 		chunkSize: chunkSize,
	// 		allTasks: data,
	// 		totalPageNumber: 20, // с сервера
	// 		currentPage: pageNumber,
	// 	});
	// };
	return async (dispatch, getState) => {
		dispatch({
			type: type,
		});
		const state = getState();
		let pageNumber = state.taskTeacher.currentPage;
		let currentData = [];
		let changeChunk = chunkSize || state.taskTeacher.chunkSize;
		let countOfPages;
		let data = [];
		try {
			const res = await request('/api/tasks', {method: 'GET'});
			data = await res.response.json();
			if (filter) {
				currentData = data.filter((item) => {
					return (item.name.indexOf(filter) >= 0);
				});
				countOfPages = currentData.length;
				if (type === `${TASK_TEACHER}GET_ALL_TASKS`) {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === `${TASK_TEACHER}CHANGE_CHUNK`) {
					pageNumber = 1;
					changeChunk = chunkSize;
					currentData = makeCurrentData(1, changeChunk, currentData);
				}
				if (type === `${TASK_TEACHER}ON_NEXT`) {
					pageNumber = pageNumber + 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
				else if (type === `${TASK_TEACHER}ON_PREVIOUS`) {
					pageNumber = pageNumber - 1;
					currentData = makeCurrentData(pageNumber, changeChunk, currentData);
				}
			} else {
				countOfPages = data.length;
				if (type === `${TASK_TEACHER}GET_ALL_TASKS`) {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, data);
				}
				if (type === `${TASK_TEACHER}CHANGE_CHUNK`) {
					pageNumber = 1;
					currentData = makeCurrentData(1, changeChunk, data);
				}
				if (type === `${TASK_TEACHER}ON_NEXT`) {
					pageNumber = pageNumber + 1;
					currentData = makeCurrentData(pageNumber, changeChunk, data);
				}
				else if (type === `${TASK_TEACHER}ON_PREVIOUS`) {
					pageNumber = pageNumber - 1;
					currentData = makeCurrentData(pageNumber, changeChunk, data);
				}
			}
		}
		catch
			(err) {
			currentData = [];
		}
		dispatch({
			type: type,
			chunkSize: changeChunk,
			allTasks: currentData,
			totalPageNumber: countOfPages, // с сервера
			currentPage: pageNumber,
		});
	};
};

export const getAllTasks = (filter) => {
	return sendRequest(`${TASK_TEACHER}GET_ALL_TASKS`, filter);
};

export const onChangeChunk = (filter, chunkSize) => {
	return sendRequest(`${TASK_TEACHER}CHANGE_CHUNK`, filter, chunkSize);
};

export const onNext = (filter) => {
	return sendRequest(`${TASK_TEACHER}ON_NEXT`, filter);
};

export const onPrevious = (filter) => {
	return sendRequest(`${TASK_TEACHER}ON_PREVIOUS`, filter);
};

export const getTask = (filter) => {
	return async (dispatch, getState) => {
		let isFetching = true;
		let task;
		try {
			const res = await request('/api/tasks', {method: 'GET'});
			const data = await res.response.json();
			task = data.findIndex((item) => {
				return (item.id == filter);
			});
			task = data[task];
			isFetching = false;
		}
		catch (err) {
			task = {};
		}
		dispatch({
			type: `${TASK_TEACHER}GET_TASK`,
			task: task,
			isFetching
		});
	};
};


export function addTask() {
	return async (dispatch, getState) => {
		const formData = (item, index) => {
			const {id, createDate, name} = item;
			return {value: id, label: name};
		};
		let categories;
		try {
			const res = await request('/api/wiki/categories', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);
		}
		catch (err) {
			categories = [];
		}
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'ADD_TASK',
			modalProps: {
				category: categories
			}
		});
	};
}

export function deleteTask(id) {
	return async (dispatch) => {
		let error = '';
		let task = [];
		let isFetching = true;
		try {
			let res = await request(`/api/tasks/${id}/delete`, {method: 'DELETE'});
			if(res.response.status === 200){
				res = await request('/api/tasks', {method: 'GET'});
				const data = await res.response.json();
				task = data.findIndex((item) => {
					return (item.id == id);
				});
				try {
					task = data[task] || [];
					isFetching = false;
				}
				catch (e) {
					task = [];
					isFetching = false;
				}
			}
		}
		catch (err) {
			error = err;
		}
		dispatch({
			type: 'HIDE_MODAL'
		});
		dispatch({
			type: `${TASK_TEACHER}GET_TASK`,
			task: task,
			isFetching
		});
		return error;
	};
}

export function addTaskfile(taskId) {
	return {
		type: 'SHOW_MODAL',
		modalType: 'ADD_TASKfiLE',
		modalProps: {
			taskId,
		}
	};
}