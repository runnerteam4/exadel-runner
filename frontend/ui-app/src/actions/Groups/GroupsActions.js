import {request} from "../fetchWrapper";

export function getGroups() {
	return async (dispatch) => {
		dispatch({
			type: 'GROUPS/GETDATA',
		});
		let currentData = [];
		try {
			const id = sessionStorage.getItem('id');
			const res = await request(`/api/groups/teacher/${id}`, {method: 'GET'});
			currentData = await res.response.json();
			if (res.response.status === 400) {
				currentData = [];
			}
		}
		catch (err) {
			currentData = [];
		}
		dispatch({
			type: 'GROUPS/GETDATA',
			data: currentData
		});
	};
}

export function sendNewGroupName(name, group_id) {
	return async (dispatch) => {
		let data = [];
		const config = JSON.stringify({name: name});
		try {

			let res = await request(`/api/groups/change/${group_id}`, {
				method: 'PUT',
				body: config,
				headers: {'Content-type': 'application/json'}
			});
			if (res.response) {
				const resGroup = await request(`/api/groups/teacher/${sessionStorage.getItem('id')}`, {method: 'GET'});
				data = await resGroup.response.json();
				if (resGroup.response.status === 400) {
					data = [];
				}
			}
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'GROUPS/CHANGE_GROUP_NAME',
			data: data
		});
		dispatch(
			{type: 'HIDE_MODAL'}
		);
	};
}

export function deleteGroup(group_id) {
	return async (dispatch) => {
		let data = [];
		try {
			let res = await request(`/api/groups/delete/${group_id}`, {method: 'DELETE'});
			if (res.response) {
				const resGroup = await request(`/api/groups/teacher/${sessionStorage.getItem('id')}`, {method: 'GET'});
				data = await resGroup.response.json();
				if (resGroup.response.status === 400) {
					data = [];
				}
			}
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'GROUPS/DELETE',
			data: data
		});
		dispatch(
			{type: 'HIDE_MODAL'}
		);
	};
}

export const addNewGroup = (students, groupName) => {
	return async (dispatch) => {
		let data = [];
		const config = JSON.stringify({name: groupName, studentIds: students});
		try {
			let res = await request('/api/groups/add', {
				method: 'POST',
				body: config,
				headers: {'Content-type': 'application/json'}
			});
			res = await res.response.json();
			const id_group = res.id;
			res = await request(`/api/groups/assign/${sessionStorage.getItem('id')}/${id_group}`, {method: 'PUT'});
			if (res.response) {
				const resGroup = await request(`/api/groups/teacher/${sessionStorage.getItem('id')}`, {method: 'GET'});
				data = await resGroup.response.json();
				if (resGroup.response.status === 400) {
					data = [];
				}
			}
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'GROUPS/ADD',
			data: data,
		});
	};
};

export const addStudents = (students, groupID) => {
	return async (dispatch) => {
		dispatch({
			type: 'GROUP/ADD',
		});
		let data = [];

		const config = JSON.stringify({studentIds: students});
		try {
			let res = await request(`/api/groups/add/${groupID}`, {
				method: 'PUT',
				body: config,
				headers: {'Content-type': 'application/json'}
			});
			res = await res.response.json();
		}
		catch (err) {
			data = [];
		}
	};
};

export function update() {


	return async (dispatch) => {
		let data = [];
		try {
			const resGroup = await request(`/api/groups/teacher/${sessionStorage.getItem('id')}`, {method: 'GET'});
			data = await resGroup.response.json();
			if (resGroup.response.status === 400) {
				data = [];
			}
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'GROUPS/UPDATE',
			data: data
		});
	};
}