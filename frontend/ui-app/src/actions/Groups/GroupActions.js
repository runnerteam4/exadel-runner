import {request} from "../fetchWrapper";

export function excludeStudent(student_id, group_id){
	return async (dispatch) => {

		let data = [];
		let currentData = [];
		let name;
		let body = JSON.stringify({groupId: group_id});
		try {
			let res = await request(`/api/groups/exclude/${student_id}`, {method: 'DELETE',
				body: body,
				headers: {'Content-type': 'application/json'}
			});
			if (res.response) {
				res = await request(`/api/groups/${group_id}`, {method: 'GET'});
				currentData = await res.response.json();
				 name = currentData.name;
				data = currentData.users;
			}
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'GROUP/EXCLUDE_STUDENT',
			data: data,
			name: name
		});
		dispatch(
			{type: 'HIDE_MODAL'}
		);
	};
}

export function getData(group_id){
	return async (dispatch) => {
		dispatch({
			type: 'GROUP/GET_DATA',
		});
		let data = [];
		let name;
		let currentData = [];
		try {
			const res = await request(`/api/groups/${group_id}`, {method: 'GET'});
			currentData = await res.response.json();
			name = currentData.name;
			data = currentData.users;
		}
		catch (err) {
			data = [];
		}
		dispatch({
			type: 'GROUP/GET_DATA',
			data: data,
			name: name
		});
	};
}
