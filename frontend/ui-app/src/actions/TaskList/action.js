import {TASK_LIST} from '../../reducers/TaskList/taskList';
import {tasks} from '../../mocks/TaskListView/data';
import {request} from "../fetchWrapper";


export const getCategories = () => {
	return async (dispatch) => {
		const formData = (item, index) => {
			const {id, createDate, name} = item;
			return {value: id, label: name};
		};
		let categories = [];
		let error = '';
		try {
			const res = await request('/api/wiki/categories', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);
		}

		catch (err) {
			error = err;
			categories = [{}];
		}
		dispatch({
			type: `${TASK_LIST}GET_CATEGORIES`,
			categories
		});
		return error;
	};
};


export const getTasks = () => {
	return async (dispatch) => {
		let tasks = [];
		let error = '';
		try {
			let res = await request(`api/tasks/student/${sessionStorage.getItem('id')}`, {method: 'GET'});
			tasks = await res.response.json();
			console.log(tasks);
		}
		catch (err) {
			error = err;
			tasks = [];
		}

		dispatch({
			type: `${TASK_LIST}GET_TASKS`,
			currentData: tasks,
			tasks
		});
		return error;
	};
};

export const getTasksFilter = (filter) => {
	return async (dispatch, getState) => {
		let error = '';
		const state = getState();
		let tasks = state.taskList.currentData;
		try {
			if (filter) {
				tasks = tasks.filter((item) => {
					return (item.name.indexOf(filter) >= 0);
				});
			}
		}
		catch (err) {
			error = err;
			tasks = [];
		}

		dispatch({
			type: `${TASK_LIST}GET_TASKS_FILTER`,
			tasks,
		});
		return error;
	};
};