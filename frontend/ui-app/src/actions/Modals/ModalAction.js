import {request} from '../fetchWrapper';

export function registrationSuccess(text) {
	return {
		type: 'SHOW_MODAL',
		modalType: 'REGISTRATION_SUCCESS',
		modalProps: {
			text,
		}
	};
}

export function registrationFailed(text) {
	return {
		type: 'SHOW_MODAL',
		modalType: 'REGISTRATION_FAILED',
		modalProps: {
			text,
		}
	};
}


export function showMessage() {
	return {
		type: 'SHOW_MODAL',
		modalType: 'WAITING_CHECK',
		modalProps: {}
	};
}

export function showAnswersModal(testId) {
	return {
		type: 'SHOW_MODAL',
		modalType: 'ANSWERS',
		modalProps: {testId}
	};
}

export function addStudent() {
	return {
		type: 'SHOW_MODAL',
		modalType: 'ADD_STUDENT',
		modalProps: {}
	};
}

export function hideModal() {
	return {
		type: 'HIDE_MODAL',
	};
}


export function addTest() {
	return {
		type: 'SHOW_MODAL',
		modalType: 'ADD_TEST',
		modalProps: {
			topic: [
				{value: 'one', label: 'Раздел1'},
				{value: 'two', label: 'Раздел2'},
			]
		}
	};
}

export function confirmDeleting(group_id) {

	return async (dispatch) => {
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'CONFIRM_DELETING',
			modalProps: {
				id: group_id,
				text:'Вы действительно хотите удалить группу?'
			}
		});
	};
}


export function confirmExcluding(student, group) {
	return {
		type: 'SHOW_MODAL',
		modalType: 'CONFIRM_EXCLUDE_STUDENT',
		modalProps: {
			groupId: group,
			studentId: student,
			text: 'Вы действительно хотите удалить студента из группы?',
		}
	};
}

export function confirmDeleteTask(id) {
	return {
		type: 'SHOW_MODAL',
		modalType: 'CONFIRM_DELETE_TASK',
		modalProps: {
			text: 'Вы действительно хотите удалить задачу?',
			taskId: id
		}
	};
}

export function changeGroupName(group_id) {

	return async (dispatch) => {
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'EDIT_GROUP_NAME',
			modalProps: {
				id: group_id
			}
		});
	};
}

export function addCategory() {
	return {
		type: 'SHOW_MODAL',
		modalType: 'ADD_CATEGORY',
		modalProps: {}
	};
}

export function assignTask(student_id, role) {
	return async (dispatch, getState) => {
		const formData = (item, index) => {
			const {id,name} = item;
			return {value: id, label: name};
		};
		let categories;
		try {
			const res = await request('/api/tasks', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);
		}
		catch(err){
			categories = [];
		}
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'ASSIGN_TASK',
			modalProps: {
				tasks: categories,
				id: student_id,
				role: role
			}
		});
	};
}

export function assignTest(student_id, role) {
	return async (dispatch) => {
		const formData = (item) => {
			const {id, name} = item;
			return {value: id, label: name};
		};
		let categories;
		try {
			const res = await request('/api/wiki/categories', {method: 'GET'});
			categories = await res.response.json();
			categories = categories.map(formData);
		}
		catch(err){
			categories = [];
		}
		dispatch({
			type: 'SHOW_MODAL',
			modalType: 'ASSIGN_TEST',
			modalProps: {
				category: categories,
				id: student_id,
				role: role
			}
		});
	};
}


export function sendNewQuestion(body) {
	return async (dispatch) => {
		const res = await request('api/questions/add', {
			method: 'POST' , body: JSON.stringify(body),
			headers: {'Content-type': 'application/json'}
		});
		dispatch({
			type: 'HIDE_MODAL',
		});
	};
}

export function sendNewTask(body, files) {
	return async (dispatch) => {

		const res = await request('/api/tasks/new', {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				'Content-type': 'application/json',
				'Accept': 'application/json',
			}
		});

		const data = await res.response.json();

		const answer = await request(`/api/tasks/${data.id}/test/new`, {
			method: 'POST',
			body: JSON.stringify(files),
			headers: {
				'Content-type': 'application/json',
				'Accept': 'application/json',
			}
		});

		dispatch({
			type: 'HIDE_MODAL'
		});
	};
}

// doesn't work
export function sendAdditionalTaskTest(id, files) {
	return async (dispatch) => {
		const answer = await request(`/api/tasks/${id}/test/new`, {
			method: 'POST',
			body: JSON.stringify(files),
			headers: {
				'Content-type': 'application/json',
				'Accept': 'application/json',
			}
		});

		dispatch({
			type: 'HIDE_MODAL'
		});
	};
}



export function sendAssigntest(body, role) {
	return async (dispatch) => {
		let currentData = JSON.stringify(body);
		let error = '';
		try {
			if(role === 'student') {
				const res = await request('/api/tests/assign/student', {
					method: 'POST', body: currentData,
					headers: {'Content-type': 'application/json'}
				});
			}else{
				const res = await request('/api/tests/assign/group', {
					method: 'POST', body: currentData,
					headers: {'Content-type': 'application/json'}
				});
			}
		}
		catch (err) {
			error = err;
		}
		dispatch({
			type: 'HIDE_MODAL'
		});
		return error;
	};
}

export function sendAssigntask(body, role) {
	return async (dispatch, getState) => {
		const state = getState();
		let currentData = JSON.stringify(body);
		let error = '';
		try {
			if(role === 'student') {
				const res = await request('/api/tasks/assign/student', {
					method: 'POST' , body: currentData,
					headers: {'Content-type': 'application/json'}
				});
			}else{
				const res = await request('/api/tasks/assign/group', {
					method: 'POST', body: currentData,
					headers: {'Content-type': 'application/json'}
				});
			}
		}
		catch (err) {
			error = err;
		}
		dispatch({
			type: 'HIDE_MODAL'
		});
		return error;
	};
}
