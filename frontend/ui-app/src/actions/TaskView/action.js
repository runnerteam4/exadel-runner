import {request} from '../fetchWrapper';
import {data} from '../../mocks/TaskView/data';

export function getTaskSuccess(response) {
	return {
		type: 'TASK/GET_TASK_SUCCESS',
		payload: response,
	};
}

export function getTaskFailed(error) {
	return {
		type: 'TASK/GET_TASK_FAILED',
		payload: error,
	};
}

export function fetchTask(taskId, userId) {
	return async (dispatch) => {
		const res = await request(`api/tasks/${taskId}/students/${userId}`, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
			}
		});

		const data = await res.response.json();
		if (res.error || res.response.status !== 200) {
			dispatch(getTaskFailed(res.error.message || res.response.status));
		} else {
			dispatch(getTaskSuccess(data));
		}
	};
}

export function fetchTaskAnswer(taskId, userId, files) {
	return async (dispatch) => {
		const file = new FormData();
		file.append('file', files);
		const res = await request(`/api/tasks/student/${userId}/task/${taskId}/run`, {
			method: 'POST',
			body: file,
		});

		if (res.response.status === 200) {
			const data = await res.response.json();
		}
	}
}
