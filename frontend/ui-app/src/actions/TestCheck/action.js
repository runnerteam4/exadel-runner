import {TEST_CHECK} from '../../reducers/TestCheck/testCheck';
import {answers as Answ} from '../../mocks/TestCheck/tests';
import {request} from '../fetchWrapper';
import {tests as mockTests} from "../../mocks/TestsTeacher/tests";

export const getAnswers = (testID) => {
	return async (dispatch) => {
		let parseAnswers;
		try {
			const res = await request('api/tests/answers/check/',
				{
					method: 'GET',
					params: testID,
					headers: {'Content-type': 'application/json'}
				}
			);
			const answers = await res.response.json();
			parseAnswers = answers.map((item) => {
				return {
					question: item.questionText,
					answer: item.answer,
					id: item.id,
				};
			});
		}
		catch(err){
			parseAnswers = [];
		}
		dispatch({
			type: `${TEST_CHECK}GET_ANSWERS`,
			answers: parseAnswers,
		});
		return parseAnswers;
	};
};

export const changeInputValue = (id, mark) => {
	return {
		type: `${TEST_CHECK}CHANGE_INPUT_VALUE`,
		mark: mark || '',
		id,
	};
};

export const changeCommentValue = (id, comment) => {
	return {
		type: `${TEST_CHECK}CHANGE_COMMENT_VALUE`,
		comment: comment || '',
		id,
	};
};

export const approveCheck = (testId) => {
	return async (dispatch, getState) => {
		const answers = getState().testCheck.answers;
		const comment = getState().testCheck.comment;
		const isFilled = answers.every(answer => answer.mark);

		let res = [];
		if (isFilled) {
			const parseBody = answers.map((item) => {
				return {
					mark: item.mark,
					answerId: item.id,
				};
			});
			res = await request('api/tests/answers/checked',
				{
					method: 'PUT',
					body: JSON.stringify({marks: parseBody, comment, testId}),
					headers: {'Content-type': 'application/json'},
				}
			);
		}

		dispatch({
			type: `${TEST_CHECK}APPROVE_CHECK`
		});

		return res;
	};
};
