import {TEST_PASSED_STUDENT} from '../../reducers/TestPassedStudent/testList';
// import {tests} from "../../mocks/TestPassedStudent/tests";
import {request} from "../fetchWrapper";


const parseDate = (date) => {
	const year = date[0];
	const month = date[1] < 10 ? `0${date[1]}` : date[1];
	const day = date[2] < 10 ? `0${date[2]}` : date[2];
	return `${day}.${month}.${year}`;
};

export const getTestsPassedStudent = () => {
	return async (dispatch) => {
		dispatch({
			type: `${TEST_PASSED_STUDENT}GET_TEST_LIST`
		});

		const id = sessionStorage.getItem('id');
		const res = await request('api/tests/student/', {method: 'GET', params: id});
		const tests = await res.response.json();
		const parseTest = tests.map((item) => {
			return {
				name: item.name,
				type: item.training ? 'Тренировочный' : 'Контрольный',
				date: parseDate(item.endTime),
				result: item.mark,
				comment: item.comment,
				id: item.testId,
			};
		});
		dispatch({
			type: `${TEST_PASSED_STUDENT}GET_TEST_LIST`,
			currentData: parseTest,
		});

		return res.error;
	};
};
