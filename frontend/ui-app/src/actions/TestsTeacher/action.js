import {TEST_TEACHER} from '../../reducers/TestsTeacher/testList';
import {request} from '../fetchWrapper';
// import {tests as mockTests} from '../../mocks/TestsTeacher/tests';

const parseDate = (date) => {
	const year = date[0];
	const month = date[1] < 10 ? `0${date[1]}` : date[1];
	const day = date[2] < 10 ? `0${date[2]}` : date[2];
	return `${day}.${month}.${year}`;
};

export const getTestsCheckTeacher = (stud = null, group = null) => {
	return async (dispatch) => {
		const body = JSON.stringify({
			teacherId: sessionStorage.getItem('id'),
			group,
			studentName: stud,
		});
		const res = await request('api/tests/checkList',
			{
				method: 'POST',
				body,
				headers: {'Content-type': 'application/json'}
			}
		);
		const tests = await res.response.json();
		const [groups, student, name, type, date] = ['Группа', 'Имя ученика', 'Название', 'Тип теста', 'Дата прохождения'];
		const parseTests = tests.map((item) => {
			return {
				[groups]: item.group,
				[student]: item.student,
				[name]: item.test,
				[type]: item.training ? 'Тренировочный' :'Контрольный',
				[date]: parseDate(item.endDate),
				id: item.testId,
			};
		});
		dispatch({
			type: `${TEST_TEACHER}GET_TESTS`,
			tests: parseTests || tests.error
		});

		return parseTests;
	};
};
