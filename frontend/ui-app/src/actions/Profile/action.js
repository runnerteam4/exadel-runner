import {request} from '../fetchWrapper';


function getUser() {
	return {
		type: 'PROFILE/GET_USER',
	};
}

function getUserSuccess(userInformation) {
	return {
		type: 'PROFILE/GET_USER_SUCCESS',
		payload: userInformation,
	}
}

function getUserFailed(error) {
	return {
		type: 'PROFILE/GET_USER_FAILED',
		payload: error,
	}
}

export const getProfileData = (userId) => {
	return async (dispatch) => {
		dispatch(getUser());
		const answer = await request(`api/users/info/${userId}`, {
			method: 'GET',
			headers: {'Accept': 'application/json'}
		});
		if (answer.error || answer.response.status !== 200) {
			dispatch(getUserFailed(answer.error.message || answer.response.status));
		} else {
			dispatch(getUserSuccess(await answer.response.json()));
		}
	};
};

export const changePassword = (currentPassword, newPassword, userId) => {
	return async (dispatch) => {
		let error = '';
		const answer = await request(`/api/users/change/${userId}`, {
			method: 'PUT',
			body: JSON.stringify({
				currentPassword,
				newPassword
			}),
			headers: {'Content-type': 'application/json'}
		});
		const isValid = answer.response.status === 200;
		return {
			error,
			isValid,
		};
	};
};

export function ModalChangePassword() {
	return{
		type:'SHOW_MODAL',
		modalType: 'CHANGE_PASSWORD',
		modalProps:{}
	};
}