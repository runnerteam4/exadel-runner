import React, {Component} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import Button from "../../components/Button/Button";
import {addTaskfile} from "../../actions/TaskTeacher/action";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {confirmDeleteTask} from "../../actions/Modals/ModalAction";

export default class startPageItem extends Component {
	render() {
		const classes = classNames(
			'list-group-item',
			this.props.className,
			'd-flex justify-content-between'
		);
		return <div className={classes}>
			<li>
				{this.props.children}
			</li>
		</div>;
	}
}