import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getStartPageData} from "../../actions/StartPage/action";
import List from "../../components/List/List";
import ListItem from "../../components/List/ListItem";


class StartPage extends Component {

	renderListItem = (item, index, checkedItems, click) => {
		return (
			<ListItem id={index}
					  selectable={false}
					  className="d-flex justify-content-center ">
				<small>{item}</small>
			</ListItem>
		);
	};

	componentDidMount() {
		this.props.getData();
	}

	render() {
		const {topTest, topTasks, topRating, topActivity} = this.props.data;
		return <React.Fragment>
			<div className="container row justify-content-center mt-1 ml-1">
				<div className="col-6">
					<form className="border text-center shadow justify-content-center">
						<h5>Лучшие в тестировании</h5>
						<List
							className="mt-2"
							data={topTest}
							renderListItem={this.renderListItem}
						/>
					</form>
				</div>
				<div className="col-6">
					<form className="border text-center shadow">
						<h5>Лучшие в задачах</h5>
						<List
							className="mt-2"
							data={topTasks}
							renderListItem={this.renderListItem}
						/>
					</form>
				</div>
				<div className="col-6">
					<form className="border text-center mt-4 mb-2 shadow">
						<h5>Лучшие по рейтингу</h5>
						<List
							className="mt-2"
							data={topRating}
							renderListItem={this.renderListItem}
						/>
					</form>
				</div>
				<div className="col-6">
					<form className="border text-center mt-4 mb-2 shadow">
						<h5>Активные студенты</h5>
						<List
							className="mt-2"
							data={topActivity}
							renderListItem={this.renderListItem}
						/>
					</form>
				</div>
			</div>
		</React.Fragment>;
	}
}


const mapStateToProps = ({startpage}) => ({
	data: startpage.data,
});

const mapDispatchToProps = (dispatch) => ({
	getData: bindActionCreators(getStartPageData, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(StartPage);