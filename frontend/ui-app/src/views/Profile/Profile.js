import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import ModalRoot from "../../components/Modals/ModalRoot";
import {getProfileData, ModalChangePassword} from '../../actions/Profile/action';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';
import Button from "../../components/Button/Button";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons/faEnvelope";
import PropTypes from "prop-types";


class Profile extends React.Component {
	async componentDidMount() {
		await this.props.getProfileData(this.props.match.params.id || sessionStorage.getItem('id'));
	}

	render() {
		let {role, data} = this.props;
		let isStudent = ((role || sessionStorage.getItem('role')) === 'ROLE_STUDENT');
		if(this.props.match.params.id !== sessionStorage.getItem('id')){
			isStudent = true;
			role = 'ROLE_STUDENT';
		}
		const targetID = Number(this.props.match.params.id);

		const user = {
			'ROLE_STUDENT': 'Студент',
			'ROLE_TEACHER': 'Учитель',
			'ROLE_ADMIN': 'Админ',
		};

		return (
			<React.Fragment>
				{ !this.props.isFetching && !this.props.error &&
				<div className="d-flex justify-content-center">
					<div>
						<div>
							<div className=" d-flex ">
								<h3>{data.firstName} {data.lastName}</h3>
								<span className="d-flex align-items-center ml-2 font-italic">{user[role || sessionStorage.getItem('role')]}</span>
							</div>
						</div>
						<div className="d-flex align-items-center">
							<FontAwesomeIcon
								icon={faEnvelope}
								className="remove-cursor mr-2"
								onClick={this.removeFile}/>
							<address className="m-0">
								<a href={`mailto:${data.email}`}>{data.email}</a>
							</address>
						</div>
						{
							isStudent
								?
								(
									<div>
										<div className="d-flex border-bottom mt-2">
											<b>Университет:</b>
											<div className="ml-2">{data.university}</div>
										</div>
										<div className="d-flex border-bottom mt-2">
											<b>Факультет:</b>
											<div className="ml-2">{data.faculty}</div>
										</div>
										<div className="d-flex border-bottom mt-2">
											<b>Год окончания:</b>
											<div className="ml-2">{data.graduationYear}</div>
										</div>
									</div>
								)
								:
								(
									<div className="d-flex border-bottom mt-2">
										<b>Место работы</b>
										<div className="ml-2">{data.university}</div>
									</div>
								)

						}
						<div className="d-flex border-bottom mt-2">
							<b>Основные навыки:</b>
							<div className="ml-2">{data.primarySkill}</div>
						</div>
						{( targetID === Number(this.props.id || sessionStorage.getItem('id'))) && <div className="col">
							<Button
								className="w-100 mt-3"
								onClick={this.props.changePassword}>
								Изменить пароль
							</Button>
						</div>}
					</div>
					<ModalRoot/>
				</div>
				}
				{
					this.props.isFetching &&
					<div className="d-flex h-50">
						<Loading/>
					</div>
				}
				{
					this.props.error &&
					<ErrorPage textError={this.props.error}/>
				}
			</React.Fragment>
		);
	}
}

Profile.propTypes ={
	visible: PropTypes.bool
};

Profile.defaultProps = {
	visible: false
};

const mapStateToProps = ({profile, login}) => ({
	data: profile.data,
	isFetching: profile.isFetching,
	error: profile.error,
	id: login.id,
	role: login.role
});

const mapDispatchToProps = (dispatch) => ({
	getProfileData: bindActionCreators(getProfileData, dispatch),
	changePassword: bindActionCreators(ModalChangePassword,dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
