import React, {Component} from 'react';
import Table from '../../components/Table/Table';
import {connect} from 'react-redux';
import GroupsRow from './GroupsRow';
import ModalRoot from "../../components/Modals/ModalRoot";
import {assignTask, assignTest, addStudent, confirmExcluding} from "../../actions/Modals/ModalAction";
import {bindActionCreators} from 'redux';
import {NavLink} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlusSquare} from "@fortawesome/free-solid-svg-icons/faPlusSquare";
import {getData} from "../../actions/Groups/GroupActions";


class Group extends Component {

	componentDidMount() {
		this.props.getData(this.props.match.params.id);
	}
	render() {
		const headers = [
			{key:'name', label: 'name'},
			{key:'', label: ''}
		];
		const getRow =(item,index,headers) => (
			<GroupsRow assignTask={this.props.assignTask}
					   assignTest={this.props.assignTest}
					   confirm={this.props.confirmExcluding}
					   role = {'student'}
					   group={this.props.match.params.id}
					   item={item}
					   key={index}
					   headers={headers}
					   link = {`/app/profile/${this.props.data[index].id}`} />
		);
		const {data}= this.props;
		return <React.Fragment>
			<div className="d-flex justify-content-between">
				<h3>
					{this.props.name}
				</h3>
				<NavLink to= {`/app/groups/group/addStudent/${this.props.match.params.id}`} className = "text-dark mr-4">
					<FontAwesomeIcon icon={faPlusSquare} className="fa-2x"/>
				</NavLink>
			</div>
			<div className= "mt-3">
				<Table headers={headers} hideHeaders={true} data={data} renderRow={getRow}/>
			</div>
			<ModalRoot/>
		</React.Fragment>;
	}
}


const mapStateToProps = ({GroupReducer}) => ({
	data: GroupReducer.group.data,
	name: GroupReducer.group.name
});

const mapDispatchToProps = (dispatch) => ({
	assignTask: bindActionCreators(assignTask, dispatch),
	assignTest: bindActionCreators(assignTest, dispatch),
	addStudent: bindActionCreators(addStudent, dispatch),
	confirmExcluding: bindActionCreators(confirmExcluding, dispatch),
	getData: bindActionCreators(getData, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(Group);
