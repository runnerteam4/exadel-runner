import React, {Component} from 'react';
import Table from '../../components/Table/Table';
import {connect} from 'react-redux';
import GroupsRow from './GroupsRow';
import ModalRoot from "../../components/Modals/ModalRoot";
import {assignTask, assignTest, changeGroupName, confirmDeleting} from "../../actions/Modals/ModalAction";
import {bindActionCreators} from 'redux';
import {NavLink} from 'react-router-dom';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlusSquare} from '@fortawesome/free-solid-svg-icons/faPlusSquare';
import {getGroups, update} from "../../actions/Groups/GroupsActions";


class Groups extends Component {
	componentDidMount(){
		this.props.getGroups(this.props.id);
	}

	render() {
		const headers = [
			{key:'name', label: 'name'},
			{key:'', label: ''}];
		const getRow =(item,index,headers) => (
			<GroupsRow assignTask={this.props.assignTask} confirm={this.props.confirmDeleting}
					   role = {'group'}
					   changeGroupName = {this.props.changeGroupName} assignTest={this.props.assignTest}
					   item={item} key={index} headers={headers} editGroup = {true} link = {`/app/groups/group/${item.id}`}/>
		);
		const {data}= this.props;
		return <React.Fragment>
			<div className= "d-flex justify-content-between ml-2">
				<h3>
					Группы
				</h3>
				<NavLink to= "/app/groups/new" className = "text-dark mr-4">
						<FontAwesomeIcon icon={faPlusSquare} className="fa-2x"/>
				</NavLink>
			</div>
			<div className="mt-3">
				<Table headers={headers} hideHeaders={true} data={data} renderRow={getRow}/>
			</div>
			{(this.props.data) && (!this.props.data.length) && <label>Вы не создали ни одной группы</label>}
			<ModalRoot/>
		</React.Fragment>;
	}
}


const mapStateToProps = ({GroupReducer, login}) => ({
	data: GroupReducer.groups.data,
	id: login.id
});

const mapDispatchToProps = (dispatch) => ({
	assignTask: bindActionCreators(assignTask, dispatch),
	assignTest: bindActionCreators(assignTest, dispatch),
	changeGroupName: bindActionCreators(changeGroupName, dispatch),
	confirmDeleting:bindActionCreators(confirmDeleting, dispatch),
	getGroups:bindActionCreators(getGroups,dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(Groups);
