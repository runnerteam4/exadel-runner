import React from 'react';
import NewGroup from "./NewGroup";


const AddStudent = (props) => {
	return (
		<NewGroup id = {props.match.params.id} addStudent = {true} title = "Добавление студента в группу"/>
	);
};

export default AddStudent;