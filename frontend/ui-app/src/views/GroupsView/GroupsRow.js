import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Button from "../../components/Button/Button";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPenSquare} from '@fortawesome/free-solid-svg-icons/faPenSquare';


export default class GroupsRow extends Component {
	assignTest = () => {
		this.props.assignTest(this.props.item.id, this.props.role);
	};
	assignTask = () => {
		this.props.assignTask(this.props.item.id, this.props.role);
	};
	confirm = () => {
		if(this.props.group){
			this.props.confirm(this.props.item.id, this.props.group);
		}else{
			this.props.confirm(this.props.item.id);
		}
	};
	changeGroupName = () => {
		this.props.changeGroupName(this.props.item.id);
	};
	renderItemElement = (header, index) => {
		if (header.label === 'name') {
			if (this.props.item['name']) {
				return <td key={index}>
					<NavLink to={this.props.link}
							 className="text-dark"> {this.props.item['name']}</NavLink>
				</td>
			} else{
				return <td key={index}>
					<NavLink to={this.props.link}
							 className="text-dark"> {this.props.item['firstName']} {this.props.item['lastName']}</NavLink>
				</td>
			}
		}
		if (header.label === 'link') {
			return null;
		}
		if (!header.label) {
			return <React.Fragment>
				<td key={index} className="d-flex justify-content-end">
					{(this.props.editGroup) &&
					<FontAwesomeIcon onClick={this.changeGroupName} icon={faPenSquare}
									 className="align-self-center mr-2 fa-2x"/>}
					<Button onClick={this.assignTest}>Назначить тест</Button>
					<Button onClick={this.assignTask} className={'ml-2'}>Назначить задачу</Button>
					{(this.props.deleteStudent) &&
					<button type="button" className="close ml-2" onClick={this.confirm}>
						<span aria-hidden="true">&times;</span>
					</button>}
				</td>
			</React.Fragment>;
		}
		return <td key={index}>
			{this.props.item[header.key]}
		</td>
	};

	render() {
		return <tr>
			{this.props.headers.map(this.renderItemElement)}
		</tr>;
	}
}

GroupsRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array,
	assignTest: PropTypes.func,
	assignTask: PropTypes.func,
	editGroup: PropTypes.bool,
	confirm: PropTypes.func,
	changeGroupName: PropTypes.func,
	deleteStudent: PropTypes.bool
};
GroupsRow.defaultProps = {
	editGroup: false,
	deleteStudent: true
};

