import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Pagination from "../../components/ListPagination/Pagination";
import Input from "../../components/Input/Input";
import {onChangeChunk, onNext, onPrevious, getData} from "../../actions/Students/StudentsAction";
import Button from "../../components/Button/Button";
import List from "../../components/List/List";
import ListItem from "../../components/List/ListItem";
import {addNewGroup, getGroups, addStudents} from "../../actions/Groups/GroupsActions";
import {NavLink} from 'react-router-dom';

class NewGroup extends Component {
	constructor() {
		super();
		this.state = {
			name: '',
			checkedItems: [],
			filter: ''
		}
	}

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
	};

	onHandleClick = () => {
		this.props.getData(this.state.filter);
	};

	componentDidMount() {
		this.props.getData();
	}

	sendGroup = () => {
		if (this.props.addStudent) {
			let students = this.state.checkedItems.map((item) => {
				return JSON.stringify(item);
			});
			this.props.addStudents(students, this.props.id);
		} else {
			let students = this.state.checkedItems.map((item) => {
				return JSON.stringify(item);
			});
			this.props.sendRequest(students, this.state.name);
			setTimeout(this.props.getGroups(sessionStorage.getItem('id')), 2000);
			this.props.history.push('/app/groups');

		}
	};

	renderListItem = (item, index, checkedItems, click) => {
		return (
			<ListItem id={item.id}
					  selectable={true}
					  multiple={true}
					  checkedItems={checkedItems}
					  className="d-flex justify-content-between"
					  onClick={click}
			>
				{item.firstName} {item.lastName}
			</ListItem>
		);
	};
	onNext = () => {
		this.props.onNext(this.state.filter);
	};

	onPrevious = () => {
		this.props.onPrevious(this.state.filter);
	};

	onChangeChunk = (value) => {
		this.props.onChangeChunk(this.state.filter, value);
	};


	handleChecked = (items) => {
		this.setState({
			checkedItems: items,
		});
	};
	renderTitle = () => {
		if (this.props.title) {
			return <h2>{this.props.title}</h2>;
		} else {
			return <React.Fragment>
				<label>Введите название:</label>
				<Input
					name="name"
					className="col-7 ml-1"
					value={this.state.name}
					onChange={this.onChange}
					maxLength="50"
					required
				/>
			</React.Fragment>;
		}
	};
	checkValidation = (event) => {
		if (this.props.addStudent) {
			this.sendGroup();
		}
		else {
			const form = event.target.parentElement.parentElement;
			if (form.checkValidity()) {
				this.sendGroup();
			}
		}
	};
	noReload = (event) => {
		event.preventDefault();
	};

	renderButton = () => {
		if (!this.props.addStudent) {
			return <Button
				type="submit"
				value="Submit"
				onClick={this.checkValidation}
				className="col-3 ml-2 btn-primary"
			>
				Сохранить
			</Button>
		}
		return <NavLink to={`/app/groups`} className="col-3 ml-2">
			<Button
				type="submit"
				value="Submit"
				onClick={this.checkValidation}
				className="btn-primary"
			>
				Сохранить
			</Button>
		</NavLink>
	};

	render() {
		const {currentPage, chunkSize, currentData, totalPageNumber} = this.props;
		return (
			<form onSubmit={this.noReload}>
				{(!this.props.title) && <div className="mb-4">
					<h3>Создание новой группы</h3>
				</div>}
				<div className="d-flex justify-content-between mt-1">
					{this.renderTitle()}
					{this.renderButton()}
				</div>
				<div className="mt-4 d-flex justify-content-between">
					<Input name="filter" className="col-10" value={this.state.filter} onChange={this.onChange}
						   placeholder={'Поиск...'}/>
					<Button onClick={this.onHandleClick}>
						Применить
					</Button>
				</div>
				<List
					className="mt-4"
					data={currentData}
					renderListItem={this.renderListItem}
					handleChecked={this.handleChecked}
					checkedItems={this.state.checkedItems}
				/>
				<Pagination
					totalPageNumber={totalPageNumber}
					currentPage={currentPage}
					chunkSize={chunkSize}
					chunkSizes={[1, 2, 5]}
					onNext={this.onNext}
					onPrevious={this.onPrevious}
					onChangeChunk={this.onChangeChunk}/>
			</form>
		);
	}
}

const mapStateToProps = ({Students}) => ({
	chunkSize: Students.chunkSize,
	currentPage: Students.currentPage,
	totalPageNumber: Students.totalPageNumber,
	currentData: Students.currentData,
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
	sendRequest: bindActionCreators(addNewGroup, dispatch),
	getGroups: bindActionCreators(getGroups, dispatch),
	getData: bindActionCreators(getData, dispatch),
	addStudents: bindActionCreators(addStudents, dispatch),
});


export default connect(mapStateToProps, mapDispatchToProps)(NewGroup);
