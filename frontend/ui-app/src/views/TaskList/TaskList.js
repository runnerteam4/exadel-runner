import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Select from 'react-select';
import './css/index.css';

import {getCategories, getTasks, getTasksFilter} from '../../actions/TaskList/action';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import Table from '../../components/Table/Table';
import TaskListRow from './TaskListRow';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';

class TaskList extends React.Component {
	constructor() {
		super();

		this.state = {

			categories: [],
			input: '',

		};
	}

	componentDidMount() {
		this.props.getCategories();
		this.props.getTasks();
	}

	//
	// getFilter = () => {
	// 	const filter = this.state.filter;
	// 	const categories = filter.categories;
	// 	const testType = filter.testType;
	// 	const input = filter.input;
	// 	return {
	// 		categories: categories ? categories.map(obj => obj.value) : [],
	// 		testType: testType ? testType.value : '', // return undef
	// 		input: input ? input : ''
	// 	};
	// };

	onChangeInput = (event) => {
		const value = event.target.value;
		this.setState({
			input: value
		});
		this.props.getTasksFilter(value);
	};

	selectOptionCategory = (categories) => {
		this.setState({
			categories: categories,
		});
	};

	render() {
		const {
			tasks, categories, isPending, error
		} = this.props;

		const renderRow = (item, index, headers) => {
			return <TaskListRow item={item} key={index} headers={headers}/>;
		};
		const headers = [
			{key: 'taskName', label: 'Название'},
			{key: 'proveTests', label: 'Принятые тесты'},
			{key: 'deadline', label: 'Дедлайн'},
			{key: 'button', label: ''}
		];
		return (
			<React.Fragment>
				<h2>Назначенные задачи</h2>
				{/*<div className="d-flex">*/}
					{/*<Select*/}
						{/*isMulti={true}*/}
						{/*value={this.state.categories}*/}
						{/*placeholder="Выберите категорию"*/}
						{/*className="w-100"*/}
						{/*onChange={this.selectOptionCategory}*/}
						{/*options={categories}*/}
					{/*/>*/}
					{/*<Button*/}
						{/*className="ml-3"*/}
						{/*onMouseUp={this.getTestList}*/}
					{/*>*/}
						{/*Применить*/}
					{/*</Button>*/}
				{/*</div>*/}
				<Input
					className="mt-3"
					placeholder="Поиск по названию задачи..."
					onChange={this.onChangeInput}
					value={this.state.input}/>
				<Table
					className="mt-4"
					headers={headers}
					data={tasks}
					renderRow={renderRow}
				>
				</Table>
				{(!error && !isPending && tasks.length === 0) &&
				<div className="mt-5 mb-5">
					<h3 className="text-center">Ничего не найдено</h3>
				</div>
				}
				{isPending && (
					<div className="d-flex h-50">
						<Loading/>
					</div>
				)}
				{error && (<ErrorPage textError={error}/>)}
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({taskList}) => ({
	tasks: taskList.tasks,
	categories: taskList.categories,
});

const mapDispatchToProps = (dispatch) => ({
	getCategories: bindActionCreators(getCategories, dispatch),
	getTasks: bindActionCreators(getTasks, dispatch),
	getTasksFilter: bindActionCreators(getTasksFilter, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
