import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button';
import './css/index.css';
import {NavLink} from 'react-router-dom';

export default class TaskListRow extends Component {
	renderItemElement = (header, index) => {
		if (header.label === '') {
			return (
				<td key={index}>
					<NavLink to={`/app/tasks/${this.props.item.id}`} className='text-dark'>
						<Button className="float-right">
							Открыть
						</Button>
					</NavLink>
				</td>
			);
		}
		if (header.label === 'Принятые тесты'){
			return <td key={index}>
				{(this.props.item.success)&& (`${this.props.item['success']}/${this.props.item['total']}`)}
			</td>
		}
		return (
			<td key={index}>
				{this.props.item[header.key]}
			</td>
		);
	};

	render() {
		return (
			<tr>
				{this.props.headers.map(this.renderItemElement)}
			</tr>
		);
	}
}

TaskListRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};
