import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Pagination from "../../components/ListPagination/Pagination";
import Input from "../../components/Input/Input";
import {onChangeChunk, onNext, onPrevious, getData} from "../../actions/Students/StudentsAction";
import Table from "../../components/Table/Table";
import GroupsRow from "../GroupsView/GroupsRow";
import {assignTask, assignTest} from "../../actions/Modals/ModalAction";
import ModalRoot from "../../components/Modals/ModalRoot";

class Students extends Component {
	constructor() {
		super();
		this.state = {
			filter: ''
		}
	}

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
		this.props.getData(e.target.value);
	};

	componentDidMount() {
		this.props.getData();
	}

	onNext = () => {
		this.props.onNext(this.state.filter);
	};

	onPrevious = () => {
		this.props.onPrevious(this.state.filter);
	};

	onChangeChunk = (value) => {
		this.props.onChangeChunk(this.state.filter, value);
	};


	handleChecked = (items) => {
		this.setState({
			checkedItems: items,
		});
	};

	noReload = (event) => {
		event.preventDefault();
	};

	render() {
		const headers = [
			{key:'name', label: 'name'},
			{key:'', label: ''}
		];
		const getRow =(item,index,headers) => (
			<GroupsRow assignTask={this.props.assignTask}
					   assignTest={this.props.assignTest}
					   role = {'student'}
					   item={item} key={this.props.currentData[index].id} headers={headers}
					   deleteStudent={false} editGroup = {false}
					   link = {`/app/profile/${this.props.currentData[index].id}`}/>
		);
		const {currentPage, chunkSize, currentData, totalPageNumber} = this.props;
		return (
			<React.Fragment>
			<form onSubmit={this.noReload}>
				{(!this.props.title) && <div className="mb-4">
					<h3>Все студенты</h3>
				</div>}
				<div className="mt-4 d-flex justify-content-between">
					<Input name="filter" value={this.state.filter} onChange={this.onChange}
						   placeholder={'Поиск...'}/>
				</div>
				<div className="mt-3">
					<Table headers={headers} hideHeaders={true} data={currentData} renderRow={getRow}/>
				</div>
				<Pagination
					totalPageNumber={totalPageNumber}
					currentPage={currentPage}
					chunkSize={chunkSize}
					chunkSizes={[5, 10, 20]}
					onNext={this.onNext}
					onPrevious={this.onPrevious}
					onChangeChunk={this.onChangeChunk}/>
			</form>
				<ModalRoot/>
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({Students}) => ({
	chunkSize: Students.chunkSize,
	currentPage: Students.currentPage,
	totalPageNumber: Students.totalPageNumber,
	currentData: Students.currentData,
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
	getData: bindActionCreators(getData, dispatch),
	assignTest: bindActionCreators(assignTest, dispatch),
	assignTask: bindActionCreators(assignTask, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(Students);