﻿import React, { Component } from 'react';
import TextArea from "../../../components/TextArea/TextArea";
import 'bootstrap/dist/css/bootstrap.css';


class StudentFields extends Component {
	makeYears() {
		const start = 1990;
		const end = new Date().getFullYear() + 10;
		const options = [];
		for (let year = start; year <= end; ++year) {
			let option = React.createElement(
				'option',
				{ value: year, key: year },
				year,
			);
			options.push(option);
		}
		return options;
	}

	makeUniversities() {
		const { universities, makeOptions } = this.props;
		return makeOptions(universities);
	}

	makeFaculties() {
		const { universities, university: universityName, makeOptions } = this.props;
		const faculties = universities.find(university => university.value === universityName).faculties;
		return makeOptions(faculties);
	}

	render() {
		const primarySkills = ['Java', 'C++', 'C#', 'C', 'JavaScript', 'Python', 'Pascal'].map((item, index) => {
			return <option key={index}>{item}</option>;
		});
		return (
			<React.Fragment>
				<div className="form-row">
					<div className="form-group col-md-4">
						<label htmlFor="universitySelect">Учебное заведение</label>
						<select name="university" className="form-control" id="universitySelect" onChange={this.props.handleChange} required>
							<option value="" disabled selected hidden>Выберете учебное заведение</option>
							{ this.makeUniversities() }
						</select>
					</div>
					{
						this.props.university &&
						<div className="form-group col-md-4">
							<label htmlFor="facultySelect">Факультет</label>
							<select name="faculty" className="form-control" id="facultySelect" onChange={this.props.handleChange} required>
								<option value="" disabled selected hidden>Выберете факультет</option>
								{ this.makeFaculties() }
							</select>
						</div>
					}
					{
						this.props.university &&
						<div className="form-group col-md-4">
							<label htmlFor="yearSelect">Год окончания</label>
							<select name="year" className="form-control" id="yearSelect" onChange={this.props.handleChange} required>
								<option value="" disabled selected hidden>Год окончания учебного заведения</option>
								{ this.makeYears() }
							</select>
						</div>
					}
				</div>
				<div className="form-group">
					<label htmlFor="primarySkillsInput">Основные навыки</label>
					<select
						name="primarySkills"
						className="form-control"
						id="primarySkillsInput"
						value={this.props.primarySkills}
						onChange={this.props.handleChange}
						required
					>
						<option value="" disabled selected hidden>Выберите основные навыки</option>
						{primarySkills}
					</select>
				</div>
			</React.Fragment>
		);
	}
}

export default StudentFields;
