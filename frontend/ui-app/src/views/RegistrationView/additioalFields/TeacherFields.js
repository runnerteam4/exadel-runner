import React, { Component } from 'react';
import TextArea from "../../../components/TextArea/TextArea";
import Input from "../../../components/Input/Input";
import 'bootstrap/dist/css/bootstrap.css';


class TeacherFields extends Component {
	constructor() {
		super();

		this.state = {
			jobStatus: '',
		};

		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value,
		});
	}

	render() {
		const universities = this.props.makeOptions(this.props.universities);
		const jobPlace = {
			educationalInstitution:
				<div className="form-group">
					<label htmlFor="jobSelect">Место работы</label>
					<select name="jobPlace" className="form-control" id="jobSelect" onChange={this.props.handleChange} required>
						<option value="" disabled selected hidden>Место работы</option>
						{ universities }
					</select>
				</div>,
			notEducationalInstitution:
				<div className="form-group">
					<label htmlFor="jobInput">Место работы</label>
					<Input
						type = "text"
						id = "jobInput"
						className = "form-control"
						name = "jobPlace"
						value = {this.props.jobPlace}
						placeholder = "Место работы"
						onChange= {this.props.handleChange}
						minLength="2"
						maxLength="50"
						required
					/>
				</div>,
		};

		const primarySkills = ['Java', 'C++', 'C#', 'C', 'JavaScript', 'Python', 'Pascal'].map((item, index) => {
			return <option key={index}>{item}</option>;
		});

		return (
			<React.Fragment>
				<div className="form-group">
					<label htmlFor="jobPSelect">Тип заведения</label>
					<select name="jobStatus" className="form-control" id="jobSelect" onChange={this.handleChange} required>
						<option value="" disabled selected hidden>Тип заведения</option>
						<option value="educationalInstitution">Учебное заведение</option>
						<option value="notEducationalInstitution">Не учебное заведение</option>
					</select>
				</div>
				{ this.state.jobStatus && jobPlace[this.state.jobStatus] }
				<div className="form-group">
					<label htmlFor="primarySkillsInput">Основные навыки</label>
					<select
						name="primarySkills"
						className="form-control"
						id="primarySkillsInput"
						value={this.props.primarySkills}
						onChange={this.props.handleChange}
						required
					>
						<option value="" disabled selected hidden>Выберете статус</option>
						{primarySkills}
					</select>
				</div>
			</React.Fragment>
		);
	}
}

export default TeacherFields;
