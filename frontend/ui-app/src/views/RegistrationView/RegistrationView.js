﻿import React, { Component } from 'react';
import Input from "../../components/Input/Input";
import Button from "../../components/Button/Button";
import StudentFields from "./additioalFields/StudentFields";
import TeacherFields from "./additioalFields/TeacherFields";
import 'bootstrap/dist/css/bootstrap.css';
import {bindActionCreators} from "redux";
import {fetchUser} from "../../actions/Registration/action";
import {registrationSuccess, registrationFailed} from "../../actions/Modals/ModalAction";
import {update} from "../../actions/Registration/action";
import connect from "react-redux/es/connect/connect";
import Loading from "../../components/Loading/Loading";
import ModalRoot from "../../components/Modals/ModalRoot";

const universities = [
	{ value: 'bsu',
		label: 'БГУ',
		faculties: [
			{ value: 'fpmi', label: 'ФПМИ' },
			{ value: 'mmf', label: 'МЕХМАТ' },
		],
	},
	{
		value: 'bsuir',
		label: 'БГУИР',
		faculties: [
			{ value: 'ksis', label: 'КСИС' },
		],
	},
	{
		value: 'bntu',
		label: 'БНТУ',
		faculties: [
			{ value: 'fitr', label: 'ФИТР' },
		]
	},
];

class RegistrationView extends Component {
	constructor() {
		super();

		this.state = {
			name: '',
			surname: '',
			email: '',
			userStatus: '',
			university: '',
			faculty: '',
			year: '',
			jobPlace: '',
			primarySkills: '',
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value,
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		const user = {
			student: {
				"user": {
					"firstName": this.state.name,
					"lastName": this.state.surname,
					"email": this.state.email
				},
				"university": {
					"name": this.state.university
				},
				"faculty": {
					"name": this.state.faculty
				},
				"primarySkill": this.state.primarySkills,
				"graduationYear": this.state.year
			},
			teacher: {
				"user": {
					"firstName": this.state.name,
					"lastName": this.state.surname,
					"email": this.state.email
				},
				"university": {
					"name": this.state.jobPlace
				},
				"primarySkill": this.state.primarySkills
			}
		};
		this.props.fetchUser(user[this.state.userStatus], this.state.userStatus);
	}

	makeOptions(data) {
		return data.map(({value, label}, index) => {
			return <option value={value} key={index}>{label}</option>
		})
	}

	componentWillUnmount() {
		this.props.update();
	}

	render() {
		const additionalFields = {
			student:
				<StudentFields
					year = {this.state.year}
					primarySkills = {this.state.primarySkills}
					universities = {universities}
					university = {this.state.university}
					faculty = {this.state.faculty}
					makeOptions = {this.makeOptions}
					handleChange = {this.handleChange}
				/>,
			teacher:
				<TeacherFields
					jobPlace = {this.state.jobPlace}
					primarySkills = {this.state.primarySkills}
					makeOptions = {this.makeOptions}
					universities = {universities}
					handleChange = {this.handleChange}
				/>,
		};

		return (
			<div className="container">
				{ !this.props.isFetching &&
					<form className="border border-secondary rounded p-3 m-3" onSubmit={this.handleSubmit}>
						<div className="form-row">
							<div className="form-group col-md-6">
								<label htmlFor="nameInput">Имя</label>
								<Input
									type = "text"
									id = "nameInput"
									className = "form-control"
									name = "name"
									value = {this.state.name}
									placeholder = "Имя"
									onChange= {this.handleChange}
									pattern="^[а-яА-Яa-zA-Z0-9]*$"
									minLength="2"
									maxLength="20"
									required
								/>
							</div>
							<div className="form-group col-md-6">
								<label htmlFor="surnameInput">Фамилия</label>
								<Input
									type = "text"
									id = "surnameInput"
									className = "form-control"
									name = "surname"
									value = {this.state.surname}
									placeholder = "Фамилия"
									onChange= {this.handleChange}
									pattern="^[а-яА-Яa-zA-Z0-9]*$"
									minLength="2"
									maxLength="30"
									required
								/>
						</div>
					</div>
					<div className="form-group">
						<label htmlFor="emailInput">Почта</label>
						<Input
							type = "email"
							id = "emailInput"
							className = "form-control"
							name = "email"
							value = {this.state.email}
							placeholder = "Почта"
							onChange = {this.handleChange}
							required
						/>
					</div>
					<div className="form-group">
						<label htmlFor="userStatusSelect">Статус пользователя</label>
						<select name="userStatus" className="form-control" id="userStatusSelect" onChange={this.handleChange} required>
							<option value="" disabled selected hidden>Выберите статус</option>
							<option value="student">Студент</option>
							<option value="teacher">Учитель</option>
						</select>
					</div>
					{ this.state.userStatus && additionalFields[this.state.userStatus] }
					<div className="row">
						<div className="col">
							<Button
								type = "submit"
								className = "btn btn-primary float-right"
								value = "Submit"
							>
								Подтвердить
							</Button>
						</div>
					</div>
					{
						!this.props.error && this.props.response &&
						(
							<div>
								<ModalRoot/>
								{ (() => { this.props.registrationSuccess('Проверьте свою электронную почту') })() }
							</div>
						)
					}
					{
						this.props.error &&
						(
							<div>
								<ModalRoot/>
								{ (() => { this.props.registrationFailed('Ошибка: ' + this.props.error) })() }
							</div>
						)
					}
				</form>
				}
				{
					this.props.isFetching &&
					(
						<div className="d-flex h-50">
							<Loading/>
						</div>
					)
				}
			</div>
		);
	}
}

const mapStateToProps = ({registration}) => ({
	isFetching: registration.isFetching,
	error: registration.error,
	response: registration.response,
});

const mapDispatchToProps = (dispatch) => ({
	fetchUser: bindActionCreators(fetchUser, dispatch),
	registrationSuccess: bindActionCreators(registrationSuccess, dispatch),
	registrationFailed: bindActionCreators(registrationFailed, dispatch),
	update: bindActionCreators(update, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationView);
