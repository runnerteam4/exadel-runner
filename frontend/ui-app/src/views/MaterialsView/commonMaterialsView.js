import React, {Component} from 'react';
import {connect} from "react-redux";
import Button from "../../components/Button/Button";
import ModalRoot from '../../components/Modals/ModalRoot';
import Materials from '../../views/MaterialsView/Materials';
import {bindActionCreators} from 'redux';
import {addCategory} from "../../actions/Modals/ModalAction";
import {addCategoryModal, addMaterialModal} from "../../actions/Materials/MaterialsAction";

class commonMaterialsView extends Component {
	render() {
		const addCategory = () => {
			return <Button onClick={this.props.addCategory} className='btn-primary mr-2'>Добавить раздел</Button>
		};
		const role = sessionStorage.getItem('role');
		if ((role === 'ROLE_TEACHER' && (sessionStorage.getItem('isConfirm')!=='false')) || role === 'ROLE_ADMIN') {
			return <React.Fragment>
				<div className="d-flex justify-content-between mb-3">
					<h2>Материалы</h2>
					<div className="d-flex justify-content-end">
						{(role === 'ROLE_ADMIN') && addCategory()}
						<Button onClick={this.props.addMaterial} className='btn-primary'>
							Добавить материал
						</Button>
					</div>
				</div>
				<Materials/>
				<ModalRoot/>
			</React.Fragment>
		}
		else {
			return <Materials/>
		}
	}
}


const mapStateToProps = ({login}) => ({
	role: login.role
});

const mapDispatchToProps = (dispatch) => ({
	addCategory: bindActionCreators(addCategoryModal, dispatch),
	addMaterial: bindActionCreators(addMaterialModal, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(commonMaterialsView);
