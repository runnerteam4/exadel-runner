import React, {Component} from 'react';
import Table from '../../components/Table/Table';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import Select from 'react-select';
import Pagination from '../../components/ListPagination/Pagination';
import {getCategories, getMaterials, getMaterialsFilter} from '../../actions/Materials/MaterialsAction';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {onChangeChunk, onNext, onPrevious} from "../../actions/Materials/MaterialsAction";
import MaterialsRow from './MaterialsRow'
import Loading from '../../components/Loading/Loading';


class Materials extends Component {
	 constructor(){
	 	super();
	 	this.state ={
			selectedOption: [],
			filter: '',
			isPending: true,
		};
	 }

	async componentDidMount() {
	 	await this.props.getMaterials();
	 	await this.props.getCategories();
	 	this.setState({
			isPending: false,
		});
	}

	onChangeSelect = (e) => {
		this.setState(
			{selectedOption: e}
		);
	};

	onChange = (e) => {
		this.setState(
			{filter: e.target.value}
		);
		this.props.getMaterialsFilter('filter', e.target.value);
	};

	onNext = async () => {
		this.setState({
			isPending: true,
		});
		await this.props.onNext(this.state.filter);
		this.setState({
			isPending: false,
		});
	};

	onPrevious = async () => {
		this.setState({
			isPending: true,
		});
		await this.props.onPrevious(this.state.filter);
		this.setState({
			isPending: false,
		});
	};

	onChangeChunk = async (value) => {
		this.setState({
			isPending: true,
		});
		await this.props.onChangeChunk(this.state.filter, value);
		this.setState({
			isPending: false,
		});
	};

	getData =  () => {
		this.setState({
			isPending: true,
		});
		if(!this.state.selectedOption.length){
			this.props.getMaterials();
		}
		this.props.getMaterialsFilter('select',this.state.selectedOption.map((item) => {return item.value;}));
		this.setState({
			isPending: false,
		});
	};

	render() {
		const getRow = (item, index, headers) => (
			<MaterialsRow item={item} key={index} headers={headers}/>
		);
		const {
			totalPageNumber, currentPage, chunkSize,
			currentData, onClick, category} = this.props;
		const headers = [
			{key: 'name', label: 'Название'},
			{key: 'link', label: 'Ссылка'},
			{key: 'author', label: 'Автор'},

		];
		const isPending = this.state.isPending;
		return <React.Fragment>
			<div className={'d-flex'}>
				<Select
					className={'w-100'}
					placeholder={'Выберите категорию...'}
					value={this.state.selectedOption}
					onChange={this.onChangeSelect}
					isMulti={true}
					options={category}
				/>
				<Button className={'ml-3'} onClick={this.getData}>Подтвердить</Button>
			</div>
			<Input className='mt-4' value={this.state.filter}
					   onChange={this.onChange} placeholder="Поиск..."/>
			{!isPending &&
			<div className={'mt-4'}>
				<Table headers={headers} hideHeaders={true} data={currentData} renderRow={getRow}/>
				<Pagination totalPageNumber={totalPageNumber}
							currentPage={currentPage}
							chunkSize={chunkSize}
							chunkSizes={[2, 5, 10]}
							onNext={this.onNext}
							onPrevious={this.onPrevious}
							onChangeChunk={this.onChangeChunk}
				/>
			</div>
			}
			{isPending && (
				<div className="d-flex h-50 mt-5 mb-5">
					<Loading/>
				</div>
			)}
		</React.Fragment>;
	}
}


const mapStateToProps = ({materials}) => ({
	selectedOption: materials.selectedOption,
	headers: materials.headers,
	input: materials.filter,
	category: materials.category,
	currentData: materials.currentData,
	chunkSize: materials.chunkSize,
	currentPage: materials.currentPage,
	totalPageNumber: materials.totalPageNumber
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
	getData: bindActionCreators(getMaterials,dispatch),
	getMaterials: bindActionCreators(getMaterials, dispatch),
	getCategories: bindActionCreators(getCategories, dispatch),
	getMaterialsFilter: bindActionCreators(getMaterialsFilter, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Materials);
