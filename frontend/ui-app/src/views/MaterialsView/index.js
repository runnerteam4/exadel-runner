import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import * as ReactDOM from 'react-dom';
import Materials from './Materials';

import store from '../../reducers/Materials/MaterialsReducer';


const appStore = createStore(store);
function App() {
	return (
		<Provider store={appStore}>
			<div className="App">
				<Materials/>
			</div>
		</Provider>
	);
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App/>, rootElement);