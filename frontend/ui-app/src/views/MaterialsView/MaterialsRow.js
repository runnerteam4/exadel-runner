import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class MaterialsRow extends Component{
	renderItemElement = (header,index) => {
		if(header.label === 'Название'){
			return <td key = {index}>
				<a href = {this.props.item['link']} target="_blank" className = 'text-dark h6'>{this.props.item[header.key]}</a>
			</td>
		}
		if(header.label === 'Ссылка'){
			return null;
		}
		return <td key={index}>
			<div className={'d-flex justify-content-end'}>
				{this.props.item['authorName']}
			</div>
		</td>
	};
	render(){
		return <tr>
		{this.props.headers.map(this.renderItemElement)}
		</tr>;
	}
}
MaterialsRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};



