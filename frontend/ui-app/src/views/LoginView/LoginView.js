import React, { Component } from 'react';
import Button from '../../components/Button/Button';
import Input from '../../components/Input/Input';
import {LogIn, getUserData} from "../../actions/LogIn/action";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import classNames from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSpinner} from '@fortawesome/free-solid-svg-icons/faSpinner';

class LoginView extends Component {
	constructor() {
		super();
		this.state = {
			login: '',
			password: '',
			isValidForm: true,
			isPending: false,
		}
	}

	componentWillUnmount() {
		this.isCancelled = true;
	}

	inputLogin = ({target}, id) => {
		this.setState({[id]: target.value});
	};

	changeValidForm = (data) => {
		this.setState({
			...this.state,
			isValidForm: data,
		});
	};

	signIn = async (event) => {
		this.setState({
			...this.state,
			isPending: true,
		});
		let isLogin = false;
		const form = event.target.parentElement;
		if (form.checkValidity()) {
			const res = await this.props.LogIn(this.state.login, this.state.password);
			isLogin = res.isLogin;
			if (isLogin) {
				sessionStorage.setItem('authorized', 'true');
				const {id, firstName, lastName, role, status} = await this.props.getUserData();
				const data = {id, firstName, lastName, role, isConfirm: status};
				Object.entries(data).forEach((item) => sessionStorage.setItem(item[0], item[1]));
				this.changeValidForm(true);
			}
			else {
				this.changeValidForm(false);
			}
		}
		else {
			this.changeValidForm(false);
		}
		this.setState({
			...this.state,
			isPending: false,
		});
		if (isLogin) {
			this.props.history.push('/app');
		}
	};

	noReload = (event) => {
		event.preventDefault();
	};

	render() {
		const contentButton = this.state.isPending ? <FontAwesomeIcon icon={faSpinner} spin/> : 'Войти';
		return (
			<React.Fragment>
				<div id="signIn" className="d-flex justify-content-center align-items-center h-75 mt-5 mb-5">
					<form onSubmit={this.noReload} className="px-4 py-4 border border-secondary rounded p-5">
						<div className={'d-flex justify-content-center'}>
							<div className="text-dark " style={{fontFamily: 'Mistral' , fontSize: '2rem' }}>- JDline -</div>
						</div>
						<Input
							type="email"
							name="email"
							placeholder="Почта"
							value={this.state.login}
							maxLength={30}
							minLength={6}
							required
							className={classNames("mt-2", this.state.isValidForm ? '' : ' alert-danger')}
							onChange={(e) => this.inputLogin(e, 'login')}
						/>
						<Input
							placeholder="Пароль"
							value={this.state.password}
							type="password"
							name="password"
							maxLength={30}
							minLength={6}
							required
							className={classNames("mt-2", this.state.isValidForm ? '' : ' alert-danger')}
							onChange={(e) => this.inputLogin(e, 'password')}
						/>
						{!this.state.isValidForm &&
						<p
							className="position-relative text-center small text-danger mb-0">
							Неверный логин или пароль
						</p>
						}
						<Button
							value="enter"
							onClick={this.signIn}
							className={"btn btn-outline-dark mt-3 col-12"}>
							{contentButton}
						</Button>
					</form>
				</div>
			</React.Fragment>
		)
	}
}

const mapStateToProps = ({login}) => ({
	id: login.id,
});

const mapDispatchToProps = (dispatch) => ({
	LogIn: bindActionCreators(LogIn, dispatch),
	getUserData: bindActionCreators(getUserData, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
