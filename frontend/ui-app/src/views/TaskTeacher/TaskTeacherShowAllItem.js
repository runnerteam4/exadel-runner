import React, {Component} from 'react';
import classNames from 'classnames';
import 'bootstrap/dist/css/bootstrap.css';
import {connect} from "react-redux";
import {NavLink} from 'react-router-dom';

class TaskTeacherShowAllItem extends Component {
	render() {
		const classes = classNames(
			'list-group-item',
			this.props.className,
			'd-flex justify-content-between'
		);
		return <div className={classes}>
			<NavLink to={`task/${this.props.id}`} key = {this.props.id} className="nav-link text-dark">
				<li>
					{this.props.children}
				</li>
			</NavLink>
		</div>;
	}
}

TaskTeacherShowAllItem.propTypes = {};

TaskTeacherShowAllItem.defaultProps = {};


const mapStateToProps = ({login}) => ({
	userID: login.userId
});


export default connect(mapStateToProps)(TaskTeacherShowAllItem);
