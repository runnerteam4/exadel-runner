import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Pagination from "../../components/ListPagination/Pagination";
import {addTask, getAllTasks} from '../../actions/TaskTeacher/action';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import List from "../../components/List/List";
import TaskTeacherShowAllItem from './TaskTeacherShowAllItem';
import {onChangeChunk, onNext, onPrevious} from "../../actions/TaskTeacher/action";
import ModalRoot from "../../components/Modals/ModalRoot";


class TaskTeacherShowAll extends React.Component {
	constructor() {
		super();

		this.state = {
			filter: ''
		};
	}

	componentDidMount() {
		this.props.getAllTasks(this.state.filter);
	}

	onChangeInput = (event) => {
		const value = event.target.value;
		this.setState({
			filter: value
		});
		this.props.getAllTasks(event.target.value);
	};
	onNext = () => {
		this.props.onNext(this.state.filter);
	};

	onPrevious = () => {
		this.props.onPrevious(this.state.filter);
	};

	onChangeChunk = (value) => {
		this.props.onChangeChunk(this.state.filter, value);
	};

	renderRow = (item) => {
		return <TaskTeacherShowAllItem id={item.id} key={item.id}>{item.name}</TaskTeacherShowAllItem>;
	};

	render() {
		return (
			<React.Fragment>
				<Button
					className="w-100 col-3 btn-primary mb-2" onClick={this.props.addTask}>
					Добавить задачу
				</Button>
				<div className="d-flex">
					<Input
						className="mt-2"
						placeholder="поиск по названию"
						onChange={this.onChangeInput}
						value={this.state.filter}/>
				</div>
				<List
					className="mt-4"
					data={this.props.allTasks}
					renderListItem={this.renderRow}/>
				<Pagination
					totalPageNumber={this.props.totalPageNumber}
					currentPage={this.props.currentPage}
					chunkSize={this.props.chunkSize}
					chunkSizes={[5, 10, 20]}
					onNext={this.onNext}
					onPrevious={this.onPrevious}
					onChangeChunk={this.onChangeChunk}/>
				<ModalRoot/>
			</React.Fragment>
		);
	}
}


const mapStateToProps = ({taskTeacher, login}) => ({
	allTasks: taskTeacher.allTasks,
	chunkSize: taskTeacher.chunkSize,
	currentPage: taskTeacher.currentPage,
	totalPageNumber: taskTeacher.totalPageNumber,
	currentData: taskTeacher.currentData,
	userID: login.userId
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
	getAllTasks: bindActionCreators(getAllTasks, dispatch),
	addTask: bindActionCreators(addTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskTeacherShowAll);
