import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button';
import {NavLink} from 'react-router-dom';

export default class TaskListRow extends Component {
	renderItemElement = (header, index) => {
		if (header.label === '') {
			return (
				<td key={index}>
					<Button className="float-right">
						Просмотреть решение
					</Button>
				</td>
			);
		}
		if(header.key === 'mark'){
			return <td key={index}>
				{this.props.item['mark']? this.props.item['mark']: 'нет решений'}
			</td>
		}
		return (
			<td key={index}>
				{this.props.item[header.key]}
			</td>
		);
	};

	render() {
		return (
			<tr>
				{this.props.headers.map(this.renderItemElement)}
			</tr>
		);
	}
}

TaskListRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};
