import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {addTaskfile, getTask} from '../../actions/TaskTeacher/action';
import Button from '../../components/Button/Button';
import {confirmDeleteTask} from "../../actions/Modals/ModalAction";
import ModalRoot from "../../components/Modals/ModalRoot";
import ErrorPage from "../../components/ErrorPage/ErrorPage";
import Loading from '../../components/Loading/Loading';

class ShowTask extends React.Component {
	componentDidMount() {
		this.props.getTask(this.props.match.params.id);
	}

	addFile = (event) => {
		event.preventDefault();
		this.props.addTaskfile(this.props.match.params.id);
	};

	deleteTask = (event) => {
		event.preventDefault();
		this.props.confirmDeleteTask(this.props.task.id);
	};

	render() {
		const {task} = this.props;
		return (
			<React.Fragment>
				<div className="d-flex justify-content-center align-items-center h-75 mt-2 mb-5">
					{(task.id) && <form>
						<h3>{task.name}</h3>
						<label className="mt-2">Условие: </label>
						<label className="ml-2">{task.condition}</label>
						<div className="mt-3">
						{/*<Button className = "btn-primary mr-2"*/}
								{/*onClick={this.addFile}>*/}
							{/*Добавить тестировочный файл*/}
						{/*</Button>*/}
						<Button onClick={this.deleteTask}>Удалить задачу</Button>
						</div>
					</form>}
					{(!this.props.isFetching) && (!task.id) && <ErrorPage textError={'Задача удалена'}/>}
					<ModalRoot/>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({taskTeacher}) => ({
	task: taskTeacher.task,
	isFetching: taskTeacher.isFetching
});

const mapDispatchToProps = (dispatch) => ({
	getTask: bindActionCreators(getTask, dispatch),
	addTaskfile: bindActionCreators(addTaskfile,dispatch),
	confirmDeleteTask: bindActionCreators(confirmDeleteTask, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ShowTask);
