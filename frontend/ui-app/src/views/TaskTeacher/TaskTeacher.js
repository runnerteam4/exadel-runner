import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Select from 'react-select';
import {NavLink} from 'react-router-dom';
import {getGroups, getTasks} from '../../actions/TaskTeacher/action';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import Table from '../../components/Table/Table';
import TaskListRow from './TaskTeacherRow';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';

class TaskTeacher extends React.Component {
	constructor() {
		super();
		this.state = {
				input: '',
		};
	}

	componentDidMount() {
		this.props.getGroups();
		this.props.getTasks(this.state.input);
	}

	onChangeInput = (event) => {
		const value = event.target.value;
		this.setState(() => {
			return {
				input: value
			}
		});
	};


	render() {
		const {
			tasks, isPending, error
		} = this.props;

		const renderRow = (item, index, headers) => {
			return <TaskListRow link={'/app/tasks/task'} item={item} key={index} headers={headers}/>;
		};
		const headers = [
			{key: 'taskName', label: 'Название'},
			{key: 'studentName', label: 'Назначена'},
			{key: 'mark', label: 'Оценка'},
			{key: 'deadline', label: 'Дедлайн'},
		];

		return (
			<React.Fragment>
				<div className="d-flex justify-content-between">
					<h3>Назначенные студентам задачи</h3>
					<NavLink to="/app/tasks/all"
							 className="text-dark">
						<Button
							className="btn-primary mt-1 mb-3"
						>
							Просмотреть все задачи
						</Button>
					</NavLink>
				</div>
				<Input
					className="mt-3"
					placeholder="Поиск по названию"
					onChange={this.onChangeInput}
					value={this.state.input}/>
				<Table
					className="mt-4"
					headers={headers}
					data={tasks}
					renderRow={renderRow}
				>
				</Table>
				{(!error && !isPending && tasks.length === 0) &&
				<div className="mt-5 mb-5">
					<h3 className="text-center">Ничего не найдено</h3>
				</div>
				}
				{isPending && (
					<div className="d-flex h-50">
						<Loading/>
					</div>
				)}
				{error && (<ErrorPage textError={error}/>)}
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({taskTeacher}) => ({
	tasks: taskTeacher.tasks
});

const mapDispatchToProps = (dispatch) => ({
	getGroups: bindActionCreators(getGroups, dispatch),
	getTasks: bindActionCreators(getTasks, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskTeacher);
