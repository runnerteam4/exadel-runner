import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button';
import {NavLink} from 'react-router-dom';


export default class TestsTeacherRow extends Component {
	renderItemElement = (header, index) => {
		switch (header.label) {
		case '':
			return (
				<td key={index}>
					<NavLink to= {`/app/tests/check/${this.props.item.id}`}>
						<Button onClick={this.props.checkAnswers}>
							Проверить
						</Button>
					</NavLink>
				</td>
			);
		default:
			return (
				<td key={index}>
					{this.props.item[header.label]}
				</td>
			);
		}
	};
	render() {
		return (
			<tr>
				{this.props.headers.map(this.renderItemElement)}
			</tr>
		);
	}
}
TestsTeacherRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};
