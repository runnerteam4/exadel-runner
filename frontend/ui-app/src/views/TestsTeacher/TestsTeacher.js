import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getTestsCheckTeacher} from '../../actions/TestsTeacher/action';
import Table from '../../components/Table/Table';
import TestsTeacherRow from './TestsTeacherRow';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';
import Input from '../../components/Input/Input';
import Button from "../../components/Button/Button";
import ModalRoot from '../../components/Modals/ModalRoot';
import {addTest} from "../../actions/Modals/ModalAction";


class TestsTeacher extends React.Component {
	constructor() {
		super();
		this.state = {
			studentName: '',
			groupName: '',
			isPending: true,
			error: '',
		};
	}

	async componentDidMount() {
		const res = await this.props.getTestsCheckTeacher();
		const error = res.error;
		this.setState({
			studentName: '',
			groupName: '',
			isPending: false,
			error,
		});
	}

	onChangeInputStudent = (event) => {
		const value = event.target.value;
		this.setState((prevState) => {
			return {
				...prevState,
				studentName: value,
			}
		});
	};

	onChangeInputGroup = (event) => {
		const value = event.target.value;
		this.setState((prevState) => {
			return {
				...prevState,
				groupName: value,
			}
		});
	};

	getTestsCheckTeacher = () => {
		this.props.getTestsCheckTeacher(this.state.studentName, this.state.groupName);
	};

	render() {
		const renderRow = (item, index, headers) => {
			return <TestsTeacherRow item={item} key={index} headers={headers} checkAnswers={this.props.redirect}/>;
		};
		const headers = [
			{key: 'studentName', label: 'Имя ученика'},
			{key: 'name', label: 'Название'},
			{key: 'type', label: 'Тип теста'},
			{key: 'date', label: 'Дата прохождения'},
			{key: 'button', label: ''}
		];
		const {tests} = this.props;
		return (
			<React.Fragment>
				<div className="d-flex justify-content-between">
					<h3>Тесты для проверки</h3>
				<Button
					className="mb-3 col-4 btn-primary"
					onClick={this.props.addTest}>
					Добавить вопрос
				</Button>
				</div>
				<div className="d-flex">
					<Input
						className="mr-1"
						placeholder="Поиск по группе"
						onChange={this.onChangeInputGroup}
						value={this.state.groupName}/>
					<Input
						className="ml-1"
						placeholder="Поиск по имени студента"
						onChange={this.onChangeInputStudent}
						value={this.state.studentName}/>
					<Button
						onMouseUp={this.getTestsCheckTeacher}
						className="ml-3">
						Поиск
					</Button>
				</div>
				{!this.state.error &&
				<Table
					className="mt-4"
					headers={headers}
					data={tests}
					renderRow={renderRow}/>
				}
				{(!this.state.error && !this.state.isPending && tests.length === 0) &&
				<div className="mt-5 mb-5">
					<h3 className="text-center">Ничего не найдено</h3>
				</div>
				}
				{
					this.state.isPending &&
					<div className="d-flex h-50">
						<Loading/>
					</div>
				}
				{
					this.state.error && (<ErrorPage textError={this.state.error.message}/>)
				}
				<ModalRoot/>
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({testsTeacher}) => ({
	tests: testsTeacher.tests,
});
const mapDispatchToProps = (dispatch) => ({
	getTestsCheckTeacher: bindActionCreators(getTestsCheckTeacher, dispatch),
	addTest: bindActionCreators(addTest, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(TestsTeacher);
