import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '../../style.css';
import Dot from '../../components/Dot/Dot';
import TextArea from '../../components/TextArea/TextArea';
import FileUploader from '../../components/FileUploader/FileUploader';
import {fetchTask, fetchTaskAnswer} from '../../actions/TaskView/action';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ErrorPage from '../../components/ErrorPage/ErrorPage';

class TaskView extends Component {
	constructor() {
		super();

		this.state = {
			upFiles: [],
		};

		this.handleFiles = this.handleFiles.bind(this);
		this.pushFiles = this.pushFiles.bind(this);
	}

	pushFiles() {
		const taskId = Number(this.props.match.params.id);
		const userId = this.props.id || sessionStorage.getItem('id');
		this.props.fetchTaskAnswer(taskId, userId, this.state.upFiles[0]);
		this.props.history.push('/app');
	}

	handleFiles(items) {
		this.setState({
			upFiles: items,
		});
	}

	componentDidMount() {
		const taskId = Number(this.props.match.params.id);
		const userId = this.props.id || sessionStorage.getItem('id');
		this.props.fetchTask(taskId, userId);
	}

	render() {
		return (
			<React.Fragment>
				{!this.props.isFetching && !this.props.error &&
					<div className="container noSelect">
						<div className="row">
							<div className="col text-center">
								<h4>{this.props.data.taskName}</h4>
								<Dot value={this.props.data.mark === null? 0 : this.props.data.mark}/>
							</div>
						</div>
						<div className="row mt-3 mb-3">
							<div className="col">
								<TextArea
									type="text"
									rows={10}
									placeholder={this.props.data.condition}
									readOnly
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-12 col-md-6 text-center">
								<FileUploader
									fileTypes='.java'
									upFiles={this.state.upFiles}
									pushFiles={this.pushFiles}
									handleFiles={this.handleFiles}
									displayButton={true}
								/>
							</div>
							<div className="col-sm-12 col-md-6 text-center">
								Пройдено: {this.props.data.success === null? 0 : this.props.data.success} /
								Всего: {this.props.data.total === null? 0 : this.props.data.total}
							</div>
						</div>
					</div>
				}
				{
					this.props.error &&
					<ErrorPage textError={this.props.error}/>
				}
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({taskView, login}) => ({
	data: taskView.data,
	isFetching: taskView.isFetching,
	error: taskView.error,
	id: login.id,
});

const mapDispatchToProps = (dispatch) => ({
	fetchTask: bindActionCreators(fetchTask, dispatch),
	fetchTaskAnswer: bindActionCreators(fetchTaskAnswer, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskView);
