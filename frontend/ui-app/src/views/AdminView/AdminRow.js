import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Button from "../../components/Button/Button";


export default class AdminRow extends Component {
	approveRequest = () => {
		this.props.approveRequest(this.props.item.id);
	};

	rejectRequest = () => {
		this.props.rejectRequest(this.props.item.id);
	};

	renderItemElement = (header, index) => {

		const role = (role) => {
			if(role === 'ROLE_TEACHER'){
				return 'преподаватель'
			}
			if(role === 'ROLE_ADMIN'){
				return 'администратор'
			}
			return 'студент';
		};
		if (header.key === 'name') {
			return <td key={index}>
				<li className="text-dark"> {this.props.item['firstName']} {this.props.item['lastName']}</li>
			</td>
		}
		if (header.key === 'role') {
			return <td key={index} className="align-self-center">
				{role(this.props.item[header.key])} {(!this.props.item['status']) && '?'}
			</td>
		}
		if (header.key === 'status' && (!this.props.item['status'])){
			return <td key={index}>
				<div className={'d-flex justify-content-end'}>
					<Button onClick={this.rejectRequest} className={'ml-2'}>Отклонить</Button>
					<Button onClick={this.approveRequest} className={'ml-2'}>Подтвердить</Button>
				</div>
			</td>
		}
		return <td key={index}>
			{this.props.item[header.key]}
		</td>
	};

	render() {
		return <tr>
			{this.props.headers.map(this.renderItemElement)}
		</tr>;
	}
}

AdminRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array,
	approveRequest: PropTypes.func,
	rejectRequest: PropTypes.func
};
AdminRow.defaultProps = {
	approveRequest: () => {},
	rejectRequest: ()=>{}
};

