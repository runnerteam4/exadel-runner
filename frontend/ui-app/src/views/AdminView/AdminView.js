import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Pagination from "../../components/ListPagination/Pagination";
import Input from "../../components/Input/Input";
import {onChangeChunk, onNext, onPrevious, getData,approveRequest,
	rejectRequest} from "../../actions/Users/action";
import Button from "../../components/Button/Button";
import Select from 'react-select';
import {NavLink} from 'react-router-dom';
import AdminRow from "./AdminRow";
import Table from "../../components/Table/Table";

class AdminView extends Component {
	constructor() {
		super();
		this.state = {
			filter: '',
			userType: []
		}
	}

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
		this.props.getData(e.target.value);
	};

	onChangeSelect = (e) => {
		this.setState(
			{userType: e}
		);
	};


	componentDidMount() {
		this.props.getData(this.state.filter);
	}

	onNext = () => {
		this.props.onNext(this.state.filter||this.state.userType.label);
	};

	onPrevious = () => {
		this.props.onPrevious(this.state.filter||this.state.userType.label);
	};

	onChangeChunk = (value) => {
		this.props.onChangeChunk(this.state.filter||this.state.userType.label, value);
	};

	getData = () => {
		this.props.getData(this.state.userType.label||this.state.filter);
	};

	render() {
		const {currentPage, chunkSize, currentData, totalPageNumber, approveRequest} = this.props;
		const headers = [
			{key: 'name', label: 'Пользователь'},
			{key: 'email', label: 'Почта'},
			{key: 'status', label: ' '},
			{key: 'role', label: 'Статус'},
		];
		const getRow = (item, index, headers) => (
			<AdminRow item={item} key={index} headers={headers}
					  rejectRequest={this.props.rejectRequest}
					  approveRequest = {approveRequest} link={`/app/profile/${item.id}`}/>
		);
		return <React.Fragment>
			<div className="d-flex justify-content-between">
				<h2>Все пользователи</h2>
				<NavLink to='/app/users/request' className='d-flex justify-content-end'>
					<Button className="btn-primary">
						Просмотреть запросы на преподавателя
					</Button>
				</NavLink>
			</div>
			<div className="d-flex mt-3">
				<Select
					className="w-100"
					value={this.state.userType}
					placeholder="Выберите пользователя"
					onChange={this.onChangeSelect}
					options={[
						{value: 'Студенты', label: 'студент'},
						{value: 'Преподаватели', label: 'преподаватель'},
					]}
				/>
				<Button className={'ml-3'} onClick={this.getData}>Применить</Button>
			</div>
			<Input
				className="mt-3"
				name="filter"
				placeholder="Поиск"
				onChange={this.onChange}
				value={this.state.filter}
			/>
			<Table headers={headers} data={currentData} className="mt-3" renderRow={getRow}/>
			<Pagination
				totalPageNumber={totalPageNumber}
				currentPage={currentPage}
				chunkSize={chunkSize}
				chunkSizes={[5, 10, 20]}
				onNext={this.onNext}
				onPrevious={this.onPrevious}
				onChangeChunk={this.onChangeChunk}/>
		</React.Fragment>;
	}
}

const mapStateToProps = ({allUsers}) => ({
	chunkSize: allUsers.chunkSize,
	currentPage: allUsers.currentPage,
	totalPageNumber: allUsers.totalPageNumber,
	currentData: allUsers.currentData,
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
	getData: bindActionCreators(getData, dispatch),
	rejectRequest: bindActionCreators(rejectRequest, dispatch),
	approveRequest: bindActionCreators(approveRequest, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(AdminView);
