import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Input from "../../components/Input/Input";
import {getData, approveRequest,
	rejectRequest} from "../../actions/Users/teacherRequestaction";
import AdminRow from "./AdminRow";
import Table from "../../components/Table/Table";

class TeacherRequest extends Component {
	constructor() {
		super();
		this.state = {
			filter: '',
		}
	}

	onChange = (e) => {
		this.setState(
			{[e.target.name]: e.target.value}
		);
		this.getData(e.target.value);
	};


	componentDidMount() {
		this.props.getData();
	}


	getData = (filter) => {
		this.props.getData(filter);
	};

	handleChecked = (items) => {
		this.setState({
			checkedItems: items,
		});
	};

	render() {
		const {currentData, approveRequest} = this.props;
		const headers = [
			{key: 'name', label: 'Пользователь'},
			{key: 'email', label: 'Почта'},
			{key: 'status', label: ' '},
			{key: 'role', label: 'Статус'},
		];
		const getRow = (item, index,headers) => (
			<AdminRow item={item} key={index} headers={headers} approveRequest = {approveRequest}
					  rejectRequest = {this.props.rejectRequest} link = "/app/profile"/>
		);
		return <React.Fragment>
			<h3>Запросы на преподавателя</h3>
			<Input
				className="mt-3"
				name = "filter"
				placeholder="Поиск по пользователю..."
				onChange={this.onChange}
				value={this.state.filter}/>
			<Table headers={headers} data={currentData} className="mt-3" renderRow={getRow}/>
			{(this.props.currentData) && (!this.props.currentData.length) &&
			<label className="d-flex justify-content-center">Запросов нет</label>}
		</React.Fragment>;
	}
}

const mapStateToProps = ({teacherRequest}) => ({
	chunkSize: teacherRequest.chunkSize,
	currentPage: teacherRequest.currentPage,
	totalPageNumber: teacherRequest.totalPageNumber,
	currentData: teacherRequest.currentData,
});

const mapDispatchToProps = (dispatch) => ({
	getData: bindActionCreators(getData, dispatch),
	rejectRequest: bindActionCreators(rejectRequest, dispatch),
	approveRequest: bindActionCreators(approveRequest, dispatch)
});


export default connect(mapStateToProps, mapDispatchToProps)(TeacherRequest);
