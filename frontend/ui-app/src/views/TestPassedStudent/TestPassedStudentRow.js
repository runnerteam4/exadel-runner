import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from '../../components/Button/Button';
import './css/index.css';

export default class TestPassedStudentRow extends Component {
	pickColor = (mark) => {
		return `#${(mark < 8 ? 15 : 30 - 2 * mark).toString(16)}${(mark >= 7 ? 15 : 2 * mark).toString(16)}0`;
	};

	showAnswers = (e) => {
		this.props.showAnswers(e.target.dataset.testId);
	};

	renderItemElement = (header, index) => {
		switch (header.label) {
		case '':
			return (
				<td key={index}>
					<Button data-test-id={this.props.item.id} onClick={this.showAnswers}>
						Просмотреть
					</Button>
				</td>
			);
		case 'Комментарий':
			return (
				<td key={index} className="comment-box">
					{this.props.item[header.key] || '-'}
				</td>
			);
		case 'Дата прохождения':
			return (
				<td key={index} className="comment-box">
					{this.props.item[header.key]}
				</td>
			);
		case 'Тип теста':
			return (
				<td key={index} className="comment-box">
					{this.props.item[header.key] ? 'Тренировочный' : 'Контрольный'}
				</td>
			);
		case 'Результат':
			let color = '#0ff';
			let mark = `${this.props.item.result}/10`;
			if (this.props.item.result) {
				mark = Math.floor(mark.split('/')[0]);
				color = this.pickColor(mark);
			}
			else {
				mark = 'не проверено'
			}
			return (
				<td className="d-flex align-items-center" key={index}>
					<span className="p-1 rounded" style={{backgroundColor: color}}>
						{mark || '0'}
					</span>
				</td>
			);
		default:
			return (
				<td key={index}>
					{this.props.item[header.key]}
				</td>
			);
		}
	};

	render() {
		return (
			<tr>
				{this.props.headers.map(this.renderItemElement)}
			</tr>
		);
	}
}

TestPassedStudentRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};
