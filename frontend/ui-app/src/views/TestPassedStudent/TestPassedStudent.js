import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getTestsPassedStudent} from '../../actions/TestPassedStudent/action';
import Table from '../../components/Table/Table';
import TestPassedStudentRow from './TestPassedStudentRow';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';
import './css/index.css';
import ModalRoot from '../../components/Modals/ModalRoot';
import {showAnswersModal} from "../../actions/Modals/ModalAction";

class TestsPassedStudent extends React.Component {
	constructor() {
		super();

		this.state = {
			isPending: true,
			error: '',
		};
	}

	async componentDidMount() {
		const res = await this.props.getTestsPassedStudent();
		const error = res.error;

		this.setState({
			isPending: false,
			error,
		});
	}

	showAnswers = (testId) => {
		this.props.redirect(testId);
	};

	render() {
		const renderRow = (item, index, headers) => {
			return <TestPassedStudentRow item={item} key={index} headers={headers} showAnswers={this.showAnswers}/>;
		};
		const headers = [
			{key: 'name', label: 'Название'},
			{key: 'training', label: 'Тип теста'},
			{key: 'date', label: 'Дата прохождения'},
			{key: 'mark', label: 'Результат'},
			{key: 'comment', label: 'Комментарий'},
			{key: '', label: ''}
		];
		const {currentData} = this.props;
		return (
			<React.Fragment>
				<h2 className="ml-2">Пройденные тесты</h2>
				<Table
					className="mt-4"
					headers={headers}
					data={currentData}
					renderRow={renderRow}/>
				{
					this.state.isPending &&
					<div className="d-flex h-50">
						<Loading/>
					</div>
				}
				{
					this.state.error && (<ErrorPage textError={this.state.error}/>)
				}
				<ModalRoot/>
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({testPassedStudent}) => ({
	currentData: testPassedStudent.currentData,
});

const mapDispatchToProps = (dispatch) => ({
	getTestsPassedStudent: bindActionCreators(getTestsPassedStudent, dispatch),
	redirect: bindActionCreators(showAnswersModal, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TestsPassedStudent);


