import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextAnswer from '../../components/Answers/TextAnswer/TextAnswer';
import ListAnswer from '../../components/Answers/ListAnswer/ListAnswer';
import 'bootstrap/dist/css/bootstrap.css';
import './css/style.css';
import TextArea from '../../components/TextArea/TextArea';
import {bindActionCreators} from 'redux';
import {changeAnswer} from '../../actions/TestView/action';
import connect from 'react-redux/es/connect/connect';

class QuestionView extends Component {
	constructor(props) {
		super(props);

		this.changeAnswer = this.changeAnswer.bind(this);

		this.components = {
			'SHORT_ANSWER':
				<TextAnswer
					data={this.props.data}
					answers={this.props.answers}
					currentPage={this.props.currentPage}
					changeAnswer={this.changeAnswer}
				/>,
			'LONG_ANSWER':
				<TextAnswer
					data={this.props.data}
					answers={this.props.answers}
					currentPage={this.props.currentPage}
					changeAnswer={this.changeAnswer}
					maxLength={120}
					placeholder="Напишите развертнутый ответ"
					rows={5}
				/>,
			'LIST_ONE_ANSWER':
				<ListAnswer
					data={this.props.data}
					answers={this.props.answers}
					currentPage={this.props.currentPage}
					changeAnswer={this.changeAnswer}
					multiple={false}
				/>,
			'LIST_MULTIPLE_ANSWER':
				<ListAnswer
					data={this.props.data}
					answers={this.props.answers}
					currentPage={this.props.currentPage}
					changeAnswer={this.changeAnswer}
					multiple={true}
				/>,
		};
	}

	changeAnswer(value) {
		const {testData, currentPage} = this.props;
		const page = currentPage;
		const questionId = testData.questions[currentPage - 1].id;
		let answer;
		if (value instanceof Array) {
			const ids = value.map((item) => {
				return testData.questions[currentPage - 1].answers[item - 1].id;
			});
			answer = ids.join(' ');
		} else {
			answer = value;
		}
		this.props.changeAnswer({ answer, page, questionId });
	}

	render() {
		const Answer = this.components[this.props.type];
		return (
			<div className="container">
				<div className="row border">
					<div className="col-12">
						<TextArea
							rows = {10}
							placeholder = {this.props.question}
							readOnly
						/>
					</div>
					<div className="col-12 p-3">
						{ Answer }
					</div>
				</div>
			</div>
		);
	}
}

QuestionView.propTypes = {
	type: PropTypes.string.isRequired,
	data: PropTypes.array,
};

QuestionView.defaultProps = {
	type: '',
	data: [],
};

const mapStateToProps = (state) => ({
	currentPage: state.testView.currentPage,
	answers: state.testView.answers,
	testData: state.testView.data,
});

const mapDispatchToProps = (dispatch) => ({
	changeAnswer: bindActionCreators(changeAnswer, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(QuestionView);

