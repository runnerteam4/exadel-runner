import React, {Component} from 'react';
import Countdown from '../../components/Countdown/Countdown';
import Pagination from '../../components/ListPagination/Pagination';
import QuestionView from '../QuestionView/QuestionView';
import Button from '../../components/Button/Button';
import {onNext, onPrevious, fetchTest, update, fetchAnswers} from '../../actions/TestView/action';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import '../../style.css';
import {showMessage} from '../../actions/Modals/ModalAction';
import ModalRoot from '../../components/Modals/ModalRoot';
import ErrorPage from '../../components/ErrorPage/ErrorPage';
import {answers} from "../../mocks/TestCheck/tests";


class TestView extends Component {
	constructor(props) {
		super(props);

		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		this.props.fetchTest(Number(this.props.match.params.id));
	}

	componentDidUpdate() {
		localStorage.setItem('currentPage', JSON.stringify(this.props.currentPage));
		localStorage.setItem('answers', JSON.stringify(this.props.answers));
	}

	// fetch data to server
	async handleSubmit(e) {
		if (e) {
			e.preventDefault();
		}
		const answer = window.confirm('Закончить тест?');
		if (answer) {
			await this.props.fetchAnswers(this.props.match.params.id, this.props.answers);
			localStorage.clear();
			this.props.update();
			this.props.showMessage();
			this.props.history.push('/app');
		}
	}

	componentWillUnmount() {
		localStorage.clear();
		this.props.update();
	}

	render() {
		const {data, currentPage, onNext, onPrevious} = this.props;
		return (
			<React.Fragment>
				{ !this.props.isFetching && !this.props.error &&
				<div className="container shadow border noSelect">
					<form onSubmit={this.handleSubmit}>
						<div className="row text-dark">
							<Countdown
								seconds={this.props.seconds}
								timeOver={this.handleSubmit}
							/>
						</div>
						<QuestionView
							key={data.questions[currentPage - 1].id}
							type={data.questions[currentPage - 1].type}
							question={data.questions[currentPage - 1].text}
							data={data.questions[currentPage - 1].answers}
						/>
						<div className="row align-items-center">
							<div className="col">
								<Pagination
									totalPageNumber={data.questions.length}
									currentPage={currentPage}
									chunkSize={1}
									chunkSizes={[]}
									onNext={onNext}
									onPrevious={onPrevious}
								/>
							</div>
							<div className="col text-right">
								<Button
									type="submit"
									value="Submit"
									className="btn-primary"
									onClick={this.handleSubmit}
								>
									Завершить тест
								</Button>
							</div>
						</div>
					</form>
				</div>
				}
				{
					this.props.error &&
					<ErrorPage textError={this.props.error.message}/>
				}
				<ModalRoot/>
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({testView}) => ({
	data: testView.data,
	currentPage: testView.currentPage,
	answers: testView.answers,
	seconds: testView.seconds,
	isFetching: testView.isFetching,
	error: testView.error,
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	fetchTest: bindActionCreators(fetchTest, dispatch),
	update: bindActionCreators(update, dispatch),
	showMessage: bindActionCreators(showMessage, dispatch),
	fetchAnswers: bindActionCreators(fetchAnswers, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TestView);
