import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {NavLink} from 'react-router-dom';
import {onChangeChunk, onNext, onPrevious, getCategories, getTestList} from '../../actions/TestList/actions';
import Select from 'react-select';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';
import Table from '../../components/Table/Table';
import Pagination from '../../components/ListPagination/Pagination';
import TestRow from './TestRow';
import Loading from '../../components/Loading/Loading';
import ErrorPage from '../../components/ErrorPage/ErrorPage';

class TestsListView extends React.Component {
	constructor() {
		super();

		this.state = {
			filter: {
				categories: [],
				testType: '',
				input: '',
			}
		};
	}

	componentDidMount() {
		this.props.getCategories();
		this.props.getTestList(this.getFilter(), this.props.chunkSize);
	}

	getFilter = () => {
		const filter = this.state.filter;
		const categories = filter.categories;
		const testType = filter.testType;
		const input = filter.input;
		return {
			categories: categories ? categories.map(obj => obj.label) : [],
			testType: testType ? testType.value : '', // return undef
			input: input ? input : ''
		};
	};

	onChangeInput = (event) => {
		const value = event.target.value;
		this.setState((prevState) => {
			return {
				filter: {
					...prevState.filter,
					input: value
				}
			}
		});
	};

	selectOptionTypeTests = (testType) => {
		this.setState((prevState) => {
			return {
				filter: {
					...prevState.filter,
					testType: testType,
				}
			}
		});
	};

	selectOptionCategory = (categories) => {
		this.setState((prevState) => {
			return {
				filter: {
					...prevState.filter,
					categories,
				}
			}
		});
	};

	onNext = () => {
		this.props.onNext(this.getFilter(), this.props.chunkSize);
	};

	onPrevious = () => {
		this.props.onPrevious(this.getFilter(), this.props.chunkSize);
	};

	onChangeChunk = (value) => {
		this.props.onChangeChunk(this.getFilter(), value);
	};

	getTestList = () => {
		this.props.getTestList(this.getFilter(), this.props.chunkSize);
	};

	render() {
		const {
			currentPage, chunkSize, currentData, totalPageNumber, categories, isPending, error
		} = this.props;

		const renderRow = (item, index, headers) => {
			return <TestRow item={item} key={index} headers={headers}/>;
		};

		return (
			<React.Fragment>
				<div className="d-flex justify-content-between">
					<h2>Назначенные тесты</h2>
					<NavLink to='/app/tests/passed' className='text-dark col-4'>
						<Button className="btn btn-primary ">
							Просмотреть пройденные
						</Button>
					</NavLink>
				</div>
				<div className="d-flex mt-3">
					<Select
						isMulti={true}
						value={this.state.categories}
						placeholder="Выберите категорию"
						className="w-100"
						onChange={this.selectOptionCategory}
						options={categories}
					/>
					<div className="w-100 ml-3 mr-3">
						<Select
							value={this.state.testType}
							placeholder="Выберите тип теста"
							onChange={this.selectOptionTypeTests}
							options={[
								{value: 'Тренировочный', label: 'Тренировочный'},
								{value: 'Контрольный', label: 'Контрольный'},
							]}
						/>
					</div>
					<Button
						className="btn"
						onMouseUp={this.getTestList}>
						Применить
					</Button>
				</div>
				<Input
					className="mt-2"
					placeholder="Поиск"
					onChange={this.onChangeInput}
					value={this.state.filter.input}/>
				<Table
					className="mt-4"
					headers={[
						{key: 'name', label: 'Название'},
						{key: 'type', label: 'Тип теста'},
						{key: 'time', label: 'Время на прохождение'},
						{key: 'deadline', label: 'Дедлайн'},
						{key: 'button', label: ''}
					]}
					data={currentData}
					renderRow={renderRow}
				>
				</Table>
				{isPending && (
					<div className="d-flex h-50">
						<Loading/>
					</div>
				)}
				{(!error && !isPending && currentData.length === 0) &&
				<div className="mt-5 mb-5">
					<h3 className="text-center">Ничего не найдено</h3>
				</div>
				}
				{!isPending && !error && (
					<div>
						<Pagination
							totalPageNumber={totalPageNumber}
							currentPage={currentPage}
							chunkSize={chunkSize}
							chunkSizes={[2, 5, 10]}
							onNext={this.onNext}
							onPrevious={this.onPrevious}
							onChangeChunk={this.onChangeChunk}
						/>
					</div>
				)}
				{error && (<ErrorPage textError={error}/>)}
			</React.Fragment>
		);
	}
}

const mapStateToProps = ({testList}) => ({
	currentData: testList.pagination.currentData,
	chunkSize: testList.pagination.chunkSize,
	currentPage: testList.pagination.currentPage,
	totalPageNumber: testList.pagination.totalPageNumber,
	isPending: testList.pagination.isPending,
	status: testList.pagination.status,
	error: testList.pagination.error,
	categories: testList.categories.categories,
});

const mapDispatchToProps = (dispatch) => ({
	onNext: bindActionCreators(onNext, dispatch),
	onPrevious: bindActionCreators(onPrevious, dispatch),
	onChangeChunk: bindActionCreators(onChangeChunk, dispatch),
	getCategories: bindActionCreators(getCategories, dispatch),
	getTestList: bindActionCreators(getTestList, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TestsListView);
