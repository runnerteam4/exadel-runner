import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Button from '../../components/Button/Button';

export default class TestRow extends Component {
	renderItemElement = (header, index) => {
		if (header.label === '') {
			return (
				<td key={index}>
					<NavLink to={`/app/tests/${this.props.item.id}`} className='text-dark'>
						<Button>Начать тест</Button>
					</NavLink>
				</td>
			);
		}
		if (header.label === 'Тип теста') {
			const type = this.props.item[header.key] ? 'Тренировочный' : 'Контрольный';
			return (
				<td key={index}>{type}</td>
			);
		}
		if (header.label === 'Время на прохождение') {
			return (
				<td key={index}>{`${Math.floor(this.props.item[header.key] / 60)} минут`}</td>
			);
		}
		if (header.label === 'Дедлайн') {
			return (
				<td key={index}>{this.props.item[header.key]}</td>
			);
		}
		return (
			<td key={index}>{this.props.item[header.key]}</td>
		);
	};

	render() {
		return (
			<tr>
				{this.props.headers.map(this.renderItemElement)}
			</tr>
		);
	}
}
TestRow.propTypes = {
	item: PropTypes.object,
	headers: PropTypes.array
};
