import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import App from './App';
import LoginView from './views/LoginView/LoginView';
import MainRoute from "./components/MainRoute/MainRoute";
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import store from './reducers/index';
import thunk from 'redux-thunk';
import RegistrationView from "./views/RegistrationView/RegistrationView";
import StartPage from "./views/StartPage/StartPage";



const appStore = createStore(store, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
	applyMiddleware(thunk));

const Wrapper = () => (
	<Provider store={appStore}>
		<Router>
			<App>
				<Switch>
					<Route path='/login' component={LoginView}/>
					<Route path='/registration' component={RegistrationView}/>
					<Route path = '/' component = {MainRoute}/>
				</Switch>
			</App>
		</Router>
	</Provider>
);

export default Wrapper;
