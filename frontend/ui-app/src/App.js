import React from 'react';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import './style.css';

const App = (props) => (
    <div className="d-flex flex-column position-relative background">
        <Header
            userName=''
            linkSignUp='/registration'
            linkSignIn='/login'
            linkLogOut='/'
        />
        <main role="main" className="container bg-white d-inline-flex flex-column col-8 p-0">
                    {props.children}
        </main>
        <Footer/>
    </div>
);

export default App;
