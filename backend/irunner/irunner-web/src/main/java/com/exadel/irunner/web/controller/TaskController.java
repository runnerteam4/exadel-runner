package com.exadel.irunner.web.controller;

import com.exadel.irunner.model.dto.TaskAnswerDTO;
import com.exadel.irunner.model.dto.TaskDTO;
import com.exadel.irunner.model.dto.*;
import com.exadel.irunner.model.filters.TaskFilter;
import com.exadel.irunner.service.converter.TaskAnswerConverter;
import com.exadel.irunner.service.task.TaskService;
import com.exadel.irunner.service.taskAnswer.TaskAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    TaskAnswerConverter taskAnswerConverter;

    @Autowired
    TaskAnswerService taskAnswerService;

    @GetMapping
    public ResponseEntity<List<TaskDTO>> getAll() {
        List<TaskDTO> taskDTOs = taskService.findAll();
        return ResponseEntity.ok(taskDTOs);
    }
    


    @GetMapping("/teacher/{id}")
    public ResponseEntity<List<StudentTaskDTO>> getAllStudentTasksOfCurrentTeacher(@PathVariable("id") Long userId){
        List<StudentTaskDTO> tasks = taskService.getStudentTaskOfCurrentTeacher(userId);
        return ResponseEntity.ok(tasks);
    }

    @PostMapping("/student/{id}/task/{taskId}/run")
    public TaskFilter handleFileUpload(@RequestPart("file") MultipartFile file,
                                       @PathVariable("id") Long id, @PathVariable("taskId")Long taskId ) throws ClassNotFoundException, NoSuchMethodException,
                                        InvocationTargetException, IllegalAccessException, IOException {
        return taskService.compileTask(file, taskId, id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDTO> findById(@PathVariable("id") Long id){
        return ResponseEntity.ok(taskService.findById(id));
    }

    @PostMapping("/new")
    public ResponseEntity<TaskDTO> add(@RequestBody TaskDTO taskDTO) {
        TaskDTO taskDTO1 = taskService.create(taskDTO);
        return ResponseEntity.ok(taskDTO1);
    }

    @GetMapping("/categories")
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        List<CategoryDTO> categories = taskService.getCategories();
        return ResponseEntity.ok(categories);
    }

    @PostMapping("/{id}/test/new")
    public ResponseEntity<List<TaskAnswerDTO>> add(@RequestBody List<TaskAnswerDTO> taskAnswerDTO, @PathVariable("id") Long taskId) {
        taskAnswerService.create(taskAnswerDTO,taskId);
        return ResponseEntity.ok(taskAnswerDTO);
    }

    @GetMapping("/tests/{id}")
        public ResponseEntity<List<TaskAnswerDTO>> getAllTaskAnswers(@PathVariable("id") Long taskId){
        return ResponseEntity.ok(taskService.findInputAnswerByTaskId(taskId));
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<List<StudentTaskDTO>> getAllStudentTasks(@PathVariable("id") Long userId){
       return  ResponseEntity.ok(taskService.getStudentTasks(userId));
    }
/*
    @GetMapping("/{name}")
    public ResponseEntity<TaskDTO> findByName(@PathVariable("name") String name) {
        TaskDTO taskDTO = taskService.findByName(name);
        return ResponseEntity.ok(taskDTO);
    }*/


    /*@GetMapping("/taskAns/{id}")//
    public ResponseEntity<List<TaskAnswerDTO>> findById(@PathVariable("id") Long id) {
        List<TaskAnswerDTO> taskAnswer = taskService.findInputAnswerByTaskId(id);
        return ResponseEntity.ok(taskAnswer);
    }*/

    @GetMapping("/{task_id}/students/{id}")
    public ResponseEntity<StudentTaskDTO> getTasksForStudent(@PathVariable("task_id") Long taskId, @PathVariable("id") Long id) {
        StudentTaskDTO studentTaskDTO = taskService.getStudentTask(taskId, id);
        return ResponseEntity.ok(studentTaskDTO);
    }

    @PostMapping("/assign/student")
    public ResponseEntity<StudentTaskDTO> assignTaskForStudent(@RequestBody StudentTaskDTO studentTaskDTO) {
        return ResponseEntity.ok(taskService.assignTaskForStudent(studentTaskDTO));

    }

    @PostMapping("/assign/group")
    public ResponseEntity<TaskGroupDTO> assignTaskForGroup(@RequestBody TaskGroupDTO taskGroupDTO) {
       TaskGroupDTO task = taskService.assignTaskForGroup(taskGroupDTO);
       return ResponseEntity.ok(task);
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<HttpStatus> deleteTask(@PathVariable("id") Long taskId){
        taskService.delete(taskId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

/*
    @PostMapping("/teacher/assign/{id}")
    public ResponseEntity<List<StudentTaskFilter>> getStudentTasksOfCurrentTeacher(@PathVariable("id") Long id, @RequestBody List<GroupDTO> groups){
        List<StudentTaskFilter> studentTaskFilters = taskService.getStudentTaskOfCurrentTeacher(id,groups);
        return ResponseEntity.ok(studentTaskFilters);
    }*/
}
