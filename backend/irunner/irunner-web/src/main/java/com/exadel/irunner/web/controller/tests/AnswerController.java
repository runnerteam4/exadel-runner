package com.exadel.irunner.web.controller.tests;

import com.exadel.irunner.model.dto.tests.AnswerMarkDTO;
import com.exadel.irunner.model.dto.tests.AnswerOfUserDTO;
import com.exadel.irunner.model.dto.tests.AnswersForTestDTO;
import com.exadel.irunner.model.dto.tests.MarkDTO;
import com.exadel.irunner.service.tests.AnswerOfUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tests")
public class AnswerController {
    @Autowired
    private AnswerOfUserService answerOfUserService;

    @PostMapping("/saveAnswer")
    public ResponseEntity<AnswersForTestDTO> saveAnswers(@RequestBody AnswersForTestDTO answers){
        answerOfUserService.save(answers);
        return ResponseEntity.ok(answers);
    }

    @GetMapping("/answers/check/{id}")
    public ResponseEntity<List<AnswerOfUserDTO>> getAnswersToCheck(@PathVariable Long id){
        return ResponseEntity.ok(answerOfUserService.getAnswersToCheck(id));
    }

    @PutMapping("/answers/checked")
    public ResponseEntity putMark(@RequestBody MarkDTO mark){
        answerOfUserService.markTest(mark);
        return ResponseEntity.ok().build();
    }

}
