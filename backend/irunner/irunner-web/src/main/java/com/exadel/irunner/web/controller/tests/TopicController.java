package com.exadel.irunner.web.controller.tests;

import com.exadel.irunner.model.dto.tests.TagDTO;
import com.exadel.irunner.model.dto.tests.TopicDTO;
import com.exadel.irunner.service.tests.TagService;
import com.exadel.irunner.service.tests.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tests/topics")
public class TopicController {
    @Autowired
    private TopicService topicService;

    @GetMapping
    public ResponseEntity<List<TopicDTO>> getAll() {
        return ResponseEntity.ok(topicService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TopicDTO> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(topicService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        topicService.delete(id);
        return (ResponseEntity) ResponseEntity.ok();
    }
}
