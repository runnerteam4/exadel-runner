package com.exadel.irunner.web.controller.login;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class LoginController {

    @GetMapping("/admin")
    public ModelAndView admin() {
        return new ModelAndView("/admin");
    }

    @GetMapping("/user")
    public ModelAndView user() {
        return new ModelAndView("/user");
    }


}
