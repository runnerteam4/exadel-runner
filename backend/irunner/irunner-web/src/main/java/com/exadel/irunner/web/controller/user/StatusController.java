package com.exadel.irunner.web.controller.user;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Status controller.
 */
@RestController
@RequestMapping("/")
public class StatusController {

    /**
     * Index string.
     *
     * @return the string
     */
    @RequestMapping("")
    public String index() {
        return "Greetings from Exadel!";
    }

}
