package com.exadel.irunner.web.controller;

import com.exadel.irunner.model.dto.GroupDTO;
import com.exadel.irunner.model.dto.GroupStudentsDTO;
import com.exadel.irunner.model.dto.UserGroupDTO;
import com.exadel.irunner.service.group.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/groups")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @PostMapping("/add")
    public ResponseEntity<GroupDTO> addGroup(@RequestBody GroupStudentsDTO dto) {
        GroupDTO group = groupService.convertToDTO(dto);
        GroupDTO newGroup = groupService.addGroup(group);
        return ResponseEntity.ok(newGroup);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GroupDTO> getGroupById(@PathVariable("id") Long id) {
        GroupDTO group = groupService.getGroupById(id);
        return ResponseEntity.ok(group);
    }

    @GetMapping("/teacher/{id}")
    public ResponseEntity<List<GroupDTO>> getTeacherGroups(@PathVariable("id") Long teacherId) {
        List<GroupDTO> groupDTO = groupService.getTeacherGroups(teacherId);
        return ResponseEntity.ok(groupDTO);
    }

    @PutMapping("/change/{id}")
    public ResponseEntity<HttpStatus> changeGroupName(@PathVariable("id") Long id, @RequestBody GroupDTO group) {
        group.setId(id);
        groupService.changeGroupName(group);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpStatus> deleteGroup(@PathVariable("id") Long id) {
        groupService.deleteGroup(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("exclude/{id}")
    public ResponseEntity<HttpStatus> excludeStudentFromGroup(@PathVariable("id") Long id,
                                                              @RequestBody UserGroupDTO userGroup) {
        userGroup.setUserId(id);
        groupService.excludeStudentFromGroup(userGroup);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/add/{id}")
    public ResponseEntity<HttpStatus> addStudentsToGroup(@PathVariable("id") Long id,
                                                         @RequestBody GroupStudentsDTO dto) {
        dto.setId(id);
        groupService.addStudentsToGroup(dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/assign/{teacherId}/{groupId}")
    public ResponseEntity<HttpStatus> assignTeacherToGroup(@PathVariable("teacherId") Long teacherId,
                                                           @PathVariable ("groupId") Long groupId) {
        UserGroupDTO groupDTO = new UserGroupDTO();
        groupDTO.setUserId(teacherId);
        groupDTO.setGroupId(groupId);
        groupService.assignTeacherToGroup(groupDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
