package com.exadel.irunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class IRunnerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(IRunnerApplication.class, args);
    }

}
