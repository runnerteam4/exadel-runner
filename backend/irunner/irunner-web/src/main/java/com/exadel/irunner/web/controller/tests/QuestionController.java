package com.exadel.irunner.web.controller.tests;

import com.exadel.irunner.model.dto.tests.QuestionDTO;
import com.exadel.irunner.service.tests.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @GetMapping
    public ResponseEntity<List<QuestionDTO>> getAll() {
        return ResponseEntity.ok(questionService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionDTO> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(questionService.findById(id));
    }

    @GetMapping("/training")
    public ResponseEntity<List<QuestionDTO>> getTrainingQuestions() {
        return ResponseEntity.ok(questionService.findTrainingQuestions());
    }

    @PostMapping("/add")
    public ResponseEntity<QuestionDTO> add(@RequestBody QuestionDTO questionDTO) {
        QuestionDTO savedQuestion = questionService.create(questionDTO);
        return ResponseEntity.ok(savedQuestion);
    }
}
