package com.exadel.irunner.web.controller.tests;

import com.exadel.irunner.model.dto.tests.TagDTO;
import com.exadel.irunner.service.tests.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tests/tags")
public class TagController {
    @Autowired
    private TagService tagService;

    @GetMapping
    public ResponseEntity<List<TagDTO>> getAll() {
        return ResponseEntity.ok(tagService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TagDTO> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(tagService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        tagService.delete(id);
        return (ResponseEntity) ResponseEntity.ok();
    }

}
