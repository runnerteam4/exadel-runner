package com.exadel.irunner.web;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@EnableWebMvc
@PropertySource("classpath:config.properties")
@ComponentScan(basePackages = {"com.exadel.irunner"})
public class WebConfig extends WebMvcConfigurerAdapter {

    @Primary
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

//    @Override
//    public void addCorsMappings(CorsRegistry registry) { //
//        registry.addMapping("/**")
//                .allowedOrigins("http://localhost:3000", "https://jdline.herokuapp.com")
//                .allowedHeaders(CorsConfiguration.ALL)
//                .allowedMethods(CorsConfiguration.ALL);
//    }
}