package com.exadel.irunner.web.controller;

import com.exadel.irunner.model.dto.CategoryDTO;
import com.exadel.irunner.model.dto.WikiDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.service.wiki.WikiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/wiki")
public class WikiController {

    @Autowired
    private WikiService wikiService;

    @GetMapping
    public ResponseEntity<List<WikiDTO>> getAll() {
        List<WikiDTO> wikiDTOs = wikiService.findAll();
        return ResponseEntity.ok(wikiDTOs);
    }

    @PostMapping("/material/new")
    public ResponseEntity<WikiDTO> add(@RequestBody WikiDTO wikiDTO) {
        wikiService.create(wikiDTO);
        return ResponseEntity.ok(wikiDTO);
    }

    @GetMapping("/categories")
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        List<CategoryDTO> categories = wikiService.getCategories();
        return ResponseEntity.ok(categories);
    }

    @GetMapping("/categories/{categoryId}")
    public ResponseEntity<List<WikiDTO>> findByCategory(@PathVariable("categoryId") Long categoryId) {
        List<WikiDTO> wikiDTOs= wikiService.findByCategoryId(categoryId);
        return ResponseEntity.ok(wikiDTOs);
    }

    @PostMapping("/category/new")
    public ResponseEntity<CategoryDTO> addCategory(@RequestBody CategoryDTO categoryDTO) {
        categoryDTO = wikiService.createCategory(categoryDTO.getName());
        return ResponseEntity.ok(categoryDTO);
    }

    @GetMapping("/categories/search")
    public ResponseEntity<List<WikiDTO>> findWikiByCategoryIds(@RequestParam(value="id") List<Long> categoryIds) {
        List<WikiDTO> wikiDTOs= wikiService.getWikiByCategoriesIds(categoryIds);
        return ResponseEntity.ok(wikiDTOs);
    }

    @GetMapping("/{name}")
    public ResponseEntity<WikiDTO> findByName(@PathVariable("name") String name) {
        WikiDTO wikiDTOs = wikiService.findByName(name);
        return ResponseEntity.ok(wikiDTOs);
    }

}
