package com.exadel.irunner.web.controller.tests;

import com.exadel.irunner.model.dto.tests.*;
import com.exadel.irunner.model.entity.tests.AnswerOfUser;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import com.exadel.irunner.model.enums.TestStatus;
import com.exadel.irunner.model.filters.TestCheckFilter;
import com.exadel.irunner.model.filters.TestFilter;
import com.exadel.irunner.service.tests.AnswerOfUserService;
import com.exadel.irunner.service.tests.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tests")
public class TestController {
    @Autowired
    private TestService testService;

    @Autowired
    private AnswerOfUserService answerOfUserService;

    @GetMapping
    public ResponseEntity<List<TestDTO>> getAll() {
        return ResponseEntity.ok((testService.findAll()));
    }

    @PostMapping
    public ResponseEntity<List<TestDTO>> getByFilter(@RequestBody TestFilter testFilter) {
        return ResponseEntity.ok(testService.findByFilter(testFilter));
    }

    @GetMapping("/{userId}/{testId}")
    public ResponseEntity<AnswersForTestDTO> startTest(@PathVariable("userId") Long userId, @PathVariable("testId") Long testId) {

        AnswersForTestDTO answersForTest = testService.startTest(userId, testId);
        if(answersForTest.getStatus() == TestStatus.IN_PROGRESS ) {
            return ResponseEntity.ok(answersForTest);
        }
        else {
            return ResponseEntity.status(403).build();
        }
    }

    @PostMapping("/assign/student")
    public ResponseEntity<TestDTO> assignTestForStudent(@RequestBody TestDTO testDTO) {
        testService.assignForStudent(testDTO);
        return ResponseEntity.ok(testDTO);
    }

    @PostMapping("/assign/group")
    public ResponseEntity<TestDTO> assignTestForGroup(@RequestBody TestDTO testDTO) {
        testService.assignForGroup(testDTO);
        return ResponseEntity.ok(testDTO);
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<List<UserTestDTO>> getPassedTests(@PathVariable Long id){
        List<UserTestDTO> answers = testService.findPassedTests(id);
        return ResponseEntity.ok(answers);
    }

    @PostMapping("/student/passed")
    public ResponseEntity<List<TestResultDTO>> getResultForTest(@RequestBody AnswersForTestDTO answersForTestDTO){
        List<TestResultDTO> results = testService.getResults(answersForTestDTO);
        return ResponseEntity.ok(results);
    }

    @PostMapping("/checkList")
    public ResponseEntity<List<TestCheckDTO>> getTestCheckList(@RequestBody TestCheckFilter testCheckFilter){
        List<TestCheckDTO> tests = testService.getTestCheckList(testCheckFilter);
        return ResponseEntity.ok(tests);
    }

    @GetMapping("/currentTime/{id}")
    public ResponseEntity<Long> getCurrentTime(@PathVariable Long id){
        return ResponseEntity.ok(testService.getTime(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        testService.delete(id);
        return (ResponseEntity) ResponseEntity.ok();
    }

}
