package com.exadel.irunner.web.controller;

import com.exadel.irunner.model.dto.*;
import com.exadel.irunner.service.user.SecurityService;
import com.exadel.irunner.service.user.StudentService;
import com.exadel.irunner.service.user.TeacherService;
import com.exadel.irunner.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private SecurityService securityService;

    @GetMapping("/current")
    public ResponseEntity<UserDTO> getCurrentUser() {
        UserDTO user = securityService.currentUser();
        return ResponseEntity.ok(user);
    }

    //@PreAuthorize("hasRole('ROLE_ADMI')")
    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") Long userId){
        UserDTO user = userService.find(userId);
        return ResponseEntity.ok(user);
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserDTO>> getAll(){
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping(value = "/register")
    public ResponseEntity<UserDTO> register(@RequestBody UserDTO user) {
        UserDTO registeredUser = userService.registerNewUserAccount(user);
        return ResponseEntity.ok(registeredUser);
    }

    @PostMapping(value = "/register/student")
    public ResponseEntity<StudentDTO> registerStudent(@RequestBody StudentDTO student) {
        StudentDTO registeredStudent = studentService.registerNewStudentAccount(student);
        return ResponseEntity.ok(registeredStudent);
    }

    @PostMapping(value = "/register/teacher")
    public ResponseEntity<TeacherDTO> registerTeacher(@RequestBody TeacherDTO teacher) {
        TeacherDTO registeredTeacher = teacherService.registerNewTeacherAccount(teacher);
        return ResponseEntity.ok(registeredTeacher);
    }

    //@PreAuthorize("isAnonymous()")
    @GetMapping(value = "{id}/enable/{email}")
    public ResponseEntity<UserDTO> enableLogin(@PathVariable("id") Long id, @PathVariable("email") String email) {
        UserDTO user = userService.find(id);
        String userEmail = user.getEmail();
        byte[] bytesOfEmail = userEmail.getBytes();
        String hashEmail = securityService.getHashEmail(bytesOfEmail);
        if(hashEmail.equals(email)){
            userService.enableUser(id);
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = "/assign")
    public ResponseEntity<HttpStatus> assign(@RequestBody UserDTO user){
        userService.assignRoleToUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("change/{id}")
    public ResponseEntity<HttpStatus> changePassword(@PathVariable("id") Long userId,
                                                     @RequestBody UserPasswordDTO dto){
        userService.changePassword(userId,dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/request")
    public ResponseEntity<List<UserDTO>> getUnconfirmedTeachers(){
        List<UserDTO> unconfirmedTeachers = userService.getUnconfirmedTeachers();
        return ResponseEntity.ok(unconfirmedTeachers);
    }

    @PutMapping("/teacher/enable/{id}")
    public ResponseEntity<HttpStatus> enableTeacher(@PathVariable("id") Long teacherId,
                                                    @RequestBody UserDTO user){
        user.setId(teacherId);
        userService.confirmTeacher(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/info/{id}")
    public ResponseEntity<GeneralUserDTO> getUserInformation(@PathVariable("id") Long userId){
        GeneralUserDTO generalUser = userService.getUserInformation(userId);
        return ResponseEntity.ok(generalUser);
    }

}