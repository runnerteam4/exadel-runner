package com.exadel.irunner;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.exadel.irunner"})
public class TestConfig {
}
