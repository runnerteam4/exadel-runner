package com.exadel.irunner.compiler.compilers;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class JavaCompilerTest {
    @Autowired
    private static JavaCompiler javaCompiler;

    @Test
    public void compileCorrect() throws IOException {
        javaCompiler.compile(new ByteArrayInputStream("com/exadel/irunner/compiler/Sample.java".getBytes()), "com/exadel/irunner/compiler/Sample.java");
    }
}