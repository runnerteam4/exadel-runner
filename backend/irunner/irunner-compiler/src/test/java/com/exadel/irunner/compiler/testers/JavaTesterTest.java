package com.exadel.irunner.compiler.testers;

import com.exadel.irunner.TestConfig;
import com.exadel.irunner.compiler.testers.JavaTester;
import com.exadel.irunner.dao.repository.task.TaskAnswerDAO;
import com.exadel.irunner.dao.repository.task.TaskDAO;
import com.exadel.irunner.model.entity.TaskAnswer;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;


import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestConfig.class)
public class JavaTesterTest {

    @Autowired
    private JavaTester javaTester;


    @Test
    public void testerTestCorrectAnswer() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException {
        String expected = "5";
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("Sample.java").getFile());
        ByteArrayInputStream bis = new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
        String ans = javaTester.test(bis, "Sample.java",new TaskAnswer());
        Assert.assertEquals(ans, expected);
    }

    @Test
    public void testerTestWrongAnswer() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException {
        String expected = "6";
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("Sample.java").getFile());
        ByteArrayInputStream bis = new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
        String ans = javaTester.test(bis, "Sample.java",new TaskAnswer());
        Assert.assertNotEquals(ans, expected);
    }
}