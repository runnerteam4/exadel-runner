import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Sample {
    public static String test(String params) {
        Scanner sc = new Scanner(params);
        int a = sc.nextInt(), b = sc.nextInt();
        int c = a + b;
        return new String(Integer.toString(c));
    }
}
