package com.exadel.irunner.compiler.interfaces;

import com.exadel.irunner.model.entity.TaskAnswer;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

public interface Runner {
    String run(TaskAnswer taskAnswer) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException;
}
