package com.exadel.irunner.compiler;

import com.exadel.irunner.compiler.compilers.JavaCompiler;
import com.exadel.irunner.compiler.runners.JavaRunner;
import com.exadel.irunner.compiler.testers.JavaTester;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration
public class CompilerConfig {

    @Bean
    public JavaTester javaTester(){
        return new JavaTester();
    }

    @Bean
    public JavaCompiler javaCompiler(){
        return new JavaCompiler();
    }

}
