package com.exadel.irunner.compiler.runners;
import com.exadel.irunner.compiler.interfaces.Runner;
import com.exadel.irunner.model.entity.TaskAnswer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;
public class JavaRunner implements Runner {
    private ClassLoader classLoader;
    private String name;
    public JavaRunner(ClassLoader classLoader, String name) {
        this.classLoader = classLoader;
        this.name = name;
    }
    @Override
    public String run(TaskAnswer taskAnswer) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> sample = classLoader.loadClass(name);
        Method test = sample.getMethod("test",String.class);
        return (String) test.invoke(test,taskAnswer.getInput());
    }
}