package com.exadel.irunner.compiler.testers;

import com.exadel.irunner.compiler.compilers.JavaCompiler;
import com.exadel.irunner.compiler.interfaces.Tester;
import com.exadel.irunner.model.entity.TaskAnswer;
import org.apache.commons.io.FilenameUtils;
import com.exadel.irunner.compiler.runners.JavaRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

@Service
public class JavaTester implements Tester {

    @Autowired
    private JavaCompiler javaCompiler;

    @Override
    public String test(InputStream inputStream, String fileName, TaskAnswer taskAnswer) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {
        ClassLoader classLoader = javaCompiler.compile(inputStream, fileName);
        String name = FilenameUtils.getBaseName(fileName);
        JavaRunner javaRunner = new JavaRunner(classLoader, name);
        return javaRunner.run(taskAnswer);
    }
}