package com.exadel.irunner.compiler.compilers;

import com.exadel.irunner.compiler.interfaces.Compiler;
import org.apache.commons.io.FilenameUtils;
import com.exadel.irunner.compiler.utils.*;
import org.springframework.stereotype.Service;

import javax.tools.*;
import java.io.*;
import java.util.*;

@Service
public class JavaCompiler implements Compiler {
    @Override
    public ClassLoader compile(InputStream inputStream, String fileName) throws IOException {
        String name = FilenameUtils.getBaseName(fileName);
        StringBuilder program = new StringBuilder();
        Scanner sc = new Scanner(inputStream);
        while (sc.hasNext()) {
            program.append(sc.nextLine());
        }

        javax.tools.JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        JavaFileObject compilationUnit =
                new StringJavaFileObject(name, program.toString());

        SimpleJavaFileManager fileManager =
                new SimpleJavaFileManager(compiler.getStandardFileManager(null, null, null));

        javax.tools.JavaCompiler.CompilationTask compilationTask = compiler.getTask(
                null, fileManager, null, null, null, Arrays.asList(compilationUnit));

        compilationTask.call();

        return new CompiledClassLoader(fileManager.getGeneratedOutputFiles());

    }
}