package com.exadel.irunner.compiler.interfaces;

import com.exadel.irunner.model.entity.TaskAnswer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;


public interface Tester {
    String test(InputStream inputStream, String fileName, TaskAnswer taskAnswer) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, IOException;
}
