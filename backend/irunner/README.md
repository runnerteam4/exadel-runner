# Heroku configs
1. APP URL: https://i-runner.herokuapp.com
2. Heroku Git https://git.heroku.com/i-runner.git
3. Heroku app name - **i-runner** (specify -a i-runner in all heroku actions)

# Preliminary actions
1. Open root application git directory (which contains .git file)
2. Install CLI deploy plugin (heroku plugins:install heroku-cli-deploy)
3. Install Clear DB Mysql add-on in client application (if haven't yet) (https://dashboard.heroku.com/apps/i-runner/r) 
Run **heroku config** to get database credentials/url.

# Heroku app deployment
1. Build irunner.war with heroku database configs.
2. Deploy the war: heroku war:deploy {path-to-war} -a i-runner
3. Enjoy the app under https://i-runner.herokuapp.com/
