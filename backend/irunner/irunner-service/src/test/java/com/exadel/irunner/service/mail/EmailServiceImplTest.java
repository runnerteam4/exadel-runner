package com.exadel.irunner.service.mail;

import com.exadel.irunner.service.mail.interfaces.EmailService;
import com.exadel.irunner.service.mail.interfaces.MessageBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = EmailConfig.class)

public class EmailServiceImplTest {

    @Autowired
    private EmailService emailService;

    @Autowired
    private MessageBuilder messageBuilder;


    @Test
    public void sendSimpleMessageTest() throws MessagingException {
        String[] recipients = {"exadelrunner@mail.ru"};
        MimeMessage message = emailService.getMimeMessage();
        emailService.sendMessage(messageBuilder.buildMessage(message, recipients, "Anton", "Test message"));
    }

    @Test
    public void sendMessageWithAttachmentTest() throws MessagingException {
        String[] recipients = {"exadelrunner@mail.ru"};
        String[] paths = {"src\\test\\resources\\attach.txt"};
        MimeMessage message = emailService.getMimeMessage();
        emailService.sendMessage(messageBuilder.buildMessage(message, recipients, "Anton", "Test message with file", paths));
    }
}