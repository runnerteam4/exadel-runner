package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.tests.AnswersForTestDTO;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import com.exadel.irunner.model.entity.tests.Test;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnswersForTestConverter extends BaseConverter<AnswersForTest, AnswersForTestDTO> {

    @Autowired
    private AnswerOfUserConverter answerOfUserConverter;

    @Autowired
    private QuestionConverter questionConverter;

    @Override
    public AnswersForTestDTO convert(AnswersForTest answersForTest) {
        AnswersForTestDTO answersForTestDTO = new AnswersForTestDTO();
        answersForTestDTO.setId(answersForTest.getId());
        answersForTestDTO.setTestId(answersForTest.getTest().getId());
        answersForTestDTO.setUserId(answersForTest.getStudent().getId());
        answersForTestDTO.setAnswers(answerOfUserConverter.convert(answersForTest.getAnswers()));
        answersForTestDTO.setMark(answersForTest.getMark());
        answersForTestDTO.setStartTime(answersForTest.getStartTime());
        answersForTestDTO.setStatus(answersForTest.getStatus());
        answersForTestDTO.setEndTime(answersForTest.getEndTime());
        answersForTestDTO.setComment(answersForTest.getComment());
        answersForTestDTO.setQuestions(questionConverter.convert(answersForTest.getQuestions()));
        return answersForTestDTO;
    }


    @Override
    public AnswersForTest convert(AnswersForTestDTO answersForTestDTO) {
        AnswersForTest answersForTest = new AnswersForTest();
        answersForTest.setId(answersForTestDTO.getId());
        answersForTest.setAnswers(answerOfUserConverter.reverseConvert(answersForTestDTO.getAnswers()));
        Test test = new Test();
        test.setId(answersForTestDTO.getTestId());
        answersForTest.setTest(test);
        User user = new User();
        user.setId(answersForTestDTO.getUserId());
        answersForTest.setStudent(user);
        answersForTest.setComment(answersForTestDTO.getComment());
        return answersForTest;
    }
}
