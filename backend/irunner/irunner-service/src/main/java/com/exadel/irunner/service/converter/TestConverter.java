package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.tests.tag.TagDAO;
import com.exadel.irunner.model.dto.tests.TagDTO;
import com.exadel.irunner.model.dto.tests.TestDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.tests.Tag;
import com.exadel.irunner.model.entity.tests.Test;
import com.exadel.irunner.model.entity.tests.Topic;
import com.exadel.irunner.model.enums.TestStatus;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TestConverter extends BaseConverter<Test, TestDTO> {

    @Autowired
    private TagDAO tagDAO;

    @Override
    public TestDTO convert(Test test) {
        TestDTO testDTO = new TestDTO();
        testDTO.setId(test.getId());
        testDTO.setName(test.getName());
        testDTO.setTraining(test.isTraining());
        testDTO.setTime(test.getTime());
        testDTO.setTags(test.getTags().stream().map(Tag::getName).collect(Collectors.toList()));
        testDTO.setTime(test.getTime());
        testDTO.setDeadline(test.getDeadline());
        testDTO.setTopicId(test.getTopic().getId());
        testDTO.setQuestionAmount(test.getQuestionAmount());
        return testDTO;
    }

    @Override
    public Test convert(TestDTO testDTO) {
        Test test = new Test();
        test.setId(testDTO.getId());
        test.setName(testDTO.getName());
        test.setTraining(testDTO.isTraining());
        test.setTime(testDTO.getTime());
        List<Tag> tags = tagDAO.findByName(testDTO.getTags());
        test.setTags(tags);
        test.setTime(testDTO.getTime());
        test.setDeadline(testDTO.getDeadline());
        User user = new User();
        user.setId(testDTO.getStudentId());
        Category topic = new Category();
        topic.setId(testDTO.getTopicId());
        test.setTopic(topic);
        test.setQuestionAmount(testDTO.getQuestionAmount());
        return test;
    }
}
