package com.exadel.irunner.service.user.impl;

import com.exadel.irunner.dao.repository.user.StudentDAO;
import com.exadel.irunner.model.dto.StudentDTO;
import com.exadel.irunner.model.entity.Student;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.Role;
import com.exadel.irunner.service.converter.StudentConverter;
import com.exadel.irunner.service.user.SecurityService;
import com.exadel.irunner.service.user.StudentService;
import com.exadel.irunner.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    private static final boolean CONFIRMED = true;

    @Autowired
    private UserService userService;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private StudentConverter studentConverter;

    @Autowired
    private SecurityService securityService;

    @Override
    public StudentDTO registerNewStudentAccount(StudentDTO studentDTO) {
        Role role = Role.ROLE_STUDENT;
        Student student = studentConverter.convert(studentDTO);
        User registeredUser = student.getUser();
        String email = registeredUser.getEmail();
        userService.validateEmail(email);
        registeredUser.setRole(role);
        String userPassword = securityService.generatePassword();
        String encodedPassword = securityService.getEncodedPassword(userPassword);
        registeredUser.setPassword(encodedPassword);
        registeredUser.setStatus(CONFIRMED);
        studentDAO.save(student);
        userService.sendMessageToUser(registeredUser, userPassword);
        return studentConverter.convert(student);
    }

}
