package com.exadel.irunner.service.mail;

import com.exadel.irunner.service.mail.interfaces.MessageBuilder;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;


@Service
public class MessageBuilderImpl implements MessageBuilder {


    @Override
    public MimeMessage buildMessage(MimeMessage message, String[] recipients, String subject, String text) {

        MimeMessageHelper helper = new MimeMessageHelper(message);

        try {
            helper.setTo(recipients);
            helper.setSubject(subject);
            helper.setText(text);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        return message;
    }

    @Override
    public MimeMessage buildMessage(MimeMessage message, String[] recipients, String subject, String text,
                                    String[] paths) {

        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(recipients);
            helper.setSubject(subject);
            helper.setText(text);
            addAttachment(helper,paths);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        return message;
    }

    private void addAttachment(MimeMessageHelper helper,String[] paths) throws MessagingException {
        for (String path : paths) {
            FileSystemResource file = new FileSystemResource(new File(path));
            helper.addAttachment(FilenameUtils.getName(path), file);
        }
    }
}
