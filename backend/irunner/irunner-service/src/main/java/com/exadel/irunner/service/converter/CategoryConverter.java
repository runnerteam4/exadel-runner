package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.CategoryDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter extends BaseConverter<Category,CategoryDTO> {

    @Override
    public CategoryDTO convert(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        return categoryDTO;
    }

    @Override
    public Category convert(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setId(categoryDTO.getId());
        category.setName(categoryDTO.getName());
        return category;
    }
}
