package com.exadel.irunner.service.user;

import com.exadel.irunner.model.dto.StudentDTO;

public interface StudentService {
    StudentDTO registerNewStudentAccount(StudentDTO student);
}
