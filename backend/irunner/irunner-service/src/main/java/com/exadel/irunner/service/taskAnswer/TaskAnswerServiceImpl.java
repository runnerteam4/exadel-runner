package com.exadel.irunner.service.taskAnswer;

import com.exadel.irunner.dao.repository.task.TaskAnswerDAO;
import com.exadel.irunner.model.dto.TaskAnswerDTO;
import com.exadel.irunner.model.entity.TaskAnswer;
import com.exadel.irunner.service.converter.TaskAnswerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class TaskAnswerServiceImpl implements TaskAnswerService {

    @Autowired
   private TaskAnswerDAO taskAnswerDAO;

    @Autowired
   private TaskAnswerConverter taskAnswerConverter;

    @Override
    public TaskAnswerDTO findById(Long id) {
        TaskAnswer taskAnswer = taskAnswerDAO.findById(id).get();
        return taskAnswerConverter.convert(taskAnswer);
    }

    @Override
    public List<TaskAnswerDTO> create(List<TaskAnswerDTO> taskAnswerDTO,Long taskId) {
        taskAnswerDTO.forEach((item)->item.setTaskId(taskId));
        List<TaskAnswer> taskAnswers = taskAnswerConverter.reverseConvert(taskAnswerDTO);

        for (TaskAnswer taskAnswer: taskAnswers) {
            taskAnswerDAO.save(taskAnswer);
        }
        return taskAnswerConverter.convert(taskAnswerDAO.findTaskAnswersByTaskId(taskId));
    }

    @Override
    public List<TaskAnswerDTO> findAll() {
        List<TaskAnswer> taskAnswers = new ArrayList<>();
        taskAnswerDAO.findAll().forEach(taskAnswers::add);
        return taskAnswerConverter.convert(taskAnswers);
    }


    @Override
    public void delete(Long id) {
    }


}
