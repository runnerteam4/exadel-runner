package com.exadel.irunner.service.group;

import com.exadel.irunner.dao.repository.group.CustomGroupDAO;
import com.exadel.irunner.dao.repository.group.GroupDAO;
import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.dto.*;
import com.exadel.irunner.model.entity.BaseEntity;
import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.UserGroup;
import com.exadel.irunner.model.enums.Role;
import com.exadel.irunner.model.exception.*;
import com.exadel.irunner.service.converter.GroupConverter;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {

    private static final String EXCLUDE_STUDENT_ERROR_MESSAGE_TEMPLATE = "There's no user/group with such id";
    private static final String NO_GROUP_ERROR_MESSAGE = "There's no group with such id";
    private static final String NO_TEACHER_GROUPS_ERROR_MESSAGE = "The teacher with such id doesn't have any group";
    private static final String NO_STUDENT_ERROR_MESSAGE = "There's no student with such id";

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private CustomGroupDAO customGroupDAO;

    @Autowired
    private GroupConverter groupConverter;

    @Autowired
    private UserDAO userDAO;

    @Override
    public GroupDTO addGroup(GroupDTO groupDTO) {
        Group newGroup = groupConverter.convert(groupDTO);
        Set<User> users = newGroup.getUsers();
        checkUsersExisting(users);
        Group group = groupDAO.save(newGroup);
        return groupConverter.convert(group);
    }

    @Override
    public GroupDTO convertToDTO(GroupStudentsDTO dto) {
        GroupDTO group = new GroupDTO();
        String groupName = dto.getName();
        group.setName(groupName);
        Set<UserDTO> userDTOS = getUsersDTOSet(dto);
        group.setUsers(userDTOS);
        return group;
    }

    private Set<UserDTO> getUsersDTOSet(GroupStudentsDTO dto) {
        Set<UserDTO> users = new HashSet<>();
        Set<Long> userIds = dto.getStudentIds();
        userIds.forEach(id -> {
            UserDTO userDTO = new UserDTO();
            userDTO.setId(id);
            users.add(userDTO);
        });
        return users;
    }

    @Override
    public GroupDTO getGroupById(Long id) {
        Optional<Group> optionalGroup = groupDAO.findById(id);
        Group other = optionalGroup.orElse(null);
        if (other == null) {
            throw new NoSuchGroupException(NO_GROUP_ERROR_MESSAGE);
        }
        Group group = optionalGroup.get();
        Group studentsOnlyGroup = getStudentsOnlyGroup(group);
        return groupConverter.convert(studentsOnlyGroup);
    }

    private Group getStudentsOnlyGroup(Group group) {
        Group studentsOnlyGroup = new Group();
        Set<User> allUsers = group.getUsers();
        Set<User> studentsOnlyUsers = getStudentsOnlyUsers(allUsers);
        studentsOnlyGroup.setUsers(studentsOnlyUsers);
        Long groupId = group.getId();
        studentsOnlyGroup.setId(groupId);
        String groupName = group.getName();
        studentsOnlyGroup.setName(groupName);
        LocalDateTime date = group.getCreateDate();
        studentsOnlyGroup.setCreateDate(date);
        return studentsOnlyGroup;
    }

    private Set<User> getStudentsOnlyUsers(Set<User> allUsers) {
        Set<User> studentsOnlyUsers = new HashSet<>();
        for (User user : allUsers) {
            if (!(user.getRole() == Role.ROLE_TEACHER || user.getRole() == Role.ROLE_ADMIN)) {
                studentsOnlyUsers.add(user);
            }
        }
        return studentsOnlyUsers;
    }

    @Override
    public List<GroupDTO> getTeacherGroups(Long teacherId) {
        List<Group> group = groupDAO.findByTeacherId(teacherId);
        if (CollectionUtils.isEmpty(group)) {
            throw new NoTeacherGroupsException(NO_TEACHER_GROUPS_ERROR_MESSAGE);
        }
        return groupConverter.convert(group);
    }

    @Override
    public void changeGroupName(GroupDTO group) {
        Long groupId = group.getId();
        String newName = group.getName();
        Integer rowsAffected = groupDAO.changeGroupName(groupId, newName);
        if (rowsAffected == 0) {
            throw new ChangeGroupNameException(NO_GROUP_ERROR_MESSAGE);
        }
    }

    @Override
    public void deleteGroup(Long groupId) {
        Group group = new Group();
        group.setId(groupId);
        if (!groupDAO.existsById(groupId)) {
            throw new NoSuchGroupException(NO_GROUP_ERROR_MESSAGE);
        }
        groupDAO.delete(group);
    }

    @Override
    public void excludeStudentFromGroup(UserGroupDTO userGroup) {
        Long studentId = userGroup.getUserId();
        Long groupId = userGroup.getGroupId();
        Integer rowsAffected = customGroupDAO.excludeStudentFromGroup(studentId, groupId);
        if (rowsAffected == 0) {
            throw new ExcludeStudentException(EXCLUDE_STUDENT_ERROR_MESSAGE_TEMPLATE);
        }
    }

    @Override
    public void addStudentsToGroup(GroupStudentsDTO dto) {
        Set<Long> studentsId = dto.getStudentIds();
        Set<User> users = getUsersSet(dto);
        checkUsersExisting(users);
        Long groupId = dto.getId();
        if (!groupDAO.existsById(groupId)) {
            throw new NoSuchGroupException(NO_GROUP_ERROR_MESSAGE);
        }
        addStudents(studentsId, groupId);
    }

    private void addStudents(Set<Long> studentsId, Long groupId) {
        studentsId.forEach(id -> {
            UserGroup newGroup = new UserGroup();
            newGroup.setGroupId(groupId);
            newGroup.setUserId(id);
            customGroupDAO.addStudentToGroup(newGroup);
        });
    }

    private void checkUsersExisting(Set<User> users) {
        users.stream().map(BaseEntity::getId).filter(userId -> !userDAO.existsById(userId)).forEach(userId -> {
            throw new NoSuchUserException(NO_STUDENT_ERROR_MESSAGE);
        });
    }

    private Set<User> getUsersSet(GroupStudentsDTO dto) {
        Set<User> users = new HashSet<>();
        Set<Long> userIds = dto.getStudentIds();
        userIds.forEach(id -> {
            User user = new User();
            user.setId(id);
            users.add(user);
        });
        return users;
    }

    @Override
    public void assignTeacherToGroup(UserGroupDTO dto) {
        UserGroup userGroup = new UserGroup();
        userGroup.setUserId(dto.getUserId());
        userGroup.setGroupId(dto.getGroupId());
        customGroupDAO.assignTeacherToGroup(userGroup);
    }

}
