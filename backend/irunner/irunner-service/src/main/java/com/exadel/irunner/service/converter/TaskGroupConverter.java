package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.group.GroupDAO;
import com.exadel.irunner.dao.repository.task.TaskDAO;
import com.exadel.irunner.dao.repository.user.TeacherDAO;
import com.exadel.irunner.model.dto.TaskGroupDTO;
import com.exadel.irunner.model.entity.TaskGroup;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskGroupConverter extends BaseConverter<TaskGroup, TaskGroupDTO> {

    @Autowired
    GroupDAO groupDAO;
    @Autowired
    TeacherDAO teacherDAO;
    @Autowired
    TaskDAO taskDAO;

    @Override
    public TaskGroup convert(TaskGroupDTO taskGroupDTO) {
        TaskGroup taskGroup = new TaskGroup();
        taskGroup.setDeadline(taskGroupDTO.getDeadline());
        taskGroup.setGroupId(taskGroupDTO.getGroupId());
        taskGroup.setTeacherId(taskGroupDTO.getTeacherId());
        taskGroup.setTaskId(taskGroupDTO.getTaskId());
        taskGroup.setId(taskGroupDTO.getId());
        return taskGroup;
    }

    @Override
    public TaskGroupDTO convert(TaskGroup taskGroup) {
        TaskGroupDTO taskGroupDTO = new TaskGroupDTO();
        taskGroupDTO.setId(taskGroup.getId());
        taskGroupDTO.setGroupId(taskGroup.getId());
        taskGroupDTO.setDeadline(taskGroup.getDeadline());
        taskGroupDTO.setTeacherId(taskGroup.getTeacherId());
        taskGroupDTO.setTaskId(taskGroup.getTaskId());
        return taskGroupDTO;
    }
}
