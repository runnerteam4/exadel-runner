package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.tests.tag.TagDAO;
import com.exadel.irunner.model.dto.tests.AnswerDTO;
import com.exadel.irunner.model.dto.tests.QuestionDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.tests.Answer;
import com.exadel.irunner.model.entity.tests.Question;
import com.exadel.irunner.model.entity.tests.Tag;
import com.exadel.irunner.model.entity.tests.Topic;
import com.exadel.irunner.service.converter.base.BaseConverter;
import com.exadel.irunner.service.user.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class QuestionConverter extends BaseConverter<Question, QuestionDTO> {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private AnswerConverter answerConverter;

    @Autowired
    private TagDAO tagDAO;

    @Override
    public QuestionDTO convert(Question question) {

        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setId(question.getId());
        questionDTO.setText(question.getText());
        List<String> tags = question.getTags().stream().map(Tag::getName).collect(Collectors.toList());
        questionDTO.setTags(tags);
        questionDTO.setType(question.getType());
        questionDTO.setComplexity(question.getComplexity());
        questionDTO.setAuthorId(question.getAuthor().getId());
        questionDTO.setAnswers(answerConverter.convert(question.getAnswers()));
        questionDTO.setTopicId(question.getTopic().getId());
        questionDTO.setTraining(question.isTraining());
        return questionDTO;
    }

    @Override
    public Question convert(QuestionDTO questionDTO) {
        Question question = new Question();
        question.setId(questionDTO.getId());
        question.setText(questionDTO.getText());
        question.setComplexity(questionDTO.getComplexity());
        question.setType(questionDTO.getType());
        question.setAssignmentNum(0);
        question.setRightAnswersNum(0);
        List<Tag> tags = questionDTO.getTags().stream().map(name->{
            Tag tag = new Tag();
            tag.setName(name);
            return tag;
        }).collect(Collectors.toList());
        question.setTags(tags);
        User author = new User();
        author.setId(questionDTO.getAuthorId());
        question.setAuthor(author);
        question.setTraining(questionDTO.isTraining());
        Category topic = new Category();
        topic.setId(questionDTO.getTopicId());
        question.setTopic(topic);

        question.setAnswers(answerConverter.reverseConvert(questionDTO.getAnswers()));
        return question;
    }
}
