package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.tests.AnswerDTO;
import com.exadel.irunner.model.entity.tests.Answer;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;

@Component
public class AnswerConverter extends BaseConverter<Answer, AnswerDTO>{
    @Override
    public AnswerDTO convert(Answer answer) {
        AnswerDTO answerDTO = new AnswerDTO();
        answerDTO.setId(answer.getId());
        answerDTO.setText(answer.getText());
        answerDTO.setCorrect(answer.isCorrect());
        return answerDTO;
    }

    @Override
    public Answer convert(AnswerDTO answerDTO) {
        Answer answer = new Answer();
        answer.setId(answerDTO.getId());
        answer.setText(answerDTO.getText());
        answer.setCorrect(answerDTO.isCorrect());
        return answer;
    }
}
