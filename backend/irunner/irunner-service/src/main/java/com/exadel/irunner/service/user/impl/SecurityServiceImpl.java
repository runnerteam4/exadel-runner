package com.exadel.irunner.service.user.impl;

import com.exadel.irunner.model.dto.UserDTO;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.security.UserContext;
import com.exadel.irunner.service.converter.UserConverter;
import com.exadel.irunner.service.user.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Service
@Transactional
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    private UserConverter userConverter;

    private static final String DICTIONARY = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final Integer MIN_LENGTH = 10;
    private static final Integer MAX_LENGTH = 15;
    private static final String ALGORITHM = "SHA-256";

    private BCryptPasswordEncoder encoder = createPasswordEncoder();

    @Override
    public String generatePassword() {
        StringBuilder result = new StringBuilder();
        SecureRandom random = new SecureRandom();
        for (int i = 0; i < getPasswordLength(); i++) {
            int dictionaryLength = DICTIONARY.length();
            int index = random.nextInt(dictionaryLength);
            char symbol = DICTIONARY.charAt(index);
            result.append(symbol);
        }
        return result.toString();
    }

    private Integer getPasswordLength() {
        return MIN_LENGTH + (int) (Math.random() * ((MAX_LENGTH - MIN_LENGTH) + 1));
    }

    @Override
    public String getHashEmail(byte[] inputBytes) {
        MessageDigest messageDigest;
        byte[] digestBytes;
        try {
            messageDigest = MessageDigest.getInstance(ALGORITHM);
            messageDigest.update(inputBytes);
            digestBytes = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return DatatypeConverter.printHexBinary(digestBytes).toLowerCase();
    }

    @Override
    public UserDTO currentUser() {
        UserContext context = (UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = context.getUser();
        return userConverter.convert(user);
    }


    @Override
    public Long currentUserId() {
        UserContext context = (UserContext) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = context.getUser();
        return user.getId();
    }

    @Bean
    private BCryptPasswordEncoder createPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public String getEncodedPassword(String password) {
        return encoder.encode(password);
    }

}
