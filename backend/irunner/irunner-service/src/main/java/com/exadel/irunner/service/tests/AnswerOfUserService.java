package com.exadel.irunner.service.tests;

import com.exadel.irunner.model.dto.tests.AnswerMarkDTO;
import com.exadel.irunner.model.dto.tests.AnswerOfUserDTO;
import com.exadel.irunner.model.dto.tests.AnswersForTestDTO;
import com.exadel.irunner.model.dto.tests.MarkDTO;

import java.util.List;

public interface AnswerOfUserService {

    void save(AnswersForTestDTO answers);

    List<AnswerOfUserDTO> getAnswersToCheck(Long id);

    void markTest(MarkDTO mark);

}
