package com.exadel.irunner.service.user;

import com.exadel.irunner.model.dto.TeacherDTO;

public interface TeacherService {
    TeacherDTO registerNewTeacherAccount(TeacherDTO teacher);
}
