package com.exadel.irunner.service.user.impl;

import com.exadel.irunner.dao.repository.user.StudentDAO;
import com.exadel.irunner.dao.repository.user.TeacherDAO;
import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.dto.GeneralUserDTO;
import com.exadel.irunner.model.dto.UserDTO;
import com.exadel.irunner.model.dto.UserPasswordDTO;
import com.exadel.irunner.model.entity.*;
import com.exadel.irunner.model.enums.Role;
import com.exadel.irunner.model.exception.EmailExistsException;
import com.exadel.irunner.model.exception.EmailNotValidException;
import com.exadel.irunner.model.exception.NoUnconfirmedTeachersException;
import com.exadel.irunner.model.exception.PasswordNotMatchesException;
import com.exadel.irunner.service.converter.UserConverter;
import com.exadel.irunner.service.mail.interfaces.EmailService;
import com.exadel.irunner.service.mail.interfaces.MessageBuilder;
import com.exadel.irunner.service.user.SecurityService;
import com.exadel.irunner.service.user.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type User service.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final String ACCOUNT_ALREADY_EXIST_TEMPLATE = "There is an account with that email address: %s";
    private static final String EMAIL_PATTERN = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
    private static final String EMAIL_NOT_VALID_TEMPLATE = "The email address: %s is not valid";
    private static final String ACTIVATION_ACCOUNT_TEMPLATE = "Hello! You've been sucessfully registered in " +
            "our system!\nUse the data below to make a login:\nLogin: %s\nPassword: %s\nPlease, activate " +
            "your account using the link below.\nLink: %s/api/users/%d/enable/%s";
    private static final String ACTIVATION_TEMPLATE = "Activation";
    private static final String PASSWORD_NOT_MATCH_TEMPLATE = "Current password doesn't match encoded password";
    private static final String NO_UNCONFIRMED_TEACHERS_ERROR_MESSAGE = "There are no unconfirmed teachers in the system";
    private static final boolean CONFIRMED = true;

    @Value("${application.url}")
    private String url;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private EmailService emailService;

    @Autowired
    private MessageBuilder messageBuilder;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private TeacherDAO teacherDAO;

    @Override
    public UserDTO find(Long userId) {
        User user = userDAO.findById(userId).get();
        return userConverter.convert(user);
    }

    @Override
    public UserDTO create(UserDTO userDTO) {
        User user = userConverter.convert(userDTO);
        userDAO.save(user);
        return userDTO;
    }

    @Override
    public List<UserDTO> findAll() {
        List<User> users = new ArrayList<>();
        userDAO.findAll().forEach(users::add);
        return userConverter.convert(users);
    }

    @Override
    public void delete(Long userID) {
        userDAO.delete(userDAO.findById(userID).get());
    }

    @Override
    public void enableUser(Long id) {
        userDAO.enableUser(id);
    }

    @Override
    public UserDTO registerNewUserAccount(UserDTO userDTO) {
        String userEmail = userDTO.getEmail();
        validateEmail(userEmail);
        User user = userConverter.convert(userDTO);
        User newUser = saveNewUser(user);
        return userConverter.convert(newUser);
    }

    @Override
    public void sendMessageToUser(User user, String userPassword) {
        String[] recipients = {user.getEmail()};
        MimeMessage mimeMessage = emailService.getMimeMessage();
        Long userId = user.getId();
        String userEmail = user.getEmail();
        byte[] bytesOfEmail = userEmail.getBytes();
        String hashEmail = securityService.getHashEmail(bytesOfEmail);
        MimeMessage message = messageBuilder.buildMessage(mimeMessage, recipients, ACTIVATION_TEMPLATE,
                String.format(ACTIVATION_ACCOUNT_TEMPLATE, userEmail, userPassword, url, userId, hashEmail));
        emailService.sendMessage(message);
    }

    @Override
    public void assignRoleToUser(UserDTO user) {
        Role newRole = user.getRole();
        Long userId = user.getId();
        userDAO.assignRoleToUser(newRole, userId);
    }

    private User saveNewUser(User user) {
        return userDAO.save(user);
    }

    private boolean emailExist(String email) {
        boolean exists = false;
        User user = userDAO.findByEmail(email);
        if (user != null) {
            exists = true;
        }
        return exists;
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void changePassword(Long userId, UserPasswordDTO dto) {
        Optional<User> optionalUser = userDAO.findById(userId);
        User user = optionalUser.get();
        String currentPassword = dto.getCurrentPassword();
        String encryptedPassword = user.getPassword();
        String newPassword = dto.getNewPassword();
        if (passwordMatches(currentPassword, encryptedPassword)) {
            String newEncodedPassword = securityService.getEncodedPassword(newPassword);
            userDAO.changePassword(userId, newEncodedPassword);
        } else {
            throw new PasswordNotMatchesException(PASSWORD_NOT_MATCH_TEMPLATE);
        }
    }

    @Override
    public boolean passwordMatches(String password, String bCryptPassword) {
        return BCrypt.checkpw(password, bCryptPassword);
    }

    @Override
    public List<UserDTO> getUnconfirmedTeachers() {
        List<User> unconfirmedTeachers = userDAO.getUnconfirmedUsers(Role.ROLE_TEACHER);
        if (CollectionUtils.isEmpty(unconfirmedTeachers)) {
            throw new NoUnconfirmedTeachersException(NO_UNCONFIRMED_TEACHERS_ERROR_MESSAGE);
        }
        return userConverter.convert(unconfirmedTeachers);
    }

    @Override
    public void validateEmail(String email) {
        if (!isValidEmail(email)) {
            throw new EmailNotValidException(String.format(EMAIL_NOT_VALID_TEMPLATE, email));
        }
        if (emailExist(email)) {
            throw new EmailExistsException(String.format(ACCOUNT_ALREADY_EXIST_TEMPLATE, email));
        }
    }

    @Override
    public void confirmTeacher(UserDTO user) {
        Long teacherId = user.getId();
        boolean status = user.isStatus();
        if(status){
            userDAO.confirmTeacher(teacherId, CONFIRMED);
        } else {
            Teacher unconfirmedTeacher = teacherDAO.getTeacherByUserId(teacherId);
            Student newStudent = makeNewStudent(unconfirmedTeacher);
            teacherDAO.delete(unconfirmedTeacher);
            studentDAO.save(newStudent);
        }
    }

    private Student makeNewStudent(Teacher unconfirmedTeacher){
        Student newStudent = new Student();
        User newUser = unconfirmedTeacher.getUser();
        newUser.setStatus(CONFIRMED);
        newUser.setRole(Role.ROLE_STUDENT);
        newStudent.setUser(newUser);
        University university = unconfirmedTeacher.getUniversity();
        newStudent.setUniversity(university);
        String primarySkill = unconfirmedTeacher.getPrimarySkill();
        newStudent.setPrimarySkill(primarySkill);
        return newStudent;
    }

    @Override
    public GeneralUserDTO getUserInformation(Long userId) {
        UserDTO user = find(userId);
        Role userRole = user.getRole();
        GeneralUserDTO generalUser = null;
        if (userRole == Role.ROLE_STUDENT) {
            Student student = studentDAO.findStudentById(userId);
            generalUser = getGeneralUserDTO(student);
        } else if (userRole == Role.ROLE_TEACHER || userRole == Role.ROLE_ADMIN) {
            Teacher teacher = teacherDAO.getTeacherByUserId(userId);
            generalUser = getGeneralUserDTO(teacher);
        }
        return generalUser;
    }

    private GeneralUserDTO getGeneralUserDTO(Student student) {
        GeneralUserDTO generalUserDTO = new GeneralUserDTO();
        String firstName = student.getUser().getFirstName();
        generalUserDTO.setFirstName(firstName);
        String lastName = student.getUser().getLastName();
        generalUserDTO.setLastName(lastName);
        String email = student.getUser().getEmail();
        generalUserDTO.setEmail(email);
        String university = student.getUniversity().getName();
        generalUserDTO.setUniversity(university);
        Faculty facultyObject = student.getFaculty();
        String faculty;
        if(facultyObject == null){
            faculty = "Не указано";
        } else {
            faculty = facultyObject.getName();
        }
        generalUserDTO.setFaculty(faculty);
        Integer graduationYear = student.getGraduationYear();
        if(graduationYear == null){
            graduationYear = 2019;
        }
        generalUserDTO.setGraduationYear(graduationYear);
        String primarySkill = student.getPrimarySkill();
        generalUserDTO.setPrimarySkill(primarySkill);
        Role role = student.getUser().getRole();
        generalUserDTO.setRole(role);
        return generalUserDTO;
    }

    private GeneralUserDTO getGeneralUserDTO(Teacher teacher) {
        GeneralUserDTO generalUserDTO = new GeneralUserDTO();
        String firstName = teacher.getUser().getFirstName();
        generalUserDTO.setFirstName(firstName);
        String lastName = teacher.getUser().getLastName();
        generalUserDTO.setLastName(lastName);
        String email = teacher.getUser().getEmail();
        generalUserDTO.setEmail(email);
        String university = teacher.getUniversity().getName();
        generalUserDTO.setUniversity(university);
        String primarySkill = teacher.getPrimarySkill();
        generalUserDTO.setPrimarySkill(primarySkill);
        Role role = teacher.getUser().getRole();
        generalUserDTO.setRole(role);
        return generalUserDTO;
    }

}