package com.exadel.irunner.service.user;

import com.exadel.irunner.model.dto.UserDTO;

public interface SecurityService {
    String generatePassword();
    String getHashEmail(byte[] inputBytes);
    UserDTO currentUser();
    String getEncodedPassword(String password);
    Long currentUserId();
}
