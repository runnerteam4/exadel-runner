package com.exadel.irunner.service.tests;

import com.exadel.irunner.dao.repository.tests.answer.AnswerDAO;
import com.exadel.irunner.dao.repository.tests.question.QuestionDAO;
import com.exadel.irunner.dao.repository.tests.tag.TagDAO;
import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.dto.tests.AnswerDTO;
import com.exadel.irunner.model.dto.tests.QuestionDTO;
import com.exadel.irunner.model.entity.tests.Answer;
import com.exadel.irunner.model.entity.tests.Question;
import com.exadel.irunner.model.entity.tests.Tag;
import com.exadel.irunner.service.converter.AnswerConverter;
import com.exadel.irunner.service.converter.QuestionConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionDAO questionDAO;

    @Autowired
    private AnswerDAO answerDAO;

    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private QuestionConverter questionConverter;

    @Autowired
    private AnswerConverter answerConverter;

    @Override
    public QuestionDTO findById(Long id) {
        Optional<Question> question = questionDAO.findById(id);
        return questionConverter.convert(question.get());
    }

    @Override
    public List<QuestionDTO> findByType(boolean type) {
        return questionConverter.convert(questionDAO.findByType(type));
    }

    @Override
    public List<QuestionDTO> findTrainingQuestions() {
        return questionConverter.convert(questionDAO.findTrainingQuestions());
    }

    @Override
    public List<QuestionDTO> findByTag(String tag) {
        return questionConverter.convert(questionDAO.findByTag(tag));
    }


    @Override
    public QuestionDTO create(QuestionDTO questionDTO) {
        Question question = questionConverter.convert(questionDTO);

        List<Tag> tags = tagDAO.findByName(questionDTO.getTags());
        List<String> list = tags.stream().map(Tag::getName).collect(Collectors.toList());
        List<String> finalList = list;
        list = questionDTO.getTags().stream().filter(tag->!finalList.contains(tag)).collect(Collectors.toList());
        list.forEach(name->{
            Tag tag = new Tag();
            tag.setName(name);
            tag = tagDAO.save(tag);
            tags.add(tag);
        });
        question.setTags(tags);

        questionDAO.save(question);
        if(questionDTO.getAnswers() != null) {
            for (AnswerDTO answer : questionDTO.getAnswers()) {
                Answer savedAnswer = answerConverter.convert(answer);
                savedAnswer.setQuestion(question);
                answerDAO.save(savedAnswer);
            }
        }
        return questionDTO;
    }

    @Override
    public void delete(Long id) {
        questionDAO.deleteById(id);
    }

    @Override
    public List<QuestionDTO> findAll() {
        return questionConverter.convert((List<Question>)questionDAO.findAll());
    }

}
