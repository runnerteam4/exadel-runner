package com.exadel.irunner.service.wiki;

import com.exadel.irunner.model.dto.CategoryDTO;
import com.exadel.irunner.model.dto.WikiDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.Wiki;

import java.util.List;

public interface WikiService {
    WikiDTO findById(Long id);

    List<WikiDTO> findByCategoryId(Long id);
    CategoryDTO createCategory(String name);
    WikiDTO create(WikiDTO wiki);
    void delete(Long id);
    List<WikiDTO> findAll();
    List<CategoryDTO> getCategories();
    List<WikiDTO> getWikiByCategoriesIds(List<Long> categoryIds);
    WikiDTO findByName(String name);
}
