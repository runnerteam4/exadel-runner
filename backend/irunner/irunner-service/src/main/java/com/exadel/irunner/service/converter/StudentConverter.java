package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.FacultyDTO;
import com.exadel.irunner.model.dto.StudentDTO;
import com.exadel.irunner.model.dto.UniversityDTO;
import com.exadel.irunner.model.dto.UserDTO;
import com.exadel.irunner.model.entity.Faculty;
import com.exadel.irunner.model.entity.Student;
import com.exadel.irunner.model.entity.University;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentConverter extends BaseConverter<Student,StudentDTO> {

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private UniversityConverter universityConverter;

    @Autowired
    private FacultyConverter facultyConverter;

    @Override
    public StudentDTO convert(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(student.getId());
        UserDTO userDTO = userConverter.convert(student.getUser());
        studentDTO.setUser(userDTO);
        UniversityDTO universityDTO = universityConverter.convert(student.getUniversity());
        studentDTO.setUniversity(universityDTO);
        FacultyDTO facultyDTO = facultyConverter.convert(student.getFaculty());
        studentDTO.setFaculty(facultyDTO);
        studentDTO.setGraduationYear(student.getGraduationYear());
        studentDTO.setPrimarySkill(student.getPrimarySkill());
        return studentDTO;
    }

    @Override
    public Student convert(StudentDTO studentDTO) {
        Student student = new Student();
        student.setId(studentDTO.getId());
        User user = userConverter.convert(studentDTO.getUser());
        student.setUser(user);
        University university = universityConverter.convert(studentDTO.getUniversity());
        student.setUniversity(university);
        Faculty faculty = facultyConverter.convert(studentDTO.getFaculty());
        student.setFaculty(faculty);
        student.setGraduationYear(studentDTO.getGraduationYear());
        student.setPrimarySkill(studentDTO.getPrimarySkill());
        return student;
    }
}
