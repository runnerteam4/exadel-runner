package com.exadel.irunner.service.group;

import com.exadel.irunner.model.dto.GroupDTO;
import com.exadel.irunner.model.dto.GroupStudentsDTO;
import com.exadel.irunner.model.dto.UserGroupDTO;

import java.util.List;

public interface GroupService {
    GroupDTO addGroup(GroupDTO group);
    GroupDTO convertToDTO(GroupStudentsDTO dto);
    GroupDTO getGroupById(Long id);
    List<GroupDTO> getTeacherGroups(Long teacherId);
    void changeGroupName(GroupDTO group);
    void deleteGroup(Long groupId);
    void excludeStudentFromGroup(UserGroupDTO userGroup);
    void addStudentsToGroup(GroupStudentsDTO dto);
    void assignTeacherToGroup(UserGroupDTO dto);
}
