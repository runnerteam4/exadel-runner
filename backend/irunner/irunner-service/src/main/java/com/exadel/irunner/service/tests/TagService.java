package com.exadel.irunner.service.tests;

import com.exadel.irunner.model.dto.tests.TagDTO;

import java.util.List;

public interface TagService {
    List<TagDTO> findAll();

    TagDTO findById(Long id);

    List<TagDTO> findByNames(List<String> name);

    TagDTO create(TagDTO tagDTO);

    void delete(Long id);
}
