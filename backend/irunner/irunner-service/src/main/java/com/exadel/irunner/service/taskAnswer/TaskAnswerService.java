package com.exadel.irunner.service.taskAnswer;

import com.exadel.irunner.model.dto.TaskAnswerDTO;
import com.exadel.irunner.model.entity.TaskAnswer;

import java.io.File;
import java.util.List;

public interface TaskAnswerService {
    TaskAnswerDTO findById(Long id);

    List<TaskAnswerDTO> create(List<TaskAnswerDTO> taskDTO, Long taskId);

    void delete(Long id);

    List<TaskAnswerDTO> findAll();

}