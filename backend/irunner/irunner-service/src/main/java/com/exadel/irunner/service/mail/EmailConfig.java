package com.exadel.irunner.service.mail;


import com.exadel.irunner.service.mail.interfaces.EmailService;
import com.exadel.irunner.service.mail.interfaces.MessageBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


@Configuration
public class EmailConfig {

    @Bean
    public EmailService emailService() {
        return new EmailServiceImpl();
    }

    @Bean
    public JavaMailSenderImpl mailSender() throws IOException {
        Properties properties = new Properties();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("mail.properties");
        properties.load(stream);

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(properties.getProperty("mailsender.host"));
        mailSender.setPort(Integer.parseInt(properties.getProperty("mailsender.port")));

        mailSender.setUsername(properties.getProperty("mailsender.user1"));
        mailSender.setPassword(properties.getProperty("mailsender.password1"));

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", properties.getProperty("mail.smtp.auth"));
        props.put("mail.smtp.starttls.enable", properties.getProperty("mail.smtp.starttls.enable"));
        props.put("mail.smtp.ssl.trust", properties.getProperty("mail.smtp.ssl.trust"));
        return mailSender;
    }


    @Bean
    public MessageBuilder messageBuilder() {
        return new MessageBuilderImpl();
    }

}
