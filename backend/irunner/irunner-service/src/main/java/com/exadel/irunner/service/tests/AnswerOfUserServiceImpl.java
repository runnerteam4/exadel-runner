package com.exadel.irunner.service.tests;

import com.exadel.irunner.dao.repository.tests.answer.AnswerDAO;
import com.exadel.irunner.dao.repository.tests.answer.AnswerOfUserDAO;
import com.exadel.irunner.dao.repository.tests.answer.AnswersForTestDAO;
import com.exadel.irunner.dao.repository.tests.question.QuestionDAO;
import com.exadel.irunner.dao.repository.tests.test.TestDAO;
import com.exadel.irunner.model.dto.tests.AnswerMarkDTO;
import com.exadel.irunner.model.dto.tests.AnswerOfUserDTO;
import com.exadel.irunner.model.dto.tests.AnswersForTestDTO;
import com.exadel.irunner.model.dto.tests.MarkDTO;
import com.exadel.irunner.model.entity.tests.AnswerOfUser;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import com.exadel.irunner.model.entity.tests.Question;
import com.exadel.irunner.model.entity.tests.Test;
import com.exadel.irunner.model.enums.TestStatus;
import com.exadel.irunner.model.enums.Type;
import com.exadel.irunner.service.converter.AnswerOfUserConverter;
import com.exadel.irunner.service.converter.AnswersForTestConverter;
import com.exadel.irunner.service.mail.interfaces.EmailService;
import com.exadel.irunner.service.mail.interfaces.MessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@Transactional
public class AnswerOfUserServiceImpl implements AnswerOfUserService {

    @Autowired
    private EmailService emailService;

    @Autowired
    private MessageBuilder messageBuilder;

    @Autowired
    private AnswersForTestDAO answersForTestDAO;

    @Autowired
    private AnswerOfUserDAO answerOfUserDAO;

    @Autowired
    private QuestionDAO questionDAO;

    @Autowired
    private AnswerDAO answerDAO;

    @Autowired
    private TestDAO testDAO;

    @Autowired
    private AnswersForTestConverter answersForTestConverter;

    @Autowired
    private AnswerOfUserConverter answerOfUserConverter;

    @Override
    public void save(AnswersForTestDTO answers) {
        AnswersForTest answersForTest = answersForTestDAO.findByIds(answers.getUserId(), answers.getTestId());

        answersForTest.setAnswers(answerOfUserConverter.reverseConvert(answers.getAnswers()));
        answersForTest.setStatus(TestStatus.FINISHED);
        answersForTest.setEndTime(LocalDateTime.now());

        for (AnswerOfUserDTO answer : answers.getAnswers()) {
            AnswerOfUser answerOfUser = answerOfUserConverter.convert(answer);
            answerOfUser.setAnswerList(answersForTest);
            switch (answerOfUser.getQuestion().getType()) {
                case LIST_ONE_ANSWER:
                    if (answerDAO.findById(Long.parseLong(answer.getAnswer())).get().isCorrect()) {
                        answerOfUser.setCorrect(true);
                        questionDAO.incrementRightAnswersNum(answer.getQuestionId());
                    }
                    break;
                case LIST_MULTIPLE_ANSWER:
                    String[] ids = answer.getAnswer().split(" ");
                    List<Long> rightAnswersIds = answerDAO.rightAnswersIds(answer.getQuestionId());
                    if (ids.length == rightAnswersIds.size()) {
                        answerOfUser.setCorrect(true);
                        for (String id : ids) {
                            if (!rightAnswersIds.contains(Long.parseLong(id))) {
                                answerOfUser.setCorrect(false);
                                break;
                            }
                        }
                        if(answerOfUser.getCorrect()){
                            questionDAO.incrementRightAnswersNum(answer.getQuestionId());
                        }
                    }
                    break;
                case SHORT_ANSWER:
                    if (questionDAO.findById(answer.getQuestionId()).get().getAnswers().get(0).getText().equals(answer.getAnswer())) {
                        answerOfUser.setCorrect(true);
                        questionDAO.incrementRightAnswersNum(answer.getQuestionId());
                    }
                    break;
            }
            answerOfUserDAO.save(answerOfUser);
        }


        List<Type> types = answersForTest.getAnswers().stream().map(answer->answer.getQuestion().getType()).collect(Collectors.toList());
        if(!types.contains(Type.LONG_ANSWER)){
            putMarkById(answersForTest.getId());
            answersForTest.setStatus(TestStatus.CHECKED);
        }

    }

    @Override
    public List<AnswerOfUserDTO> getAnswersToCheck(Long id) {
        Optional<AnswersForTest> answersForTest = answersForTestDAO.findById(id);
        List<AnswerOfUserDTO> list = answerOfUserConverter.convert(
                answersForTest.get().getAnswers().stream()
                        .filter(answer->answer.getQuestion().getType().equals(Type.LONG_ANSWER)).collect(Collectors.toList()));
        list.forEach(answer -> answer.setQuestionText(questionDAO.findById(answer.getQuestionId()).get().getText()));
        return list;
    }

    @Override
    public  void markTest(MarkDTO mark){
        Optional<AnswersForTest> answersForTest = answersForTestDAO.findById(mark.getTestId());
        answersForTest.get().setStatus(TestStatus.CHECKED);
        answersForTest.get().setComment(mark.getComment());
        AtomicInteger markForAnswer = new AtomicInteger(0);

        answersForTest.get().getAnswers().forEach(answer->{
            if(answer.getCorrect() && !answer.getQuestion().getType().equals(Type.LONG_ANSWER)) {
                markForAnswer.updateAndGet(v -> v + answer.getQuestion().getComplexity() * 4 / answer.getAnswerList().getTest().getQuestionAmount()*10);
            }
        });

        mark.getMarks().forEach(m->{
            Optional<AnswerOfUser> answerOfUser = answerOfUserDAO.findById(m.getAnswerId());
            if(m.getMark() >= 4) {
                answerOfUser.get().setCorrect(true);
            }
            markForAnswer.updateAndGet(v -> v + answerOfUser.get().getQuestion().getComplexity() * 4 * m.getMark() / answerOfUser.get().getAnswerList().getTest().getQuestionAmount());
        });

        if(markForAnswer.get() < 4){
            AtomicBoolean b = new AtomicBoolean(true);
            answersForTest.get().getAnswers().forEach(answer->{
                if(answer.getQuestion().getComplexity() == 1 && !answer.getCorrect())
                    b.set(false);
            });
            if(b.get()){
                answersForTest.get().setMark(4);
                return;
            }
        }
        answersForTest.get().setMark( Math.round((float)markForAnswer.get()/10));

        String[] recipient = {answersForTest.get().getStudent().getEmail()};
        MimeMessage message = emailService.getMimeMessage();
        emailService.sendMessage(messageBuilder.buildMessage(message, recipient, "JDLine",
                "Тест " + answersForTest.get().getTest().getName() + " проверен. Вы можете узнать результат в разделе 'Проверенные тесты'."));

    }

    private void putMarkById(Long id) {
        Optional<AnswersForTest> answersForTest = answersForTestDAO.findById(id);
        List<AnswerOfUser> answers = answerOfUserDAO.findByTestId(id);

        AtomicInteger mark = new AtomicInteger(0);
        answers.forEach(answer->{
            if(answer.getCorrect()) {
                mark.updateAndGet(v -> v + answer.getQuestion().getComplexity() * 4 / answer.getAnswerList().getTest().getQuestionAmount()*10);
            }
        });

        if(mark.get() < 4){
            AtomicBoolean b = new AtomicBoolean(true);
            if(answers.size() != 0) {
                answers.forEach(answer -> {
                    if (answer.getQuestion().getComplexity() == 1 && !answer.getCorrect())
                        b.set(false);
                });
            }
            else{
                b.set(false);
            }
            if(b.get()){
                answersForTest.get().setMark(4);
                return;
            }
        }
        answersForTest.get().setMark(Math.round((float)mark.get()/10));
        String[] recipient = {answersForTest.get().getStudent().getEmail()};
        MimeMessage message = emailService.getMimeMessage();
        emailService.sendMessage(messageBuilder.buildMessage(message, recipient, "JDLine",
                "Тест " + answersForTest.get().getTest().getName() + " проверен. Вы можете узнать результат в разделе 'Проверенные тесты'."));
    }
}
