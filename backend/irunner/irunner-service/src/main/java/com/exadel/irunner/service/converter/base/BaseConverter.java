package com.exadel.irunner.service.converter.base;


import com.exadel.irunner.model.dto.BaseEntityDTO;
import com.exadel.irunner.model.entity.BaseEntity;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseConverter<TO extends BaseEntity, DTO extends BaseEntityDTO> implements Converter<TO,DTO>{

    public List<DTO> convert(Collection<TO> tos) {
        if(CollectionUtils.isNotEmpty(tos)) {
            return tos.stream().map(this::convert).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<TO> reverseConvert(Collection<DTO> tos) {
        if(CollectionUtils.isNotEmpty(tos)) {
            return tos.stream().map(this::convert).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
