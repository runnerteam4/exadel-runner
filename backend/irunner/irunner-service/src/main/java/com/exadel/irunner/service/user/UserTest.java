package com.exadel.irunner.service.user;

import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;

public class UserTest {

    @Autowired
    private UserDAO userDAO;

    public void setUser(){//it works
        User user = new User();
        user.setFirstName("MAX");
        user.setLastName("buhay?");
        user.setPassword("1234567890");
        user.setEmail("max@mail.com");
        user.setId(Long.valueOf(3));
        userDAO.save(user);
    }
}
