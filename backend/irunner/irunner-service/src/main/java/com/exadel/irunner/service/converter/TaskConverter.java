package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.wiki.CategoryDAO;
import com.exadel.irunner.model.dto.CategoryDTO;
import com.exadel.irunner.model.dto.TaskDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.Task;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskConverter extends BaseConverter<Task, TaskDTO> {

    @Autowired
    CategoryDAO categoryDAO;

    @Autowired
    CategoryConverter categoryConverter;

    @Override
    public TaskDTO convert(Task task) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setSolutionCount(task.getSolutionCount());
        taskDTO.setSuccessSolutionCount(task.getCorrectSolutionCount());
        taskDTO.setName(task.getName());
        taskDTO.setCondition(task.getCondition());
        CategoryDTO categoryDTO = categoryConverter.convert(task.getCategory());
        taskDTO.setCategoryId(categoryDTO.getId());
        return taskDTO;
    }

    @Override
    public Task convert(TaskDTO taskDTO) {
        Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setCondition(taskDTO.getCondition());
        Category category = categoryDAO.findById(taskDTO.getCategoryId()).get();
        if(category.getName()!=null) {
            task.setCategory(category);
        }
        else{
            task.setCategory(new Category());
        }
        return task;
    }
}
