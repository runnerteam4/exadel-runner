package com.exadel.irunner.service.task;

import com.exadel.irunner.compiler.testers.JavaTester;
import com.exadel.irunner.dao.repository.group.GroupDAO;
import com.exadel.irunner.dao.repository.task.StudentTaskDAO;
import com.exadel.irunner.dao.repository.task.TaskAnswerDAO;
import com.exadel.irunner.dao.repository.task.TaskDAO;
import com.exadel.irunner.dao.repository.task.TaskGroupDAO;
import com.exadel.irunner.dao.repository.user.StudentDAO;
import com.exadel.irunner.dao.repository.user.TeacherDAO;
import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.dao.repository.wiki.CategoryDAO;
import com.exadel.irunner.model.dto.*;
import com.exadel.irunner.model.entity.*;
import com.exadel.irunner.model.filters.TaskFilter;
import com.exadel.irunner.model.dto.TaskGroupDTO;
import com.exadel.irunner.service.converter.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

@Service
@Transactional
public class TaskServiceImpl implements TaskService{

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private TaskGroupConverter taskGroupConverter;

    @Autowired
    private GroupConverter groupConverter;

    @Autowired
    private StudentTaskConverter studentTaskConverter;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private StudentTaskDAO studentTaskDAO;

    @Autowired
    private TaskAnswerConverter taskAnswerConverter;

    @Autowired
    private CategoryConverter categoryConverter;

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private TaskConverter taskConverter;

    @Autowired
    private JavaTester javaTester;

    @Autowired
    private TaskAnswerDAO taskAnswerDAO;

    @Autowired
    private TaskGroupDAO taskGroupDAO;

    @Override
    public TaskDTO findById(Long id){
        Task task = taskDAO.findById(id).get();
        return taskConverter.convert(task);
    }

    @Override
    public TaskDTO create(TaskDTO taskDTO){
        Task task = taskConverter.convert(taskDTO);
        if(taskDAO.findByName(taskDTO.getName())==null) {
            taskDAO.save(task);
        }
        task = taskDAO.findByName(taskDTO.getName());
        return taskConverter.convert(task);
    }

    @Override
    public TaskGroup createGroupTask(TaskGroupDTO taskGroupDTO){
        TaskGroup taskGroup = taskGroupConverter.convert(taskGroupDTO);
        taskGroup.setTeacherId(teacherDAO.findByUserId(taskGroupDTO.getTeacherId()));
        taskGroupDAO.save(taskGroup);
        return taskGroup;
    }

    @Override
    public TaskGroupDTO assignTaskForGroup(TaskGroupDTO taskGroupDTO){
        if(taskGroupDAO.findTaskGroupByTaskIdAndGroupId(taskGroupDTO.getTaskId(),taskGroupDTO.getGroupId())==null) {
            TaskGroup taskGroup = createGroupTask(taskGroupDTO);
            Group group = groupDAO.findById(taskGroup.getGroupId()).get();
            List<User> users = new ArrayList<>(group.getUsers());
            List<Long> userIds = new ArrayList<>();
            users.forEach((item) -> {
                userIds.add(item.getId());
            });
            List<Student> students = studentDAO.students(userIds);
            for (Student student : students) {
                StudentTask studentTask = new StudentTask();
                studentTask.setTeacherId(taskGroup.getTeacherId());
                studentTask.setDeadline(taskGroup.getDeadline());
                studentTask.setTaskId(taskGroup.getTaskId());
                studentTask.setStudentId(student.getId());
                studentTaskDAO.save(studentTask);
            }
        }
        return taskGroupDTO;
    }

    @Override
    public List<StudentTaskDTO> getStudentTaskOfCurrentTeacher(Long userId){
       return  studentTaskConverter.convert(studentTaskDAO.findStudentTasksByTeacherId(teacherDAO.findByUserId(userId)));
    }
    @Override
    public List<StudentTaskDTO> getStudentTasks(Long userId){
        Long studentId = studentDAO.findByUserId(userId);
        List<StudentTask> studentTasks= studentTaskDAO.findStudentTasks(studentId);
        return studentTaskConverter.convert(studentTasks);
    }


    @Override
    public void delete(Long id){
        List<TaskAnswer> taskAnswers = taskAnswerDAO.findTaskAnswersByTaskId(id);
        List<TaskGroup> taskGroups = taskGroupDAO.findGrouptaskByTaskId(id);
        List<StudentTask> studentTasks = studentTaskDAO.findStudentTaskByTaskId(id);
        while(!taskAnswers.isEmpty()){
            taskAnswerDAO.deleteById(taskAnswers.get(0).getId());
            taskAnswers.remove(0);
        }
        while(!studentTasks.isEmpty()){
            studentTaskDAO.deleteById(studentTasks.get(0).getId());
            studentTasks.remove(0);
        }
        while(!taskGroups.isEmpty()){
            taskGroupDAO.deleteById(taskGroups.get(0).getId());
            taskGroups.remove(0);
        }
        taskDAO.deleteById(id);
    }

    @Override
    public List<TaskDTO> findAll(){
        List<Task> tasks = new ArrayList<>();
        taskDAO.findAll().forEach(tasks::add);
        return taskConverter.convert(tasks);
    }

    @Override
    public StudentTaskDTO getStudentTask(Long taskId, Long id){
       StudentTask studentTask = studentTaskDAO.findStudentTask(taskId,studentDAO.findByUserId(id));
       return studentTaskConverter.convert(studentTask);
    }

    @Override
    public TaskDTO findByName(String name){
        Task task = taskDAO.findByName(name);
        return taskConverter.convert(task);
    }



    @Override
    public List<TaskAnswerDTO> findInputAnswerByTaskId(Long id){
        return taskAnswerConverter.convert(taskAnswerDAO.findTaskAnswersByTaskId(id));
    }

    public TaskFilter compileTask(MultipartFile file, Long taskId, Long id) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException {
        Integer testCount = 0;
        Integer mark;
        TaskFilter taskFilter = new TaskFilter();
        StudentTask studentTask = studentTaskDAO.findById(taskId).get();
        Task task = taskDAO.findById(studentTask.getTaskId()).get();
        Long studentId = studentDAO.findByUserId(id);
        List<TaskAnswer> taskAnswers = taskAnswerConverter.reverseConvert(findInputAnswerByTaskId(task.getId()));
        for (TaskAnswer taskAnswer: taskAnswers) {
            String expected = taskAnswer.getAnswer();
            ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());
            String ans = javaTester.test(bis, file.getOriginalFilename(),taskAnswer);
            if(!expected.equals(ans)){
                taskFilter.setResult("Failed");
            }
            if(expected.equals(ans)) {
                testCount++;
                if(testCount>studentTask.getSuccess()) {
                    incrementCorrectStudentTask(studentId, task.getId());
                }
            }
        }
        taskFilter.setTestCount(testCount);
        mark = (100/taskAnswers.size()*testCount)/10;
        if(studentTask.getMark()==null||studentTask.getMark()<mark) {
            studentTaskDAO.changeMark(studentTask.getId(), mark);
        }
        if(testCount==taskAnswers.size()) {
            taskFilter.setResult("Complete");
            incrementCorrectTaskSolution(task.getId());
        }
        taskFilter.setTotal(taskAnswers.size());
        taskFilter.setMark(mark);
        incrementTaskSolution(task.getId());
        incrementStudentTaskAttempt(studentId,task.getId());
        return taskFilter;
    }

    @Override
    public StudentTaskDTO assignTaskForStudent(StudentTaskDTO studentTaskDTO){
        StudentTask studentTask = studentTaskConverter.convert(studentTaskDTO);
        studentTask.setStudentId(studentDAO.findByUserId(studentTaskDTO.getStudentId()));
        studentTask.setTeacherId(teacherDAO.findByUserId(studentTaskDTO.getTeacherId()));
        if(studentTaskDAO.findStudentTask(studentTaskDTO.getTaskId(),studentTask.getStudentId())==null) {
            studentTask.setSuccess(0);
            studentTask.setAttempt(0);
            studentTaskDAO.save(studentTask);
        }
        return studentTaskConverter.convert(studentTask);
    }

    @Override
    public List<CategoryDTO> getCategories() {
        List<CategoryDTO> categoriesDtos;
        List<Category> categories = new ArrayList<>();
        categoryDAO.findAll().forEach(categories::add);
        categoriesDtos = categoryConverter.convert(categories);
        return categoriesDtos;
    }

    private void incrementCorrectStudentTask(Long id, Long taskId){
        studentTaskDAO.incrementCorrectSolution(id,taskId);
    }

    private void incrementStudentTaskAttempt(Long id, Long taskId){
        studentTaskDAO.incrementAttempt(id,taskId);
    }

    @Override
    public void incrementTaskSolution(Long id){
        taskDAO.updateSolution(id);
    }

    @Override
    public void incrementCorrectTaskSolution(Long id){
        taskDAO.updateCorrectSolution(id);
    }
}
