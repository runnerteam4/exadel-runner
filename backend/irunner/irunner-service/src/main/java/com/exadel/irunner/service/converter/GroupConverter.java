package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.GroupDTO;
import com.exadel.irunner.model.dto.UserDTO;
import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class GroupConverter extends BaseConverter<Group,GroupDTO> {

    @Autowired
    private UserConverter userConverter;

    @Override
    public GroupDTO convert(Group group) {
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setId(group.getId());
        groupDTO.setName(group.getName());
        Set<User> users = group.getUsers();
        Set<UserDTO> userDTOS = getUserDTOSet(users);
        groupDTO.setUsers(userDTOS);
        return groupDTO;
    }

    @Override
    public Group convert(GroupDTO groupDTO) {
        Group group = new Group();
        group.setId(groupDTO.getId());
        group.setName(groupDTO.getName());
        Set<UserDTO> userDTOS = groupDTO.getUsers();
        Set<User> users = getUserSet(userDTOS);
        group.setUsers(users);
        return group;
    }

    private Set<UserDTO> getUserDTOSet(Set<User> users){
        Set<UserDTO> userDTOS = new HashSet<>();
        for(User user : users){
            UserDTO userDTO = userConverter.convert(user);
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    private Set<User> getUserSet(Set<UserDTO> userDTOS){
        Set<User> users = new HashSet<>();
        for(UserDTO userDTO : userDTOS){
            User user = userConverter.convert(userDTO);
            users.add(user);
        }
        return users;
    }
}
