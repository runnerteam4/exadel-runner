package com.exadel.irunner.service.tests;

import com.exadel.irunner.dao.repository.tests.tag.TagDAO;
import com.exadel.irunner.model.dto.tests.TagDTO;
import com.exadel.irunner.model.entity.tests.Tag;
import com.exadel.irunner.service.converter.TagConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    @Autowired
    private TagDAO tagDAO;

    @Autowired
    private TagConverter tagConverter;

    @Override
    public List<TagDTO> findAll() {
        List<Tag> tags = new ArrayList<>();
        tagDAO.findAll().forEach(tags::add);
        return tagConverter.convert(tags);
    }

    @Override
    public TagDTO findById(Long id) {
        Optional<Tag> tag = tagDAO.findById(id);
        return tagConverter.convert(tag.get());
    }

    @Override
    public List<TagDTO> findByNames(List<String> names) {
        List<Tag> tags = tagDAO.findByName(names);
        return tagConverter.convert(tags);
    }

    @Override
    public TagDTO create(TagDTO tagDTO) {
        Tag tag = tagConverter.convert(tagDTO);
        tagDAO.save(tag);
        return tagDTO;
    }

    @Override
    public void delete(Long id) {
        tagDAO.deleteById(id);
    }
}
