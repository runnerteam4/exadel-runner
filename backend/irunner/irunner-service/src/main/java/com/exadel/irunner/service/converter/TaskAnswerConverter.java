package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.task.TaskDAO;
import com.exadel.irunner.model.dto.TaskAnswerDTO;
import com.exadel.irunner.model.dto.TaskDTO;
import com.exadel.irunner.model.entity.Task;
import com.exadel.irunner.model.entity.TaskAnswer;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskAnswerConverter extends BaseConverter<TaskAnswer,TaskAnswerDTO> {

    @Autowired
   private TaskConverter taskConverter;

    @Autowired
    private TaskDAO taskDAO;

    @Override
    public TaskAnswerDTO convert(TaskAnswer taskAnswer) {
        TaskAnswerDTO taskAnswerDTO = new TaskAnswerDTO();
        taskAnswerDTO.setId(taskAnswer.getId());
        taskAnswerDTO.setInput(taskAnswer.getInput());
        taskAnswerDTO.setAnswer(taskAnswer.getAnswer());
        taskAnswerDTO.setTaskId(taskAnswer.getTask().getId());
        return taskAnswerDTO;
    }

    @Override
    public TaskAnswer convert(TaskAnswerDTO taskAnswerDTO) {
        TaskAnswer taskAnswer = new TaskAnswer();
        taskAnswer.setId(taskAnswerDTO.getId());
        taskAnswer.setInput(taskAnswerDTO.getInput());
        taskAnswer.setTask(taskDAO.findById(taskAnswerDTO.getTaskId()).get());
        taskAnswer.setAnswer(taskAnswerDTO.getAnswer());
        return taskAnswer;
    }
}
