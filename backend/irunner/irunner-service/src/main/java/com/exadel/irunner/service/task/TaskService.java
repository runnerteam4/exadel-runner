package com.exadel.irunner.service.task;

import com.exadel.irunner.model.dto.*;
import com.exadel.irunner.model.entity.TaskGroup;
import com.exadel.irunner.model.filters.StudentTaskFilter;
import com.exadel.irunner.model.filters.TaskFilter;
import com.exadel.irunner.model.dto.TaskGroupDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface TaskService {
    TaskDTO findById(Long id);
    TaskDTO create(TaskDTO taskDTO);
    void delete(Long id);
    List<TaskDTO> findAll();
    TaskGroup createGroupTask(TaskGroupDTO taskGroupDTO);
    List<StudentTaskDTO> getStudentTasks(Long userId);
    TaskGroupDTO assignTaskForGroup(TaskGroupDTO taskGroupDTO);
    List<StudentTaskDTO>  getStudentTaskOfCurrentTeacher(Long userId);
    TaskDTO findByName(String name);
    StudentTaskDTO assignTaskForStudent(StudentTaskDTO studentTaskDTO);
    StudentTaskDTO getStudentTask(Long taskId, Long id);
    List<CategoryDTO> getCategories();
    List<TaskAnswerDTO> findInputAnswerByTaskId(Long id);
    void incrementTaskSolution(Long id);
    void incrementCorrectTaskSolution(Long id);
    TaskFilter compileTask(MultipartFile file, Long taskId, Long id) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException;
}
