package com.exadel.irunner.service.wiki;

import com.exadel.irunner.dao.repository.wiki.CategoryDAO;
import com.exadel.irunner.dao.repository.wiki.WikiDAO;
import com.exadel.irunner.model.dto.CategoryDTO;
import com.exadel.irunner.model.dto.WikiDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.Wiki;
import com.exadel.irunner.service.converter.CategoryConverter;
import com.exadel.irunner.service.converter.WikiConverter;
import com.exadel.irunner.service.user.SecurityService;
import com.exadel.irunner.service.user.impl.SecurityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WikiServiceImpl implements WikiService {

    @Autowired
    private WikiDAO wikiDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private WikiConverter wikiConverter;

    @Autowired
    private CategoryConverter categoryConverter;

    @Autowired
    private SecurityService securityService;

    @Override
    public WikiDTO findById(Long id) {
        Wiki wiki = wikiDAO.findById(id).get();
        return wikiConverter.convert(wiki);
    }

    @Override
    public List<WikiDTO> findByCategoryId(Long id) {
        List<Wiki> wikis = wikiDAO.findByCategory(id);
        return wikiConverter.convert(wikis);
    }


    @Override
    public WikiDTO create(WikiDTO wikiDTO) {
        Wiki wiki = wikiConverter.convert(wikiDTO);
        Long id = securityService.currentUser().getId();
        wiki.setAuthorId(id);
        wikiDAO.save(wiki);
        return wikiConverter.convert(wiki);
    }

    @Override
    public CategoryDTO createCategory(String name) {
        Category categoryTemp = categoryDAO.findByName(name);
        if (categoryTemp == null) {
            Category category = new Category();
            category.setName(name);
            category = categoryDAO.save(category);
            return categoryConverter.convert(category);
        }
        return categoryConverter.convert(categoryTemp);
    }

    @Override
    public List<WikiDTO> findAll() {
        List<Wiki> wikis = new ArrayList<>();
        wikiDAO.findAll().forEach(wikis::add);
        return wikiConverter.convert(wikis);
    }

    @Override
    public List<CategoryDTO> getCategories() {
        List<CategoryDTO> categoriesDtos;
        List<Category> categories = new ArrayList<>();
        categoryDAO.findAll().forEach(categories::add);
        categoriesDtos = categoryConverter.convert(categories);
        return categoriesDtos;
    }

    @Override
    public void delete(Long id) {
        Wiki wiki = wikiConverter.convert(findById(id));
        wikiDAO.delete(wiki);
    }

    @Override
    public WikiDTO findByName(String name) {
        Wiki wiki = wikiDAO.findByName(name);
        return wikiConverter.convert(wiki);
    }

    @Override
    public List<WikiDTO> getWikiByCategoriesIds(List<Long> categoryIds) {
        List<Wiki> wikis = new ArrayList<>();
        wikiDAO.findByCategory(categoryIds).forEach(wikis::add);
        return wikiConverter.convert(wikis);
    }

}
