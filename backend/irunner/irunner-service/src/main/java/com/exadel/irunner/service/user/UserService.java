package com.exadel.irunner.service.user;

import com.exadel.irunner.model.dto.GeneralUserDTO;
import com.exadel.irunner.model.dto.StudentDTO;
import com.exadel.irunner.model.dto.UserDTO;
import com.exadel.irunner.model.dto.UserPasswordDTO;
import com.exadel.irunner.model.entity.User;

import java.util.List;

/**
 * The interface User service.
 */
public interface UserService {
    UserDTO find(Long userId);
    UserDTO create(UserDTO user);
    void delete(Long userID);
    List<UserDTO> findAll();
    UserDTO registerNewUserAccount(UserDTO user);
    void enableUser(Long id);
    void sendMessageToUser(User user, String userPassword);
    void assignRoleToUser(UserDTO user);
    void changePassword(Long userId, UserPasswordDTO dto);
    boolean passwordMatches(String password, String bCryptPassword);
    List<UserDTO> getUnconfirmedTeachers();
    void validateEmail(String email);
    void confirmTeacher(UserDTO user);
    GeneralUserDTO getUserInformation(Long userId);
}

