package com.exadel.irunner.service.tests;

import com.exadel.irunner.model.dto.tests.TopicDTO;

import java.util.List;

public interface TopicService {
    List<TopicDTO> findAll();

    TopicDTO findById(Long id);

    TopicDTO findByName(String name);

    TopicDTO create(TopicDTO topicDTO);

    void delete(Long id);
}
