package com.exadel.irunner.service.converter.base;

import com.exadel.irunner.model.entity.BaseEntity;

public interface Converter<TO extends BaseEntity,DTO> {
    DTO convert(TO to);
    TO convert(DTO dto);
}
