package com.exadel.irunner.service.tests;

import com.exadel.irunner.dao.repository.group.GroupDAO;
import com.exadel.irunner.dao.repository.tests.answer.AnswersForTestDAO;
import com.exadel.irunner.dao.repository.tests.question.QuestionDAO;
import com.exadel.irunner.dao.repository.tests.test.TestDAO;
import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.dto.tests.*;
import com.exadel.irunner.model.entity.BaseEntity;
import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.tests.*;
import com.exadel.irunner.model.enums.Role;
import com.exadel.irunner.model.enums.TestStatus;
import com.exadel.irunner.model.filters.TestCheckFilter;
import com.exadel.irunner.model.filters.TestFilter;
import com.exadel.irunner.service.converter.AnswersForTestConverter;
import com.exadel.irunner.service.converter.TestConverter;
import com.exadel.irunner.service.converter.UserTestConverter;
import com.exadel.irunner.service.mail.interfaces.EmailService;
import com.exadel.irunner.service.mail.interfaces.MessageBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Transactional
public class TestServiceImpl implements TestService {

    @Autowired
    private EmailService emailService;

    @Autowired
    private MessageBuilder messageBuilder;

    @Autowired
    private TestDAO testDAO;

    @Autowired
    private QuestionDAO questionDAO;

    @Autowired
    private TestConverter testConverter;

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AnswersForTestDAO answersForTestDAO;

    @Autowired
    private AnswersForTestConverter answersForTestConverter;

    @Autowired
    private UserTestConverter userTestConverter;

    @Override
    public TestDTO findById(Long id) {
        Optional<Test> test = testDAO.findById(id);
        return testConverter.convert(test.get());
    }

    @Override
    public void assignForStudent(TestDTO testDTO) {
        Test test = testConverter.convert(testDTO);
        Optional<User> student = userDAO.findById(testDTO.getStudentId());
        test.setUsers(Collections.singletonList(student.get()));
        testDAO.save(test);

        String[] recipient = {student.get().getEmail()};
        MimeMessage message = emailService.getMimeMessage();
        emailService.sendMessage(messageBuilder.buildMessage(message, recipient, "JDLine",
                "Тест " + testDTO.getName() + " назначен. Deadline: " + testDTO.getDeadline()));
    }

    @Override
    public void assignForGroup(TestDTO testDTO) {
        Optional<Group> group = groupDAO.findById(testDTO.getGroupId());
        Test test = testConverter.convert(testDTO);
        if(group.isPresent()) {
            test.setUsers(new ArrayList<>(group.get().getUsers().stream().filter(user -> user.getRole().equals(Role.ROLE_STUDENT)).collect(Collectors.toList())));
            test.setGroup(group.get());
        }
        testDAO.save(test);

        String[] recipients = (String[]) test.getUsers().stream().map(User::getEmail).toArray();
        MimeMessage message = emailService.getMimeMessage();
        emailService.sendMessage(messageBuilder.buildMessage(message, recipients, "JDLine",
                "Тест " + testDTO.getName() + " назначен. Deadline: " + testDTO.getDeadline()));
    }

    @Override
    public AnswersForTestDTO startTest(Long userId, Long testId) {
        AnswersForTest test = answersForTestDAO.findByIds(userId, testId);

        if (test.getStatus() == TestStatus.WAITING) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");
            LocalDate deadline = LocalDate.parse(test.getTest().getDeadline(), formatter);

            if (ChronoUnit.SECONDS.between(deadline.atStartOfDay(), LocalDateTime.now().plusHours(3)) > 0) {
                test.setStatus(TestStatus.FINISHED);
                test.setMark(0);
                test.setEndTime(LocalDateTime.now());
                test.setEndTime(LocalDateTime.now());
                test.setComment("Дедлайн просрочен");
            } else {

                List<Question> questions = (List<Question>) questionDAO.findAll();
                test.setStatus(TestStatus.IN_PROGRESS);

                if (test.getTest().isTraining()) {
                    questions = questions.stream().filter(Question::isTraining).collect(Collectors.toList());
                }

                questions = questions.stream().filter(q -> q.getTopic().getId().equals(test.getTest().getTopic().getId()))
                        .filter(q -> {
                            List<Tag> list = q.getTags();
                            list.retainAll(test.getTest().getTags());
                            return list.size() > 0;
                        })
                        .collect(Collectors.toList());

                Map<Integer, List<Question>> map = questions.stream().collect(Collectors.groupingBy(Question::getComplexity));

                List<Question> questionsToAdd = new ArrayList<>();
                map.values().forEach(list -> {
                    Collections.shuffle(list);
                    questionsToAdd.addAll(list.subList(0, test.getTest().getQuestionAmount() / 4));
                });

                questionsToAdd.forEach(q -> questionDAO.incrementAssignmentNum(q.getId()));
                test.setQuestions(questionsToAdd);
                test.setStartTime(LocalDateTime.now());
            }
        }
            AnswersForTestDTO answersForTestDTO = answersForTestConverter.convert(test);
            answersForTestDTO.setTime(test.getTest().getTime());
        return answersForTestDTO;
    }

    @Override
    public void delete(Long id) {
        testDAO.delete(testConverter.convert(findById(id)));
    }

    @Override
    public List<TestDTO> findAll() {
        return testConverter.convert((List<Test>) testDAO.findAll());
    }

    @Override
    public List<TestDTO> findByFilter(TestFilter testFilter){
        List<Test> test = answersForTestDAO.getByFilter(testFilter.getName(),
                testFilter.getType(),
                testFilter.getTopics(),
                testFilter.getUserId(),
                testFilter.getPage(),
                testFilter.getChunkSize());

        return testConverter.convert(test);
    }

    @Override
    public List<UserTestDTO> findPassedTests(Long id) {
        return userTestConverter.convert(answersForTestDAO.getPassedTests(id));
    }

    @Override
    public List<TestResultDTO> getResults(AnswersForTestDTO answersForTestDTO) {
       AnswersForTest answersForTest = answersForTestDAO.findByIds(answersForTestDTO.getUserId(), answersForTestDTO.getTestId());
       List<TestResultDTO> results = answersForTest.getQuestions().stream().map(question -> {
           TestResultDTO result = new TestResultDTO();
           result.setQuestion(question.getText());
           boolean correct = false;
           Optional<AnswerOfUser> ans = answersForTest.getAnswers().stream()
                   .filter(answer-> answer.getQuestion().getId().equals(question.getId()))
                   .findFirst();
           if(ans.isPresent()) {
               correct = ans.get().getCorrect();
           }
           result.setCorrect(correct);
           return result;
       }).collect(Collectors.toList());
       return results;
    }

    @Override
    public List<TestCheckDTO> getTestCheckList(TestCheckFilter testCheckFilter) {
        List<Group> groups = groupDAO.findByTeacherId(testCheckFilter.getTeacherId());
        List<User> users = new ArrayList<>();
        groups.forEach(group -> users.addAll(group.getUsers()));
        List<AnswersForTest> tests = answersForTestDAO.findByStudentId(users.stream().map(BaseEntity::getId).distinct().collect(Collectors.toList()));
        tests = tests.stream().filter(test->test.getStatus().equals(TestStatus.FINISHED)).collect(Collectors.toList());
        List<TestCheckDTO> testCheckDTO = tests.stream().map(test->{
            TestCheckDTO testCheck = new TestCheckDTO();
            testCheck.setEndDate(test.getEndTime());
            testCheck.setStudent(test.getStudent().getFirstName() + " " + test.getStudent().getLastName());
            testCheck.setTestId(test.getId());
            testCheck.setTest(test.getTest().getName());
            testCheck.setTraining(test.getTest().isTraining());
            if(test.getTest().getGroup() != null) {
                testCheck.setGroup(test.getTest().getGroup().getName());
            }
            return testCheck;
        }).collect(Collectors.toList());
        if(testCheckFilter.getGroup() != null) {
            testCheckDTO = testCheckDTO.stream().filter(test -> test.getGroup().contains(testCheckFilter.getGroup())).collect(Collectors.toList());
        }
        if(testCheckFilter.getStudentName() != null){
            testCheckDTO = testCheckDTO.stream().filter(test->test.getStudent().contains(testCheckFilter.getStudentName())).collect(Collectors.toList());
        }

        return testCheckDTO;
    }

    @Override
    public Long getTime(Long id) {
        Optional<AnswersForTest> answersForTest = answersForTestDAO.findById(id);
        LocalDateTime endTime = answersForTest.get().getStartTime().plusSeconds(answersForTest.get().getTest().getTime());
        long duration = Duration.between(LocalDateTime.now(), endTime).toMillis();
        return TimeUnit.MILLISECONDS.toSeconds(duration);
    }


}
