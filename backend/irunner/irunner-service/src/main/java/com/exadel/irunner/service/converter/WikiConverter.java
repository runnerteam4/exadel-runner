package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.dao.repository.wiki.CategoryDAO;
import com.exadel.irunner.model.dto.CategoryDTO;
import com.exadel.irunner.model.dto.WikiDTO;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.Wiki;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WikiConverter extends BaseConverter<Wiki,WikiDTO> {

    @Autowired
    private CategoryConverter categoryConverter;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    public WikiDTO convert(Wiki wiki) {
        WikiDTO wikiDTO = new WikiDTO();
        wikiDTO.setId(wiki.getId());
        wikiDTO.setName(wiki.getName());
        wikiDTO.setLink(wiki.getLink());
        wikiDTO.setAuthorId(wiki.getAuthorId());
        CategoryDTO categoryDTO = categoryConverter.convert(wiki.getCategory());
        wikiDTO.setCategoryId(categoryDTO.getId());
        User user = userDAO.findById(wiki.getAuthorId()).get();
        wikiDTO.setAuthorName(user.getFirstName()+" "+user.getLastName());
       return wikiDTO;
    }

    @Override
    public Wiki convert(WikiDTO wikiDTO) {
        Wiki wiki = new Wiki();
        wiki.setId(wikiDTO.getId());
        wiki.setName(wikiDTO.getName());
        wiki.setLink(wikiDTO.getLink());
        wiki.setAuthorId(wiki.getAuthorId());
        Category category = categoryDAO.findById(wikiDTO.getCategoryId()).get();
        if(category.getName()!=null) {
            wiki.setCategory(category);
        }
        else{
            wiki.setCategory(new Category());
        }
        return wiki;
    }
}
