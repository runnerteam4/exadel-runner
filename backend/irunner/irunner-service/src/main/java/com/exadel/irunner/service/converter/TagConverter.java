package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.tests.TagDTO;
import com.exadel.irunner.model.entity.tests.Tag;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;


@Component
public class TagConverter extends BaseConverter<Tag, TagDTO> {
    @Override
    public TagDTO convert(Tag tag) {
        TagDTO tagDTO = new TagDTO();
        tagDTO.setId(tag.getId());
        tagDTO.setName(tag.getName());
        return tagDTO;
    }

    @Override
    public Tag convert(TagDTO tagDTO) {
        Tag tag = new Tag();
        tag.setId(tagDTO.getId());
        tag.setName(tagDTO.getName());
        return tag;
    }
}
