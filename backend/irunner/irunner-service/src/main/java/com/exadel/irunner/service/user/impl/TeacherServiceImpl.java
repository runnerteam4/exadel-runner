package com.exadel.irunner.service.user.impl;

import com.exadel.irunner.dao.repository.user.TeacherDAO;
import com.exadel.irunner.model.dto.TeacherDTO;
import com.exadel.irunner.model.entity.Teacher;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.Role;
import com.exadel.irunner.service.converter.TeacherConverter;
import com.exadel.irunner.service.user.SecurityService;
import com.exadel.irunner.service.user.TeacherService;
import com.exadel.irunner.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private UserService userService;

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private TeacherConverter teacherConverter;

    @Autowired
    private SecurityService securityService;

    @Override
    public TeacherDTO registerNewTeacherAccount(TeacherDTO teacherDTO) {
        Teacher teacher = teacherConverter.convert(teacherDTO);
        User registeredUser = teacher.getUser();
        String email = registeredUser.getEmail();
        userService.validateEmail(email);
        registeredUser.setRole(Role.ROLE_TEACHER);
        String userPassword = securityService.generatePassword();
        String encodedPassword = securityService.getEncodedPassword(userPassword);
        registeredUser.setPassword(encodedPassword);
        teacherDAO.save(teacher);
        userService.sendMessageToUser(registeredUser, userPassword);
        return teacherConverter.convert(teacher);
    }

}
