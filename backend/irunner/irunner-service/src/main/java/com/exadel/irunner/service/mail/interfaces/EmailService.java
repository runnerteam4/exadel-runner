package com.exadel.irunner.service.mail.interfaces;

import javax.mail.internet.MimeMessage;

public interface EmailService {

    void sendMessage(MimeMessage message);

    MimeMessage getMimeMessage();
}
