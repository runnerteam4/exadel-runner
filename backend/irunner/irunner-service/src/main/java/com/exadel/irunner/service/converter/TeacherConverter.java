package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.TeacherDTO;
import com.exadel.irunner.model.dto.UniversityDTO;
import com.exadel.irunner.model.dto.UserDTO;
import com.exadel.irunner.model.entity.Teacher;
import com.exadel.irunner.model.entity.University;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeacherConverter extends BaseConverter<Teacher,TeacherDTO> {

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private UniversityConverter universityConverter;

    @Override
    public TeacherDTO convert(Teacher teacher) {
        TeacherDTO teacherDTO = new TeacherDTO();
        teacherDTO.setId(teacher.getId());
        UserDTO userDTO = userConverter.convert(teacher.getUser());
        teacherDTO.setUser(userDTO);
        UniversityDTO universityDTO = universityConverter.convert(teacher.getUniversity());
        teacherDTO.setUniversity(universityDTO);
        teacherDTO.setPrimarySkill(teacher.getPrimarySkill());
        return teacherDTO;
    }

    @Override
    public Teacher convert(TeacherDTO teacherDTO) {
        Teacher teacher = new Teacher();
        teacher.setId(teacherDTO.getId());
        User user = userConverter.convert(teacherDTO.getUser());
        teacher.setUser(user);
        University university = universityConverter.convert(teacherDTO.getUniversity());
        teacher.setUniversity(university);
        teacher.setPrimarySkill(teacherDTO.getPrimarySkill());
        return teacher;
    }
}
