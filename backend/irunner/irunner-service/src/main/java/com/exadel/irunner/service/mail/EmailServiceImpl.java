package com.exadel.irunner.service.mail;

import com.exadel.irunner.service.mail.interfaces.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Override
    public void sendMessage(MimeMessage message) {
        mailSender.send(message);
    }

    @Override
    public MimeMessage getMimeMessage(){
        return mailSender.createMimeMessage();
    }
}
