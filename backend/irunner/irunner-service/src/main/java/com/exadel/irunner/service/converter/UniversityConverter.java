package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.UniversityDTO;
import com.exadel.irunner.model.entity.University;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;

@Component
public class UniversityConverter extends BaseConverter<University,UniversityDTO> {
    @Override
    public UniversityDTO convert(University university) {
        UniversityDTO universityDTO = new UniversityDTO();
        universityDTO.setId(university.getId());
        universityDTO.setName(university.getName());
        return universityDTO;
    }

    @Override
    public University convert(UniversityDTO universityDTO) {
        University university = new University();
        university.setId(universityDTO.getId());
        university.setName(universityDTO.getName());
        return university;
    }
}
