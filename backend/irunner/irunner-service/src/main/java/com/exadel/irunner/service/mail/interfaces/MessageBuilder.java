package com.exadel.irunner.service.mail.interfaces;

import javax.mail.internet.MimeMessage;

public interface MessageBuilder {

    MimeMessage buildMessage(MimeMessage message, String[] recipients, String subject, String text);

    MimeMessage buildMessage(MimeMessage message, String[] recipients, String subject, String text, String[] paths);
}
