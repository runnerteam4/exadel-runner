package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.task.TaskAnswerDAO;
import com.exadel.irunner.dao.repository.task.TaskDAO;
import com.exadel.irunner.dao.repository.user.StudentDAO;
import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.dto.StudentTaskDTO;
import com.exadel.irunner.model.entity.Student;
import com.exadel.irunner.model.entity.StudentTask;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StudentTaskConverter extends BaseConverter<StudentTask, StudentTaskDTO> {

    @Autowired
    StudentDAO studentDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    TaskDAO taskDAO;

    @Autowired
    TaskAnswerDAO taskAnswerDAO;

    @Override
    public StudentTaskDTO convert(StudentTask studentTask) {
        StudentTaskDTO studentTaskDTO= new StudentTaskDTO();
        studentTaskDTO.setId(studentTask.getId());
        studentTaskDTO.setAttempt(studentTask.getAttempt());
        studentTaskDTO.setMark(studentTask.getMark());
        studentTaskDTO.setSuccess(studentTask.getSuccess());
        studentTaskDTO.setStudentId(studentTask.getStudentId());
        studentTaskDTO.setTaskId(studentTask.getTaskId());
        Student student = studentDAO.findById(studentTask.getStudentId()).get();
        studentTaskDTO.setStudentName(student.getUser().getFirstName()+" "+student.getUser().getLastName());
        studentTaskDTO.setTaskName(taskDAO.findById(studentTask.getTaskId()).get().getName());
        studentTaskDTO.setTeacherId(studentTask.getTeacherId());
        studentTaskDTO.setDeadline(studentTask.getDeadline());
        studentTaskDTO.setCondition(taskDAO.findById(studentTask.getTaskId()).get().getCondition());
        studentTaskDTO.setTotal(taskAnswerDAO.findTaskAnswersByTaskId(studentTask.getTaskId()).size());
        return studentTaskDTO;
    }

    @Override
    public StudentTask convert(StudentTaskDTO studentTaskDTO) {
        StudentTask studentTask = new StudentTask();
        studentTask.setId(studentTaskDTO.getId());
        studentTask.setAttempt(studentTaskDTO.getAttempt());
        studentTask.setMark(studentTaskDTO.getMark());
        studentTask.setStudentId(studentTaskDTO.getStudentId());
        studentTask.setTaskId(studentTaskDTO.getTaskId());
        studentTask.setSuccess(studentTaskDTO.getSuccess());
        studentTask.setDeadline(studentTaskDTO.getDeadline());
        studentTask.setTeacherId(studentTaskDTO.getTeacherId());
        return studentTask;
    }
}
