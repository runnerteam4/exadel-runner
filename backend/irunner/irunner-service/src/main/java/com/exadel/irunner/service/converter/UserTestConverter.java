package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.tests.UserTestDTO;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;

@Component
public class UserTestConverter extends BaseConverter<AnswersForTest, UserTestDTO> {
    @Override
    public UserTestDTO convert(AnswersForTest answersForTest) {
        UserTestDTO userTest = new UserTestDTO();
        userTest.setId(answersForTest.getId());
        userTest.setComment(answersForTest.getComment());
        userTest.setEndTime(answersForTest.getEndTime());
        userTest.setMark(answersForTest.getMark());
        userTest.setName(answersForTest.getTest().getName());
        userTest.setTestId(answersForTest.getTest().getId());
        userTest.setTraining(answersForTest.getTest().isTraining());
        return userTest;
    }

    @Override
    public AnswersForTest convert(UserTestDTO userTestDTO) {
        return null;
    }
}
