package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.FacultyDTO;
import com.exadel.irunner.model.entity.Faculty;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;

@Component
public class FacultyConverter extends BaseConverter<Faculty,FacultyDTO> {
    @Override
    public FacultyDTO convert(Faculty faculty) {
        FacultyDTO facultyDTO = new FacultyDTO();
        facultyDTO.setId(faculty.getId());
        facultyDTO.setName(faculty.getName());
        return facultyDTO;
    }

    @Override
    public Faculty convert(FacultyDTO facultyDTO) {
        Faculty faculty = new Faculty();
        faculty.setId(facultyDTO.getId());
        faculty.setName(facultyDTO.getName());
        return faculty;
    }
}
