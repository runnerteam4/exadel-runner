package com.exadel.irunner.service.tests;

import com.exadel.irunner.model.dto.tests.QuestionDTO;
import com.exadel.irunner.model.entity.tests.Question;

import java.util.List;
import java.util.Map;

public interface QuestionService {
    QuestionDTO findById(Long id);

    List<QuestionDTO> findByType(boolean type);

    List<QuestionDTO> findTrainingQuestions();

    List<QuestionDTO> findByTag(String tag);

    QuestionDTO create(QuestionDTO question);

    void delete(Long id);

    List<QuestionDTO> findAll();

}
