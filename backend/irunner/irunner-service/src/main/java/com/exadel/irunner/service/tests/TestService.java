package com.exadel.irunner.service.tests;

import com.exadel.irunner.model.dto.tests.*;
import com.exadel.irunner.model.filters.TestCheckFilter;
import com.exadel.irunner.model.filters.TestFilter;

import java.util.List;

public interface TestService {
    TestDTO findById(Long id);

    void assignForStudent(TestDTO testDTO);

    void assignForGroup(TestDTO testDTO);

    AnswersForTestDTO startTest(Long userId, Long testId);

    void delete(Long id);

    List<TestDTO> findAll();

    List<TestDTO> findByFilter(TestFilter testFilter);

    List<UserTestDTO> findPassedTests(Long id);

    List<TestResultDTO> getResults(AnswersForTestDTO answersForTestDTO);

    List<TestCheckDTO> getTestCheckList(TestCheckFilter testCheckFilter);

    Long getTime(Long id);
}
