package com.exadel.irunner.service.converter;

import com.exadel.irunner.model.dto.tests.TopicDTO;
import com.exadel.irunner.model.entity.tests.Topic;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.stereotype.Component;


@Component
public class TopicConverter extends BaseConverter<Topic, TopicDTO> {
    @Override
    public TopicDTO convert(Topic topic) {
        TopicDTO topicDTO = new TopicDTO();
        topicDTO.setId(topic.getId());
        topicDTO.setName(topic.getName());
        return topicDTO;
    }

    @Override
    public Topic convert(TopicDTO topicDTO) {
        Topic topic = new Topic();
        topic.setId(topicDTO.getId());
        topic.setName(topicDTO.getName());
        return topic;
    }
}
