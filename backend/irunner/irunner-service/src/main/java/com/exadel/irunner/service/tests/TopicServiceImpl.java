package com.exadel.irunner.service.tests;

import com.exadel.irunner.dao.repository.tests.topic.TopicDAO;
import com.exadel.irunner.model.dto.tests.TopicDTO;
import com.exadel.irunner.model.entity.tests.Topic;
import com.exadel.irunner.service.converter.TopicConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicDAO topicDAO;

    @Autowired
    private TopicConverter topicConverter;

    @Override
    public List<TopicDTO> findAll() {
        List<Topic> tags = new ArrayList<>();
        topicDAO.findAll().forEach(tags::add);
        return topicConverter.convert(tags);
    }

    @Override
    public TopicDTO findById(Long id) {
        Optional<Topic> tag = topicDAO.findById(id);
        return topicConverter.convert(tag.get());
    }

    @Override
    public TopicDTO findByName(String name) {
        Optional<Topic> tag = topicDAO.findByName(name);
        return topicConverter.convert(tag.get());
    }

    @Override
    public TopicDTO create(TopicDTO topicDTO) {
        Topic tag = topicConverter.convert(topicDTO);
        topicDAO.save(tag);
        return topicDTO;
    }

    @Override
    public void delete(Long id) {
        topicDAO.deleteById(id);
    }
}
