package com.exadel.irunner.service.converter;

import com.exadel.irunner.dao.repository.tests.question.QuestionDAO;
import com.exadel.irunner.model.dto.tests.AnswerOfUserDTO;
import com.exadel.irunner.model.entity.tests.AnswerOfUser;
import com.exadel.irunner.model.entity.tests.Question;
import com.exadel.irunner.service.converter.base.BaseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnswerOfUserConverter extends BaseConverter<AnswerOfUser, AnswerOfUserDTO> {
    @Autowired
    private QuestionDAO questionDAO;

    @Override
    public AnswerOfUserDTO convert(AnswerOfUser answerOfUser) {
        AnswerOfUserDTO answerOfUserDTO = new AnswerOfUserDTO();
        answerOfUserDTO.setId(answerOfUser.getId());
        answerOfUserDTO.setQuestionId(answerOfUser.getQuestion().getId());
        answerOfUserDTO.setAnswer(answerOfUser.getAnswer());
        answerOfUserDTO.setCorrect(answerOfUser.getCorrect());
        return answerOfUserDTO;
    }

    @Override
    public AnswerOfUser convert(AnswerOfUserDTO answerOfUserDTO) {
        AnswerOfUser answerOfUser = new AnswerOfUser();
        answerOfUser.setId(answerOfUserDTO.getId());
        Question question = questionDAO.findById(answerOfUserDTO.getQuestionId()).get();
        answerOfUser.setQuestion(question);
        answerOfUser.setAnswer(answerOfUserDTO.getAnswer());
        answerOfUser.setCorrect(false);
        return answerOfUser;
    }
}
