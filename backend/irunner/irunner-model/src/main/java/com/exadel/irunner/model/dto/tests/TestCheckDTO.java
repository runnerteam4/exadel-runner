package com.exadel.irunner.model.dto.tests;

import com.exadel.irunner.model.dto.BaseEntityDTO;

import java.time.LocalDateTime;

public class TestCheckDTO extends BaseEntityDTO {
    private String group;
    private String student;
    private String test;
    private boolean training;
    private LocalDateTime endDate;
    private Long testId;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public boolean isTraining() {
        return training;
    }

    public void setTraining(boolean training) {
        this.training = training;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }
}
