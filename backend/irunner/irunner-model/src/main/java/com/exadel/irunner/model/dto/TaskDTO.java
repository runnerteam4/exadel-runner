package com.exadel.irunner.model.dto;

public class TaskDTO extends BaseEntityDTO {

    private String name;
    private String condition;
    private Long categoryId;
    private Integer solutionCount;
    private Integer successSolutionCount;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String text) {
        this.condition = text;
    }

    public Integer getSolutionCount() {
        return solutionCount;
    }

    public void setSolutionCount(Integer solutionCount) {
        this.solutionCount = solutionCount;
    }

    public Integer getSuccessSolutionCount() {
        return successSolutionCount;
    }

    public void setSuccessSolutionCount(Integer successSolutionCount) {
        this.successSolutionCount = successSolutionCount;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

}
