package com.exadel.irunner.model.entity.tests;

import com.exadel.irunner.model.entity.BaseEntity;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.Type;

import javax.persistence.*;
import java.util.List;

@Entity
public class Question extends BaseEntity {

    @Column(name = "text")
    private String text;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private Type type;

    @Column(name = "complexity")
    private Integer complexity;

    @Column(name = "assignment_number")
    private Integer assignmentNum;

    @Column(name = "right_answers_number")
    private Integer rightAnswersNum;

    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "topic_id")
    private Category topic;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "question_tag",
            joinColumns = {@JoinColumn(name = "question_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    private List<Tag> tags;

    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY)
    private List<Answer> answers;

    @Column(name = "is_training")
    private boolean training;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "author_id")
    private User author;

    public Integer getAssignmentNum() {
        return assignmentNum;
    }

    public void setAssignmentNum(Integer assignmentNum) {
        this.assignmentNum = assignmentNum;
    }

    public Integer getRightAnswersNum() {
        return rightAnswersNum;
    }

    public void setRightAnswersNum(Integer rightAnswersNum) {
        this.rightAnswersNum = rightAnswersNum;
    }

    public Category getTopic() {
        return topic;
    }

    public void setTopic(Category topic) {
        this.topic = topic;
    }

    public User getAuthor() { return author; }

    public void setAuthor(User author) { this.author = author; }

    public boolean isTraining() {
        return training;
    }

    public void setTraining(boolean training) {
        this.training = training;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getComplexity() {
        return complexity;
    }

    public void setComplexity(Integer complexity) {
        this.complexity = complexity;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Answer> getAnswers() { return answers; }

    public void setAnswers(List<Answer> answers) { this.answers = answers; }

}
