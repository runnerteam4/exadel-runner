package com.exadel.irunner.model.exception;

public class ChangeGroupNameException extends RuntimeException {
    public ChangeGroupNameException(String message) {
        super(message);
    }
}