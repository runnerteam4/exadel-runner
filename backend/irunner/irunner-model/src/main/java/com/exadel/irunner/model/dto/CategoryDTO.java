package com.exadel.irunner.model.dto;


public class CategoryDTO extends BaseEntityDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
