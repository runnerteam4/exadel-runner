package com.exadel.irunner.model.dto.tests;

import java.util.List;

public class MarkDTO {
    private List<AnswerMarkDTO> marks;
    private String comment;
    private Long testId;

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<AnswerMarkDTO> getMarks() {
        return marks;
    }

    public void setMarks(List<AnswerMarkDTO> marks) {
        this.marks = marks;
    }
}
