package com.exadel.irunner.model.dto;


import java.io.Serializable;
import java.time.LocalDateTime;

public abstract class BaseEntityDTO implements Serializable{

    private Long id;

    private LocalDateTime createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }
}
