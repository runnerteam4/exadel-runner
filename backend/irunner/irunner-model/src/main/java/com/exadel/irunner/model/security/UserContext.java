package com.exadel.irunner.model.security;

import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/* The 'authorities' should be fixed
   by implementing 'Permission'
*/

public class UserContext implements UserDetails {

    private String userName;
    private String password;
    private Role role;
    private Collection<? extends GrantedAuthority> authorities;
    private User user;

    public UserContext(String userName, String password, Role role, Collection<? extends GrantedAuthority> authorities,
                       User user) {
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.authorities = authorities;
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getUserName() {
        return userName;
    }

    public Role getRole() {
        return role;
    }

    public User getUser() {
        return user;
    }
}