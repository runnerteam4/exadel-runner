package com.exadel.irunner.model.dto;

public class UniversityDTO extends BaseEntityDTO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
