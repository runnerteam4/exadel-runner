package com.exadel.irunner.model.dto.tests;

import com.exadel.irunner.model.dto.BaseEntityDTO;

import java.time.LocalDateTime;

public class UserTestDTO extends BaseEntityDTO {
    private Long testId;
    private String name;
    private boolean training;
    private LocalDateTime endTime;
    private Integer mark;
    private String comment;

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTraining() {
        return training;
    }

    public void setTraining(boolean training) {
        this.training = training;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
