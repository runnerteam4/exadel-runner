package com.exadel.irunner.model.dto.tests;

import com.exadel.irunner.model.dto.BaseEntityDTO;
import com.exadel.irunner.model.entity.tests.Question;
import com.exadel.irunner.model.enums.TestStatus;

import java.time.LocalDateTime;
import java.util.List;

public class AnswersForTestDTO extends BaseEntityDTO {
    private Long userId;
    private Long testId;
    private List<AnswerOfUserDTO> answers;
    private Integer mark;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private Integer time;
    private TestStatus status;
    private List<QuestionDTO> questions;
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public TestStatus getStatus() {
        return status;
    }

    public void setStatus(TestStatus status) {
        this.status = status;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public List<AnswerOfUserDTO> getAnswers() { return answers; }

    public void setAnswers(List<AnswerOfUserDTO> answers) {
        this.answers = answers;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }

}
