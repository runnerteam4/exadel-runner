package com.exadel.irunner.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "task")
public class Task extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "text")
    private String condition;

    @Column(name = "solution_count")
    private Integer solutionCount;

    @Column(name = "correct_solution_count")
    private Integer correctSolutionCount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="category_id")
    private Category category;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String text) {
        this.condition = text;
    }

    public Integer getSolutionCount() {
        return solutionCount;
    }

    public void setSolutionCount(Integer solutionCount) {
        this.solutionCount = solutionCount;
    }

    public Integer getCorrectSolutionCount() {
        return correctSolutionCount;
    }

    public void setCorrectSolutionCount(Integer correctSolutionCount) {
        this.correctSolutionCount = correctSolutionCount;
    }

}
