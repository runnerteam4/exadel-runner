package com.exadel.irunner.model.entity.tests;

import com.exadel.irunner.model.entity.BaseEntity;

import javax.persistence.*;

@Entity
public class Answer extends BaseEntity {

    @Column(name = "text")
    private String text;

    @ManyToOne(targetEntity = Question.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    private Question question;

    @Column(name = "correct")
    private boolean correct;

    public Answer() {
    }

    public Answer(String text, Question question, boolean correct) {
        this.text = text;
        this.question = question;
        this.correct = correct;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
