package com.exadel.irunner.model.entity.tests;

import com.exadel.irunner.model.entity.BaseEntity;

import javax.persistence.Entity;

@Entity
public class Topic extends BaseEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
