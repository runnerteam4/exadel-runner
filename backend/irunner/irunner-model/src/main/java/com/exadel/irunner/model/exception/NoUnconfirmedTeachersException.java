package com.exadel.irunner.model.exception;

public class NoUnconfirmedTeachersException extends RuntimeException {
    public NoUnconfirmedTeachersException(String message) {
        super(message);
    }
}
