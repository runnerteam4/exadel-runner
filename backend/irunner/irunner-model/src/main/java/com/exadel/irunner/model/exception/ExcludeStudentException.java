package com.exadel.irunner.model.exception;

public class ExcludeStudentException extends RuntimeException {
    public ExcludeStudentException(String message) {
        super(message);
    }
}