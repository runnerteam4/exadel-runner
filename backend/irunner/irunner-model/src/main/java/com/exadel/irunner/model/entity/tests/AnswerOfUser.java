package com.exadel.irunner.model.entity.tests;

import com.exadel.irunner.model.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AnswerOfUser extends BaseEntity {
    @ManyToOne(targetEntity = Question.class)
    @JoinColumn(name = "question_id")
    private Question question;

    @Column(name = "answer")
    private String answer;

    @Column(name = "correct")
    private boolean correct;

    @ManyToOne(targetEntity = AnswersForTest.class)
    @JoinColumn(name = "answer_list_id")
    private AnswersForTest answerList;

    public boolean getCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public AnswersForTest getAnswerList() {
        return answerList;
    }

    public void setAnswerList(AnswersForTest answerList) {
        this.answerList = answerList;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
