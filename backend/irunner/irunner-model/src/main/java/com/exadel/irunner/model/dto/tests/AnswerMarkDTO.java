package com.exadel.irunner.model.dto.tests;

import com.exadel.irunner.model.dto.BaseEntityDTO;

public class AnswerMarkDTO extends BaseEntityDTO {
    private Long answerId;
    private Integer mark;

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }
}
