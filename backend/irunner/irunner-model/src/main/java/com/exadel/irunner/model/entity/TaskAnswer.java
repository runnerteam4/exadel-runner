package com.exadel.irunner.model.entity;

import com.exadel.irunner.model.entity.tests.Answer;

import javax.persistence.*;

@Entity
@Table(name="task_answer")
public class TaskAnswer extends BaseEntity {

    @Column(name="input")
    private String input;

    @Column(name="answer")
    private String answer;

    @ManyToOne
    @JoinColumn(name="task_id")
    private Task task;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}