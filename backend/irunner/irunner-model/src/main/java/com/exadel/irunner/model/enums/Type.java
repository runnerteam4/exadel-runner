package com.exadel.irunner.model.enums;

public enum Type {
    LIST_ONE_ANSWER,
    LIST_MULTIPLE_ANSWER,
    SHORT_ANSWER,
    LONG_ANSWER
}
