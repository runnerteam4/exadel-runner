package com.exadel.irunner.model.enums;

public enum TestStatus {
    WAITING,
    IN_PROGRESS,
    FINISHED,
    CHECKED
}
