package com.exadel.irunner.model.entity.tests;

import com.exadel.irunner.model.entity.BaseEntity;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.TestStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class AnswersForTest extends BaseEntity {
    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id")
    private User student;

    @ManyToOne(targetEntity = Test.class)
    @JoinColumn(name = "test_id")
    private Test test;

    @Column(name = "mark")
    private Integer mark;

    @Column(name = "comment")
    private String comment;

    @OneToMany(mappedBy = "answerList", fetch = FetchType.LAZY)
    private List<AnswerOfUser> answers;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TestStatus status;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    private LocalDateTime endTime;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "test_question",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "question_id")}
    )
    private List<Question> questions;

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public TestStatus getStatus() {
        return status;
    }

    public void setStatus(TestStatus status) {
        this.status = status;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public List<AnswerOfUser> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerOfUser> answers) {
        this.answers = answers;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

}
