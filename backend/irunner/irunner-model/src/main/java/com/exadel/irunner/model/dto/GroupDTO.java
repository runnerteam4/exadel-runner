package com.exadel.irunner.model.dto;

import java.util.HashSet;
import java.util.Set;

public class GroupDTO extends BaseEntityDTO {

    private String name;
    private Set<UserDTO> users = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

}
