package com.exadel.irunner.model.entity.tests;

import com.exadel.irunner.model.entity.BaseEntity;
import com.exadel.irunner.model.entity.Category;
import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.TestStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
public class Test extends BaseEntity {
    @Column(name = "name")
    private String name;

    @Column(name = "training")
    private boolean training;

    @Column(name = "time")
    private Integer time;

    @Column(name = "deadline")
    private String deadline;

    @Column(name = "question_amount")
    private Integer questionAmount;

    @ManyToOne(targetEntity = Group.class)
    @JoinColumn(name = "group_id")
    private Group group;

    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "topic_id")
    private Category topic;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "answers_for_test",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private List<User> users;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "test_tag",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    private List<Tag> tags;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Integer getQuestionAmount() {
        return questionAmount;
    }

    public void setQuestionAmount(Integer questionAmount) {
        this.questionAmount = questionAmount;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Category getTopic() {
        return topic;
    }

    public void setTopic(Category topic) {
        this.topic = topic;
    }

    public boolean isTraining() {
        return training;
    }

    public void setTraining(boolean training) {
        this.training = training;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTime() { return time; }

    public void setTime(Integer time) { this.time = time; }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
