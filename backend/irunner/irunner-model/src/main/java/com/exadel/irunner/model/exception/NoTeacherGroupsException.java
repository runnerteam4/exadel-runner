package com.exadel.irunner.model.exception;

public class NoTeacherGroupsException extends RuntimeException {
    public NoTeacherGroupsException(String message) {
        super(message);
    }
}
