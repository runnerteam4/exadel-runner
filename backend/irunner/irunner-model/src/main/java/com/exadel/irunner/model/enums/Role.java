package com.exadel.irunner.model.enums;

public enum Role {
    ROLE_ADMIN,
    ROLE_TEACHER,
    ROLE_STUDENT
}
