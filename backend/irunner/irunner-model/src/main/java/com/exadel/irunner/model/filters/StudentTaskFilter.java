package com.exadel.irunner.model.filters;

import com.exadel.irunner.model.dto.StudentTaskDTO;
import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.StudentTask;

import java.util.List;

public class StudentTaskFilter {

    private String taskName;
    private String groupName;
    private String assignTo;
    private Integer mark;
    private String deadline;

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(String assignTo) {
        this.assignTo = assignTo;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}
