package com.exadel.irunner.model.entity;

import javax.persistence.*;

@Entity
@Table(name="category")
public class Category extends BaseEntity {

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
