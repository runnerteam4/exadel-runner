package com.exadel.irunner.model.dto.tests;

import com.exadel.irunner.model.dto.BaseEntityDTO;

public class AnswerDTO extends BaseEntityDTO {
    private String text;
    private boolean correct;

    public AnswerDTO(){}

    public AnswerDTO(String text, boolean correct) {
        this.text = text;
        this.correct = correct;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}
