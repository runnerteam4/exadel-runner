package com.exadel.irunner.model.exception;

public class NoSuchGroupException extends RuntimeException {
    public NoSuchGroupException(String message) {
        super(message);
    }
}
