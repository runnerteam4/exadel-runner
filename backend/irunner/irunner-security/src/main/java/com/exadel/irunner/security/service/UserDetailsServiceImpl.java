package com.exadel.irunner.security.service;

import com.exadel.irunner.dao.repository.user.UserDAO;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.enums.Role;
import com.exadel.irunner.model.exception.AccountNotActivatedException;
import com.exadel.irunner.model.security.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/* The 'authorities' should be fixed
   by implementing 'Permission'
*/

@Service
@Transactional
@ComponentScan(basePackages = "com.exadel.irunner")
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final String USER_NOT_FOUND_MESSAGE = "No user found with username: %s";
    private static final String ACCOUNT_NOT_ACTIVATED_MESSAGE = "Account with email: %s hasn't been activated";

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDAO.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(String.format(USER_NOT_FOUND_MESSAGE, email));
        }
        if(!user.isEnabled()){
            throw new AccountNotActivatedException(String.format(ACCOUNT_NOT_ACTIVATED_MESSAGE,email));
        }
        String userName = user.getEmail();
        String userPassword = user.getPassword();
        Role role = user.getRole();
        List<GrantedAuthority> authorities = getAuthorities(role);
        return new UserContext(userName, userPassword, role, authorities,user);
    }

    private static List<GrantedAuthority> getAuthorities(Role roleType) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        String roleName = roleType.name();
        authorities.add(new SimpleGrantedAuthority(roleName));
        return authorities;
    }
}