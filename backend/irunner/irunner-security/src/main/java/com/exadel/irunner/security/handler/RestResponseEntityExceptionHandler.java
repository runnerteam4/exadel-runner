package com.exadel.irunner.security.handler;

import com.exadel.irunner.model.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

    @ExceptionHandler(value = EmailExistsException.class)
    private ResponseEntity<String> handleEmailExistingConflict(EmailExistsException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = EmailNotValidException.class)
    private ResponseEntity<String> handleEmailNotValidConflict(EmailNotValidException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = ExcludeStudentException.class)
    private ResponseEntity<String> handleExcludeStudentConflict(ExcludeStudentException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = ChangeGroupNameException.class)
    private ResponseEntity<String> handleChangeGroupNameConflict(ChangeGroupNameException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = NoTeacherGroupsException.class)
    private ResponseEntity<String> handleNoTeacherGroupsConflict(NoTeacherGroupsException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = NoSuchGroupException.class)
    private ResponseEntity<String> handleNoSuchGroupConflict(NoSuchGroupException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = NoSuchUserException.class)
    private ResponseEntity<String> handleNoSuchUserConflict(NoSuchUserException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = NoUnconfirmedTeachersException.class)
    private ResponseEntity<String> handleNoUnconfirmedTeachersConflict(NoUnconfirmedTeachersException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = PasswordNotMatchesException.class)
    private ResponseEntity<String> handlePasswordNotMatchesConflict(PasswordNotMatchesException exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(exceptionMessage);
    }

    @ExceptionHandler(value = Exception.class)
    private ResponseEntity<String> handleExceptionConflict(Exception exception){
        String exceptionMessage = exception.getMessage();
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(exceptionMessage);
    }

}


