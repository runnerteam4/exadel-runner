package com.exadel.irunner.dao.repository.tests.question;

import com.exadel.irunner.model.entity.tests.Question;

import java.util.List;

interface CustomQuestionDAO {
    Question saveOrUpdate(Question question);
}
