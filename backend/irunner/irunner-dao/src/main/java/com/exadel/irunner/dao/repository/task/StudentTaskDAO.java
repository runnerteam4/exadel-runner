package com.exadel.irunner.dao.repository.task;

import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.StudentTask;
import com.exadel.irunner.model.entity.TaskAnswer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentTaskDAO extends CrudRepository<StudentTask, Long> {

    @Query(value = "select studentTask from StudentTask studentTask where studentTask.id = :id and studentTask.studentId =:studentId")
    StudentTask findStudentTask(@Param("id") Long id, @Param("studentId") Long studentId);

   @Query(value = "select studentTask from StudentTask studentTask where studentTask.studentId=:studentId")
    List<StudentTask> findStudentTasks(@Param("studentId") Long studentId);

   @Query(value = "select studentTask from StudentTask studentTask where studentTask.taskId=:taskId")
    List<StudentTask> findStudentTaskByTaskId(@Param("taskId") Long taskId);

   @Query(value="select studentTask from StudentTask studentTask where studentTask.teacherId=:teacherId")
    List<StudentTask> findStudentTasksByTeacherId(@Param("teacherId") Long teacherId);

    @Modifying
    @Query(value = "update StudentTask set attempt = attempt + 1 where studentId = :studentId and taskId=:taskId")
    void incrementAttempt (@Param("studentId") Long studentId, @Param("taskId") Long taskId);

    @Modifying
    @Query(value = "update StudentTask set success = success + 1 where studentId = :studentId and taskId=:taskId")
    void incrementCorrectSolution (@Param("studentId") Long studentId, @Param("taskId") Long taskId);

    @Modifying
    @Query(value = "update StudentTask set mark = :mark where id = :id")
    Integer changeMark(@Param("id") Long id, @Param("mark") Integer mark);

}
