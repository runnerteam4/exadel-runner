package com.exadel.irunner.dao.repository.tests.answer;

import com.exadel.irunner.model.entity.tests.AnswerOfUser;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import com.exadel.irunner.model.entity.tests.Test;

import java.util.List;

public interface CustomAnswersForTestDAO {
    List<AnswerOfUser> getAnswersToCheck(Long id);

    List<AnswersForTest> getPassedTests(Long id);

    List<Test> getByFilter(String name, Boolean type, List<String> topics, Long userId, Integer page, Integer chunkSize);
}
