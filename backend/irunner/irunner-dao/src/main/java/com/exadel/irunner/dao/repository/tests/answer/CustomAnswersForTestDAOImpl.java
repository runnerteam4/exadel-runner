package com.exadel.irunner.dao.repository.tests.answer;

import com.exadel.irunner.model.entity.tests.AnswerOfUser;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import com.exadel.irunner.model.entity.tests.Test;
import com.exadel.irunner.model.enums.TestStatus;
import com.exadel.irunner.model.enums.Type;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

public class CustomAnswersForTestDAOImpl implements CustomAnswersForTestDAO {
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<AnswerOfUser> getAnswersToCheck(Long id) {
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("select answer from AnswersForTest aft" +
                " inner join aft.answers answer where aft.student.id = :id and " +
                "answer.question.type = :LongAnswer", AnswerOfUser.class)
                .setParameter("id", id).setParameter("LongAnswer", Type.LONG_ANSWER);
        return query.list();
    }

    @Override
    public List<AnswersForTest> getPassedTests(Long id) {
        Session session = em.unwrap(Session.class);
        Query query = session.createQuery("select answer from AnswersForTest answer" +
                " where answer.student.id = :id and answer.status in (:finished)")
                .setParameter("id", id).setParameter("finished", new ArrayList<>(Arrays.asList(TestStatus.CHECKED, TestStatus.FINISHED)));
        return query.list();
    }

    @Override
    public List<Test> getByFilter(String name, Boolean type, List<String> topics, Long userId, Integer page, Integer chunkSize) {
        Session session = em.unwrap(Session.class);
        StringBuilder hql = new StringBuilder("SELECT DISTINCT test FROM AnswersForTest aft LEFT JOIN aft.test test LEFT JOIN aft.student user WHERE aft.status = :status");
        Map<String, Object> params = new HashMap<>();

        if(type != null) {
            hql.append(" AND test.training = :type");
            params.put("type", type);
        }
        if(topics != null) {
            hql.append(" AND test.topic.name in (:topics)");
            params.put("topics", topics);
        }

        if(userId != null){
            hql.append(" AND user.id = :userId");
            params.put("userId", userId);
        }

        Query query = session.createQuery(hql.toString(), Test.class).setParameter("status", TestStatus.WAITING);

        for(Map.Entry<String, Object> entry: params.entrySet())
            query.setParameter(entry.getKey(), entry.getValue());

        query.setFirstResult((page-1)*chunkSize).setMaxResults(chunkSize);

        List<Test> tests = query.list();
        if(name != null){
            tests = tests.stream().filter(t->t.getName().contains(name)).collect(Collectors.toList());
        }
        return tests;
    }
}
