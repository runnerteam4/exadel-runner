package com.exadel.irunner.dao.repository.tests.question;

import com.exadel.irunner.model.entity.tests.Question;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CustomQuestionDAOImpl implements CustomQuestionDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Question saveOrUpdate(Question question) {
        Session session = em.unwrap(Session.class);
        session.persist(question);
        return question;
    }
}
