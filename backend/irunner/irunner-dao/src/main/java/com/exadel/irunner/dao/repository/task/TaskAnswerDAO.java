package com.exadel.irunner.dao.repository.task;

import com.exadel.irunner.model.entity.Task;
import com.exadel.irunner.model.entity.TaskAnswer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskAnswerDAO extends CrudRepository<TaskAnswer,Long> {
    @Query(value = "select taskAnswer from TaskAnswer taskAnswer where taskAnswer.task.id = :taskId")
    List<TaskAnswer> findTaskAnswersByTaskId(@Param("taskId") Long id);

}
