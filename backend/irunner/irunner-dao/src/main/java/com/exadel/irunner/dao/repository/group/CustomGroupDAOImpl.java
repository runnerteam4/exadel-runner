package com.exadel.irunner.dao.repository.group;

import com.exadel.irunner.model.entity.UserGroup;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class CustomGroupDAOImpl implements CustomGroupDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Integer excludeStudentFromGroup(Long studentId, Long groupId) {
        String hql = "delete from UserGroup ug where ug.userId = :studentId and ug.groupId = :groupId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("studentId", studentId);
        query.setParameter("groupId", groupId);
        return query.executeUpdate();
    }

    @Override
    public void addStudentToGroup(UserGroup userGroup) {
        Session session = entityManager.unwrap(Session.class);
        session.persist(userGroup);
    }

    @Override
    public void assignTeacherToGroup(UserGroup userGroup) {
        Session session = entityManager.unwrap(Session.class);
        session.persist(userGroup);
    }

}
