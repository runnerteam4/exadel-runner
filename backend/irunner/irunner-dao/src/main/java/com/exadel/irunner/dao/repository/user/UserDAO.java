package com.exadel.irunner.dao.repository.user;
import com.exadel.irunner.model.entity.User;
import com.exadel.irunner.model.entity.UserGroup;
import com.exadel.irunner.model.enums.Role;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TODO
 *
 * @author Anton Harakh
 * @since 07.07.2018
 */
@Repository
public interface UserDAO extends CrudRepository<User,Long>{

    @Query(value = "select user from User user where user.email = :email")
    User findByEmail(@Param("email") String email);

    @Query(value = "select user from User user where user.id = (:ids)")
    List<User> findByIds(@Param("ids") List<Long> id);

    /*@Query(value="select user.id from User inner join UserGroup where User.id=:UserGroup.userId and UserGroup.groupId=:groupId")
    List<Long> getUserIdsByGroupId(@Param("groupId") Long groupId);*/

    @Modifying
    @Query(value = "update User set isEnabled = true where id = :id")
    void enableUser(@Param("id") Long id);

    @Modifying
    @Query(value = "update User set status = :status where id = :id")
    void confirmTeacher(@Param("id") Long id, @Param("status") boolean status);

    @Modifying
    @Query(value = "update User set role = :role where id = :id")
    void assignRoleToUser(@Param("role") Role role, @Param("id") Long id);

    @Modifying
    @Query(value = "update User set password = :password where id = :id")
    void changePassword(@Param("id") Long id, @Param("password") String password);

    @Query("select u FROM User u where u.role = :role and u.status = false")
    List<User> getUnconfirmedUsers(@Param("role") Role role);

}
