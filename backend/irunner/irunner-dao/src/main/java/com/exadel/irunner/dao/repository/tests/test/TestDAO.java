package com.exadel.irunner.dao.repository.tests.test;

import com.exadel.irunner.model.entity.tests.Test;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestDAO extends CrudRepository<Test, Long>, CustomTestDAO{

    @Query("select test from Test test where test.name = :name")
    List<Test> findByName(@Param("name") String name);

}
