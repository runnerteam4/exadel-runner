package com.exadel.irunner.dao.repository.group;

import com.exadel.irunner.model.entity.Group;
import com.exadel.irunner.model.entity.UserGroup;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupDAO extends CrudRepository<Group,Long> {

    @Query("select distinct g FROM Group g INNER JOIN g.users u where u.id = :id")
    List<Group> findByTeacherId(@Param("id") Long id);



    @Modifying
    @Query(value = "update Group set name = :name where id = :id")
    Integer changeGroupName(@Param("id") Long id, @Param("name") String name);

}
