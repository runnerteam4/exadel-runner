package com.exadel.irunner.dao.repository.group;

import com.exadel.irunner.model.entity.UserGroup;

public interface CustomGroupDAO {
    Integer excludeStudentFromGroup(Long studentId, Long groupId);
    void addStudentToGroup(UserGroup userGroup);
    void assignTeacherToGroup(UserGroup userGroup);
}
