package com.exadel.irunner.dao.repository.task;

import com.exadel.irunner.model.entity.Task;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskDAO extends CrudRepository<Task, Long> {

    @Query(value = "select task from Task task where task.name = :taskName")
    Task findByName(@Param("taskName") String name);


    @Query(value="select task from Task task where task.category.id = :categoryId")
    List<Task> findByCategory(@Param("categoryId") Long categoryId);

    @Modifying
    @Query(value = "update Task set solutionCount = solutionCount + 1 where id = :taskId")
    void updateSolution(@Param("taskId") Long id);

    @Modifying
    @Query(value = "update Task set correctSolutionCount = correctSolutionCount + 1 where id = :taskId")
    void updateCorrectSolution(@Param("taskId") Long id);

}
