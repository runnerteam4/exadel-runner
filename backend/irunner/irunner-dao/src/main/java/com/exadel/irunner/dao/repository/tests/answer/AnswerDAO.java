package com.exadel.irunner.dao.repository.tests.answer;

import com.exadel.irunner.model.entity.tests.Answer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerDAO extends CrudRepository<Answer, Long> {
    @Query("select ans.id from Answer ans where ans.question.id = :id and ans.correct = true")
    List<Long> rightAnswersIds(@Param("id") Long id);
}
