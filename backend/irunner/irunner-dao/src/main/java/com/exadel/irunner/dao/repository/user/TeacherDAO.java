package com.exadel.irunner.dao.repository.user;

import com.exadel.irunner.model.entity.Teacher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * TODO
 *
 * @author Nick Prishchepov
 * @since 25.07.2018
 */
@Repository
public interface TeacherDAO extends CrudRepository<Teacher, Long> {

    @Query("select teacher FROM Teacher teacher INNER JOIN teacher.user u where u.id = :id")
    Teacher getTeacherByUserId(@Param("id") Long id);

    @Query("select g.id FROM Teacher g INNER JOIN g.user u where u.id = :id")
    Long findByUserId(@Param("id") Long id);

}
