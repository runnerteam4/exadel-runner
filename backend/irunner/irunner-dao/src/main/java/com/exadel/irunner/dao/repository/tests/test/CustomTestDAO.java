package com.exadel.irunner.dao.repository.tests.test;

import com.exadel.irunner.model.entity.tests.Test;
import com.exadel.irunner.model.filters.TestFilter;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Set;

interface CustomTestDAO {
    List<Test> findByFilter(String name, Boolean type, List<String> topics, Long userId, Integer page, Integer chunkSize);
}
