package com.exadel.irunner.dao.repository.task;

import com.exadel.irunner.model.entity.TaskAnswer;
import com.exadel.irunner.model.entity.TaskGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskGroupDAO extends CrudRepository<TaskGroup, Long> {

    @Query(value = "select taskGroup from TaskGroup taskGroup where taskGroup.groupId in (:groupIds) ")
    List<TaskGroup> findTaskGroupByGroupId(@Param("groupIds") List<Long> groupIds);

    @Query(value = "select taskGroup from TaskGroup taskGroup where taskGroup.groupId=:groupId and taskGroup.taskId=:taskId")
    TaskGroup findTaskGroupByTaskIdAndGroupId(@Param("taskId") Long taskId, @Param("groupId")Long groupId);

    @Query (value="select taskGroup from TaskGroup taskGroup where taskGroup.taskId=:taskId")
    List<TaskGroup> findGrouptaskByTaskId(@Param("taskId")Long taskId);


}
