package com.exadel.irunner.dao.repository.user;
import com.exadel.irunner.model.entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TODO
 *
 * @author Nick Prishchepov
 * @since 25.07.2018
 */
@Repository
public interface StudentDAO extends CrudRepository<Student,Long>{

    @Query(value="select students from Student students inner join students.user user where user.id in (:ids)")
    List<Student> students (@Param("ids") List<Long> userIds);

    @Query("select g.id FROM Student g INNER JOIN g.user u where u.id = :id")
    Long findByUserId(@Param("id") Long id);

    @Query("select s FROM Student s INNER JOIN s.user u where u.id = :id")
    Student findStudentById(@Param("id") Long id);



}
