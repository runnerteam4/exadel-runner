package com.exadel.irunner.dao.repository.tests.test;

import com.exadel.irunner.model.entity.tests.Test;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomTestDAOImpl implements CustomTestDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Test> findByFilter(String name, Boolean type, List<String> topics, Long userId, Integer page, Integer chunkSize) {
        Session session = em.unwrap(Session.class);
        StringBuilder hql = new StringBuilder("SELECT DISTINCT test FROM Test test LEFT JOIN test.topic topic LEFT JOIN test.users user WHERE 1 = 1");
        Map<String, Object> params = new HashMap<>();
        if(name != null) {
            hql.append(" AND  test.name = :name");
            params.put("name", name);
        }
        if(type != null) {
            hql.append(" AND test.training = :type");
            params.put("type", type);
        }
        if(topics != null) {
            hql.append(" AND topic.name in (:topics)");
            params.put("topics", topics);
        }

        if(userId != null){
            hql.append(" AND user.id = :userId");
            params.put("userId", userId);
        }

        Query query = session.createQuery(hql.toString(), Test.class);

        for(Map.Entry<String, Object> entry: params.entrySet())
            query.setParameter(entry.getKey(), entry.getValue());

        query.setFirstResult((page-1)*chunkSize).setMaxResults(chunkSize);
                return query.list();
            }
    }

