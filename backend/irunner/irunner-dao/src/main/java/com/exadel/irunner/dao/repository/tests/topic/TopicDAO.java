package com.exadel.irunner.dao.repository.tests.topic;


import com.exadel.irunner.model.entity.tests.Topic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TopicDAO extends CrudRepository<Topic, Long>{
    @Query("select topic from Topic topic where topic.name = :name")
    Optional<Topic> findByName(@Param("name") String name);
}
