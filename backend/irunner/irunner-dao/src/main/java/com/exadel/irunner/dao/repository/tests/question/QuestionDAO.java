package com.exadel.irunner.dao.repository.tests.question;

import com.exadel.irunner.model.entity.tests.Question;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionDAO extends CrudRepository<Question, Long>, CustomQuestionDAO {
    @Query("select question from Question question where question.training = true")
    List<Question> findTrainingQuestions();

    @Query("select question from Question question where question.type = :type")
    List<Question> findByType(@Param("type") boolean type);

    @Query("select question from Question question INNER JOIN question.tags tag where tag.name=:name")
    List<Question> findByTag(@Param("name") String name);

    @Modifying
    @Query("update Question set assignmentNum = assignmentNum+1 where id = :id")
    void incrementAssignmentNum(@Param("id") Long id);

    @Modifying
    @Query("update Question set rightAnswersNum = rightAnswersNum+1 where id = :id")
    void incrementRightAnswersNum(@Param("id") Long id);

}
