package com.exadel.irunner.dao.repository.wiki;
import com.exadel.irunner.model.entity.Wiki;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WikiDAO extends CrudRepository<Wiki,Long> {
    @Query(value = "select wiki from Wiki wiki where wiki.category.id = :categoryId")
    List<Wiki> findByCategory(@Param("categoryId") Long categoryId);

    @Query(value = "select wiki from Wiki wiki where wiki.category.id in (:categoryIds)")
    List<Wiki> findByCategory(@Param("categoryIds") List<Long> categoryIds);

    @Query(value = "select wiki from Wiki wiki where wiki.name = :wikiName")
    Wiki findByName(@Param("wikiName") String name);

}
