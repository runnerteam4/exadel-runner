package com.exadel.irunner.dao.repository.tests.tag;

import com.exadel.irunner.model.entity.tests.Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagDAO extends CrudRepository<Tag, Long> {
    @Query("select tag from Tag tag where tag.name in (:names)")
    List<Tag> findByName(@Param("names") List<String> names);
}
