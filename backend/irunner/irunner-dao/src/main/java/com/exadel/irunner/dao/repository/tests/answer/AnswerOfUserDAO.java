package com.exadel.irunner.dao.repository.tests.answer;

import com.exadel.irunner.model.entity.tests.AnswerOfUser;
import com.exadel.irunner.model.entity.tests.AnswersForTest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerOfUserDAO extends CrudRepository<AnswerOfUser, Long> {
    @Query("select answer from AnswerOfUser answer where answer.answerList.id = :id")
    public List<AnswerOfUser> findByTestId(@Param("id") Long id);


}
