package com.exadel.irunner.dao.repository.tests.answer;

import com.exadel.irunner.model.entity.tests.AnswersForTest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnswersForTestDAO extends CrudRepository<AnswersForTest, Long>, CustomAnswersForTestDAO {
    @Query("select answer from AnswersForTest answer where answer.student.id = :userId and answer.test.id = :testId")
    AnswersForTest findByIds(@Param("userId") Long userId, @Param("testId") Long testId);

    @Query("select answer from AnswersForTest answer where answer.student.id in (:ids)")
    List<AnswersForTest> findByStudentId(@Param("ids") List<Long> ids);

    @Query("select answer from AnswersForTest answer where answer.student.id = :id")
    List<AnswersForTest> findByStudentId(@Param("id") Long id);
}
