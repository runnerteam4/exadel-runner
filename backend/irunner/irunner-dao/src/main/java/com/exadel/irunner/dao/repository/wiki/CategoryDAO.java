package com.exadel.irunner.dao.repository.wiki;
import com.exadel.irunner.model.entity.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryDAO extends CrudRepository<Category,Long> {
    
    @Query(value = "select category from Category category where category.name = :categoryName")
    Category findByName(@Param("categoryName") String name);

}
